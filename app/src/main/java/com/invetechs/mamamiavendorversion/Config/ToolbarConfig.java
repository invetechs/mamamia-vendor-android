package com.invetechs.mamamiavendorversion.Config;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Adapter.NotificationstAdapter;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.NotificationsApi.NotificationRequest;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.NotificationResponse;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Activity.NotificationsActivity;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;
import com.nex3z.notificationbadge.NotificationBadge;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;
import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class ToolbarConfig {

    // vars
    static String user_id = "0";
    JSONObject jsonObject;
    TextView toolbar_title;
    ImageView toolbar_back;
    ImageView img_notifications;
    String user_login = "";
    ShowDialog showDialog = new ShowDialog();
    Activity context;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public NotificationBadge badge_notification;
    Animation shake;
    static int count_number = 0;
    Dialog progress;
    LinearLayout layout;
    ContentLoadingProgressBar contentLoadingProgressBar;
    Handler handler;
    List<NotificationResponse.DataBean> arrayList;
    Context this_notification;
    String flag = "";
    PusherOptions options;
    Pusher pusher;
    Channel channel;

    public ToolbarConfig() {
    } // default constructor


    public ToolbarConfig(Activity context) {
        this.context = context;
        shake = AnimationUtils.loadAnimation(context, R.anim.shake);
        toolbar_title = context.findViewById(R.id.tv_title);
        toolbar_back = context.findViewById(R.id.img_back);
        img_notifications = context.findViewById(R.id.img_notification);
        badge_notification = context.findViewById(R.id.notification_number);
        getUserID();
        notification();

    } // param constructor


    public ToolbarConfig(Activity context, Context this_notification, String flag) {
        this.context = context;
        this.this_notification = this_notification;
        this.flag = flag;
        shake = AnimationUtils.loadAnimation(context, R.anim.shake);
        toolbar_title = context.findViewById(R.id.tv_title);
        toolbar_back = context.findViewById(R.id.img_back);
        img_notifications = context.findViewById(R.id.img_notification);
        badge_notification = context.findViewById(R.id.notification_number);
        getUserID();
        notification();

    } // param constructor


    public void setTitle(String title) {
        toolbar_title.setText(title);
    } // function of setTitle to toolbar

    public void setBack(final Activity context) {
        toolbar_back.setVisibility(View.VISIBLE);
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onBackPressed();
            }
        });
    } // function handl backButton on toolbar

    public String getUserID() {

        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        user_login = prefs.getString("id", "0");

        Log.i("QP", "profile id : " + user_login);
        return user_id;
    } // function get the id of user to send it to api


    public void notification() {

        Log.i("QP", "notification FUNCTION");
        options = new PusherOptions();
        options.setCluster("eu");
        pusher = new Pusher("2f846111e977afd34a83", options);
        channel = pusher.subscribe("my-channel");
        channel.bind("my-event", new SubscriptionEventListener() {

            @Override
            public void onEvent(String channelName, String eventName, final String data) {

                try {

                    jsonObject = new JSONObject(data);
                    user_id = jsonObject.getString("user_id");

                    Log.i("EER", "" + user_id + " : " + user_login);

                    try {

                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (user_id.equals(user_login)) {

                                    try {

                                        count_number = Integer.parseInt(jsonObject.getString("count"));
                                        badge_notification.setNumber(count_number);
                                        ShortcutBadger.applyCount(context, count_number); //for 1.1.4+

                                        img_notifications.setAnimation(shake);
                                        if (flag.equals("notification")) {

                                            if (!(context).isFinishing())
                                                getNotifications();
                                        }

                                        Log.i("EER", "count : " + count_number);
                                        Log.i("QP", "notification count   : " + count_number);
                                        Log.i("QP", "user id notification : " + user_id);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                                // Stuff that updates the UI
                            }
                        });
                    } catch (Exception e) {
                        Log.i("QP", "notification exception   : " + e.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        pusher.connect();
        img_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent notification = new Intent(context, NotificationsActivity.class);
                context.startActivity(notification);


            }
        }); // intent to notification activity


    } // function handle notification icon on toolbar

    public void addNotification() {
        img_notifications.setAnimation(shake);
        badge_notification.setNumber(count_number);
        ShortcutBadger.applyCount(context, count_number); //for 1.1.4+

        Log.i("QP", "send notifications number : " + count_number);
    }

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void initRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        NotificationstAdapter adapter;
        RecyclerView notification_recycler_view = context.findViewById(R.id.notification_recycler_view);
        adapter = new NotificationstAdapter(context, arrayList);
        notification_recycler_view.setHasFixedSize(true);
        notification_recycler_view.setLayoutManager(layoutManager);
        notification_recycler_view.setAdapter(adapter);
    }

    @SuppressLint("HandlerLeak")
    private void getNotifications() {
        progress = new Dialog(this_notification);
        progress.setContentView(R.layout.progress_loading_dialog);
        progress.setCancelable(false);

        layout = progress.findViewById(R.id.layoutDialog);
        contentLoadingProgressBar = progress.findViewById(R.id.progressLoading);
        contentLoadingProgressBar.setVisibility(View.VISIBLE);

        layout.setBackgroundResource(R.color.colorPrimaryDark);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progress.getWindow()
                .setLayout((int) (getScreenWidth(context) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();

      /*  new Thread() {
            public void run() {*/

        Retrofit retrofit = RetrofiUrlConnection.connectWith();

        final NotificationRequest notificationApi = retrofit.create(NotificationRequest.class);

        final Call<NotificationResponse> getInterestConnection = notificationApi.showNotifications(user_login);

        getInterestConnection.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                try {
                    String code = response.body().getCode();

                    Log.i("QP", "code" + code);

                    response.body();

                    Log.i("QP", "response" + response.body().getCode());

                    if (code.equals("200")) {

                        arrayList = response.body().getData();

                        initRecyclerView();
                    } // login success

                    progress.dismiss();

                } // try
                catch (Exception e) {
                    Log.i("QP", "exception : " + e.toString());
                    progress.dismiss();
                } // catch
            } // onResponse

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                showDialog.initDialog(context.getString(R.string.retry), this_notification);
                Log.i("QP", "error : " + t.toString());
                progress.dismiss();
            } // on Failure
        });
        /*    }

        }.start();*/
    } // show notifications

    public void onDestroy() {
        if (context != null) {
            context = null;
        }

        Thread.interrupted();
        if (pusher != null) {
            pusher.disconnect();

        }
    }

}
