package com.invetechs.mamamiavendorversion.Language;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.os.Build;

import java.util.Locale;

public class MyContextWrapper extends ContextWrapper {

    // vars
    private Context context;


    public MyContextWrapper(Context context) {
        super(context);
        this.context = context;
    } // constructor

    public ContextWrapper wrap(String language) {
        Context mContext = context;
        Configuration config = context.getResources().getConfiguration();
        if (!language.isEmpty()) {
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                setSystemLocale(config, locale);
            }
            else {
                setSystemLocaleLegacy(config, locale);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            {
                mContext = context.createConfigurationContext(config);

            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            {
                mContext = context.createConfigurationContext(config);

            }
            else {
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            }
        }
        return new MyContextWrapper(mContext);
    } // wrapping language

    private void setSystemLocaleLegacy(Configuration config, Locale locale) {
        config.locale = locale;
    } // set locale

    @TargetApi(Build.VERSION_CODES.N)
    private void setSystemLocale(Configuration config, Locale locale) {
        config.setLocale(locale);
    } // set target api

}
