package com.invetechs.mamamiavendorversion.Language;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewCompat;

import me.anwarshahriar.calligrapher.Calligrapher;

public class Language extends FragmentActivity
{
    protected SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLanguage();


    } // on create function

    private void initLanguage() {

        if (sharedPreferences.getString("language","ar").equals("en"))
        {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }

        else  if (sharedPreferences.getString("language","ar").equals("ar")){
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        sharedPreferences = newBase.getSharedPreferences("user",MODE_PRIVATE);

        super.attachBaseContext(new MyContextWrapper(newBase).wrap(sharedPreferences.getString("language","ar")));

    } // set default language to arabic
}
