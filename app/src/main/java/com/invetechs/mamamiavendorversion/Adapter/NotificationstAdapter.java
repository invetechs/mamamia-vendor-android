package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.NotificationsApi.SendNotification;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.NotificationResponse;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Activity.ClientBanquetOrderDetails;
import com.invetechs.mamamiavendorversion.View.Activity.OrderDetails;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class NotificationstAdapter extends RecyclerView.Adapter<NotificationstAdapter.MyViewHolder> {

    // vars
    Context context;
    private List<NotificationResponse.DataBean> arrayList;
    SharedPreferences sharedPreferences;
    String status_value;
    String notification_id = "";
    Handler handler;
    CustomDialogProgress progress;
    LinearLayout layout;
    ShowDialog showDialog = new ShowDialog();

    public NotificationstAdapter(Context context, List<NotificationResponse.DataBean> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_item, parent, false);

        return new MyViewHolder(view);
    } // on create function


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);


        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tv_title.setTypeface(font);
            holder.tv_time.setTypeface(font);
            holder.img_view.setRotation(holder.img_view.getRotation() + 180);


            if (arrayList.get(position).getStatus().equals("1") && arrayList.get(position).getTypeOrder().equals("banquet"))
            {
                holder.tv_title.setText(context.getString(R.string.win)  + " " +  arrayList.get(position).getPartyName());
                holder.tv_time.setText(arrayList.get(position).getAr_ago());

            }
            else
            {
                holder.tv_title.setText(arrayList.get(position).getAr_description());
                holder.tv_time.setText(arrayList.get(position).getAr_ago());

            }

        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tv_title.setTypeface(font);
            holder.tv_time.setTypeface(font);

            if (arrayList.get(position).getStatus().equals("1") && arrayList.get(position).getTypeOrder().equals("banquet"))
            {
                holder.tv_title.setText(context.getString(R.string.win)  + " " +  arrayList.get(position).getPartyName());
                holder.tv_time.setText(arrayList.get(position).getAr_ago());

            }
            else
            {
                holder.tv_title.setText(arrayList.get(position).getAr_description());
                holder.tv_time.setText(arrayList.get(position).getAr_ago());

            }

        }

        notification_id = arrayList.get(position).getId();


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (arrayList.get(position).getStatus().equals("0")) {
                 //   sendNotificationID(arrayList.get(position).getId());
                    Intent intent = new Intent(context, OrderDetails.class);
                    intent.putExtra("orderId", String.valueOf(arrayList.get(position).getOrder_id()));
                    intent.putExtra("archive", "0");
                    context.startActivity(intent);
                    ((Activity)context).finish();
                    Log.i("QP", "status = " + status_value);

                }



                if (arrayList.get(position).getStatus().equals("2")) {
                 //   sendNotificationID(arrayList.get(position).getId());
                    String order_number = String.valueOf(arrayList.get(position).getOrder_id());
                    Intent intent = new Intent(view.getContext(), ClientBanquetOrderDetails.class);
                    intent.putExtra("client_banquets_id", order_number);
                    intent.putExtra("archive", "0");
                    context.startActivity(intent);
                    ((Activity)context).finish();
                    Log.i("QP", "status = " + status_value);

                }


                if (arrayList.get(position).getStatus().equals("1") || arrayList.get(position).getStatus().equals("-1")) {
                  //  sendNotificationID(arrayList.get(position).getId());
                    Intent intent = new Intent(context, ClientBanquetOrderDetails.class);
                    String order_number = String.valueOf(arrayList.get(position).getOrder_id());
                    intent.putExtra("client_banquets_id", order_number);
                    intent.putExtra("archive", "1");
                    context.startActivity(intent);
                    ((Activity)context).finish();
                    Log.i("QP", "status = " + status_value);

                }
            }
        });

    }

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void sendNotificationID(final String notId) {
        progress = new CustomDialogProgress();
        progress.init(context);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();

        new Thread() {
            public void run() {

                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final SendNotification notificationApi = retrofit.create(SendNotification.class);

                final Call<NotificationResponse> getInterestConnection = notificationApi.sendNotification(notId);

                Log.i("QP", "notification_id : " + notId);


                getInterestConnection.enqueue(new Callback<NotificationResponse>() {
                    @Override
                    public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                        try {
                            String code = response.body().getCode();

                            Log.i("QP", "code" + code);

                            response.body();

                            Log.i("QP", "response" + response.body().getCode());

                            if (code.equals("200")) {

                                Log.i("QP", "notification_id = " + notification_id);

                            } // login success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<NotificationResponse> call, Throwable t) {
                        showDialog.initDialog(context.getString(R.string.retry), context);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
            }

        }.start();
    } // show notifications id

    @Override
    public int getItemCount() {
        return arrayList.size();
    } // get items count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_time)
        TextView tv_time;

        @BindView(R.id.tv_title)
        TextView tv_title;

        @BindView(R.id.img_view)
        ImageView img_view;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    } // view holder class
}
