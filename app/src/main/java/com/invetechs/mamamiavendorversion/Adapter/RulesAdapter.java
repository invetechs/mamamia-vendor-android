package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.RulesResultModel;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.Model.Class.Model.RulesModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

import static android.content.Context.MODE_PRIVATE;

public class RulesAdapter extends RecyclerView.Adapter<RulesAdapter.MyViewHolder> {

    // vars
    Context context;
    List<RulesResultModel.AllrulesBean>   rulesModelArrayList ;
    Boolean isClicked;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public RulesAdapter(Context context, List<RulesResultModel.AllrulesBean> rulesModelArrayList) {
        this.context = context;
        this.rulesModelArrayList = rulesModelArrayList;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
            View view = LayoutInflater.from(context).inflate(R.layout.rules_item,parent,false);
            return  new MyViewHolder(view);
    } // function oncreate view

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tvOrderNumber.setText(rulesModelArrayList.get(position).getAr_title());
            holder.tvPrice.setTypeface(font);
            holder.tvPointsNum.setTypeface(font);
            holder.tv_discount.setTypeface(font);
            holder.tvOrderNumber.setTypeface(font);
            holder.price_title.setTypeface(font);
            holder.points_title.setTypeface(font);
            holder.discount_title.setTypeface(font);

        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tvOrderNumber.setText(rulesModelArrayList.get(position).getEn_title());
            holder.tvPrice.setTypeface(font);
            holder.tvPointsNum.setTypeface(font);
            holder.tv_discount.setTypeface(font);
            holder.price_title.setTypeface(font);
            holder.points_title.setTypeface(font);
            holder.discount_title.setTypeface(font);
        }

        holder.tvPrice.setText(rulesModelArrayList.get(position).getRule()+ "  " + context.getResources().getString(R.string.sr));
        holder.tvPointsNum.setText(rulesModelArrayList.get(position).getPoints()+"  " + context.getResources().getString(R.string.pointss));
        holder.tv_discount.setText(rulesModelArrayList.get(position).getDiscount() + "  " + context.getResources().getString(R.string.sr));
        isClicked = true;
        holder.childLinear.setVisibility(View.GONE);
        holder.parentLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClicked == true)
                {
                    holder.childLinear.setVisibility(View.VISIBLE);
                    isClicked = false;
                }

                else if (isClicked == false)
                {
                    holder.childLinear.setVisibility(View.GONE);
                    isClicked = true;
                }
            }
        });

    } // function on bind

    @Override
    public int getItemCount() {
        Log.i("QP","sizeArray : " + rulesModelArrayList.size());
        return rulesModelArrayList.size();
    } // function get count of items

    public class MyViewHolder extends RecyclerView.ViewHolder {

        //bind view
        @BindView(R.id.tv_order_number)
        TextView tvOrderNumber;

        @BindView(R.id.price_title)
        TextView price_title;

        @BindView(R.id.discount_title)
        TextView discount_title;

        @BindView(R.id.points_title)
        TextView points_title;

        @BindView(R.id.parent_linear)
        LinearLayout parentLinear;

        @BindView(R.id.child_linear)
        LinearLayout childLinear;

        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.tv_points_num)
        TextView tvPointsNum;

        @BindView(R.id.tv_discount)
        TextView tv_discount;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    } // view holder class
}
