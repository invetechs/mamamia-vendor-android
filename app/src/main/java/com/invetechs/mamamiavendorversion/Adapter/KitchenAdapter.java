package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.invetechs.mamamiavendorversion.Model.Class.Model.Branch;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.SelectAllKitchenResultModel;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Activity.KitchenDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

import static android.content.Context.MODE_PRIVATE;

public class KitchenAdapter extends RecyclerView.Adapter<KitchenAdapter.MyViewHolder> {

    // vars
    Context context;
    List<SelectAllKitchenResultModel.KitchensdataBean> kitchen_arraylist;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public KitchenAdapter(Context context, List<SelectAllKitchenResultModel.KitchensdataBean> kitchen_arraylist) {
        this.context = context;
        this.kitchen_arraylist = kitchen_arraylist;
    } // constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.kitchen_item, parent, false);
        sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(context);
            calligrapher.setFont((Activity) context, "fonts/GESSTwoMedium.otf", true);

        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(context);
            calligrapher.setFont((Activity) context, "fonts/OpenSans-Regular.ttf", true);

        }

        return new MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

      Glide.with(context)
                .load("https://mammamiaa.com/cpanel/upload/kitchen/" +kitchen_arraylist.get(position).getImage())
                .into(holder.kitchenImage);

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context). getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.kitchenTitle.setText(kitchen_arraylist.get(position).getAr_title());
            holder.kitchenTitle.setTypeface(font);
        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.kitchenTitle.setText(kitchen_arraylist.get(position).getEn_title());
            holder.kitchenTitle.setTypeface(font);

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, KitchenDetails.class);
                 intent.putExtra("kitchenId",kitchen_arraylist.get(position).getId());
                context.startActivity(intent);
            }
        });

    } // on bind view function

    @Override
    public int getItemCount() {
        return kitchen_arraylist.size();
    } // get item count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.kitchen_image)
        ImageView kitchenImage;

        @BindView(R.id.kitchen_title)
        TextView kitchenTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    } // view holder
}
