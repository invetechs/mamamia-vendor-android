package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.PointsResultModel;
import com.invetechs.mamamiavendorversion.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

import static android.content.Context.MODE_PRIVATE;

public class PointAdapter extends RecyclerView.Adapter<PointAdapter.MyViewHolder>
{

    // vars
    Context context ;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    List<PointsResultModel.AlluserspointBean> pointsList ;

    public PointAdapter(Context context, List<PointsResultModel.AlluserspointBean> pointsLis)
    {
        this.context = context ;
        this.pointsList = pointsLis ;

    } // constructor

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemPointName)
        TextView tx_itemPointName;

        @BindView(R.id.itemPointMobileNumber)
        TextView tx_itemPointMobileNumber;

        @BindView(R.id.itemPointPoints)
        TextView tx_itemPointPoints;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    } // view holder class

    @NonNull
    @Override
    public PointAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.point_item, parent, false);
        return new PointAdapter.MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull PointAdapter.MyViewHolder holder, int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tx_itemPointName.setTypeface(font);
            holder.tx_itemPointMobileNumber.setTypeface(font);
            holder.tx_itemPointPoints.setTypeface(font);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tx_itemPointName.setTypeface(font);
            holder.tx_itemPointMobileNumber.setTypeface(font);
            holder.tx_itemPointPoints.setTypeface(font);

        }

        holder.tx_itemPointName.setText(pointsList.get(position).getUser().getName());
        holder.tx_itemPointMobileNumber.setText(pointsList.get(position).getUser().getMobilenumber());
        holder.tx_itemPointPoints.setText(pointsList.get(position).getPoints()+"");
    } // on bind view function

    @Override
    public int getItemCount() {
        return pointsList.size();
    } // get item count


}
