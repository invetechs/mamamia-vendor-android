package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.AllOrdersResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ArchiveResultModel;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Activity.ClientBanquetOrderDetails;
import com.invetechs.mamamiavendorversion.View.Activity.OrderDetails;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

import static android.content.Context.MODE_PRIVATE;

public class ArchivesAdapter extends RecyclerView.Adapter<ArchivesAdapter.MyViewHolder> {
    // vars
    Context context;
    ArrayList<String> spacecrafts;
    String flag = "";
    SharedPreferences sharedPreferences;
    List<ArchiveResultModel.ArchivesBean> allorders;
    SharedPreferences.Editor editor;
    String location = "", location_customer = "";


    public ArchivesAdapter(Context context, ArrayList<String> spacecrafts, List<ArchiveResultModel.ArchivesBean> allorders, String flag) {
        this.context = context;
        this.allorders = allorders;
        this.flag = flag;
        this.spacecrafts = spacecrafts;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemOrder_orderName)
        TextView tx_orderName;

        @BindView(R.id.itemOrder_orderNumber)
        TextView tx_orderNumber;

        @BindView(R.id.order_name_title)
        TextView order_name_title;

        @BindView(R.id.price_title)
        TextView price_title;

        @BindView(R.id.itemOrder_customerLocation)
        TextView tx_customerLocation;

        @BindView(R.id.itemOrder_cost)
        TextView tx_cost;

        @BindView(R.id.location_title)
        TextView location_title;

        @BindView(R.id.tx_accept)
        TextView tx_accept;

        @BindView(R.id.tx_reject)
        TextView tx_reject;


        @BindView(R.id.itemOrder_layout)
        LinearLayout layout_item;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    } // class  of MyViewHolder

    @NonNull
    @Override
    public ArchivesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.archives_item, parent, false);
        return new ArchivesAdapter.MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull final ArchivesAdapter.MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tx_orderName.setTypeface(font);
            holder.tx_orderName.setTypeface(font);
            holder.tx_orderName.setEllipsize(TextUtils.TruncateAt.START);
            holder.tx_customerLocation.setTypeface(font);
            holder.tx_cost.setTypeface(font);
            holder.tx_accept.setTypeface(font);
            holder.tx_reject.setTypeface(font);
            holder.tx_orderNumber.setTypeface(font);
            holder.tx_customerLocation.setTypeface(font);
            holder.tx_cost.setTypeface(font);
            //  holder.order_title.setTypeface(font);
            holder.order_name_title.setTypeface(font);
            holder.location_title.setTypeface(font);
            holder.price_title.setTypeface(font);

            if (allorders.get(position).getType().equals("meal")) {
                holder.tx_orderName.setText(allorders.get(position).getAr_title());

            } // order Details

            else if (allorders.get(position).getType().equals("banquet")) {
                holder.tx_orderName.setText(allorders.get(position).getEn_partyname());
            } // client banquet order details

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tx_orderNumber.setTypeface(font);
            holder.tx_orderName.setTypeface(font);
            holder.tx_orderName.setTypeface(font);
            holder.tx_orderName.setEllipsize(TextUtils.TruncateAt.END);
            holder.tx_customerLocation.setTypeface(font);
            holder.tx_cost.setTypeface(font);
            holder.tx_accept.setTypeface(font);
            holder.tx_reject.setTypeface(font);
            holder.tx_orderNumber.setTypeface(font);
            holder.tx_customerLocation.setTypeface(font);
            holder.tx_cost.setTypeface(font);
            // holder.order_title.setTypeface(font);
            holder.order_name_title.setTypeface(font);
            holder.location_title.setTypeface(font);
            holder.price_title.setTypeface(font);

            if (allorders.get(position).getType().equals("meal")) {
                holder.tx_orderName.setText(allorders.get(position).getEn_title());

            } // order Details
            else if (allorders.get(position).getType().equals("banquet")) {
                holder.tx_orderName.setText(allorders.get(position).getEn_partyname());

            } // client banquet order details


        }


        if (allorders.get(position).getStatus().equals("1")) {
            holder.tx_accept.setVisibility(View.VISIBLE);
            holder.tx_reject.setVisibility(View.GONE);

            holder.tx_accept.setText(R.string.accepted);

        } else if (allorders.get(position).getStatus().equals("-1")) {
            holder.tx_accept.setVisibility(View.GONE);
            holder.tx_reject.setVisibility(View.VISIBLE);

            holder.tx_reject.setText(R.string.rejected);
        }


        if (allorders.get(position).getType().equals("meal")) {
            location_customer = allorders.get(position).getUserplace();
        } // order Details
        else if (allorders.get(position).getType().equals("banquet")) {
            holder.location_title.setText(context.getString(R.string.banquet_location));
            location_customer = allorders.get(position).getPartylocation();


        } // client banquet order details

        location = "";
        for (int i = 0; i < 20; i++) {
            if (location_customer != null)
                if (location_customer.length() > 20) {
                    if (i == 19)
                        location = location + "...";
                    else
                        location = location + location_customer.charAt(i);
                } else if (location_customer.length() <= 20) {
                    location = location_customer;

                }
        }
        holder.tx_orderNumber.setText(String.valueOf(allorders.get(position).getId()));
        holder.tx_customerLocation.setText(location);
        holder.tx_cost.setText(allorders.get(position).getTotalprice());
        holder.layout_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (allorders.get(position).getType().equals("meal")) {
                    Intent intent = new Intent(view.getContext(), OrderDetails.class);
                    intent.putExtra("orderId", String.valueOf(allorders.get(position).getId()));
                    intent.putExtra("archive", "1");
                    view.getContext().startActivity(intent);
                } // order Details

                else {
                    String order_number = String.valueOf(allorders.get(position).getId());
                    Intent intent = new Intent(view.getContext(), ClientBanquetOrderDetails.class);
                    intent.putExtra("client_banquets_id", order_number);
                    intent.putExtra("archive", "1");
                    Log.i("QP", "recieve client_banquets_id : " + order_number);
                    view.getContext().startActivity(intent);
                } // client banquet order details
            }
        }); // click on order item
    } // on bind view function

    @Override
    public int getItemCount() {
        return allorders.size();
    } // get item count
}
