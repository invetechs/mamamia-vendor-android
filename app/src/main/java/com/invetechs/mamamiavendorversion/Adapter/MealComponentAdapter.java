package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.OneOrderResultModel;
import com.invetechs.mamamiavendorversion.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class MealComponentAdapter extends RecyclerView.Adapter<MealComponentAdapter.MyViewHolder> {

    // vars
    Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    StringBuilder my_additions;

    List<OneOrderResultModel.OneorderBean.MealsBean> meals = new ArrayList<>();
    List<OneOrderResultModel.OneorderBean.MealsBean.AdditionsBean> additions = new ArrayList<>();

    public MealComponentAdapter(Context context, List<OneOrderResultModel.OneorderBean.MealsBean> meals) {
        this.context = context;
        this.meals = meals;
    }

    public MealComponentAdapter(Context context, List<OneOrderResultModel.OneorderBean.MealsBean> meals, List<OneOrderResultModel.OneorderBean.MealsBean.AdditionsBean> additions) {
        this.context = context;
        this.meals = meals;
        this.additions = additions;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemName)
        TextView mealName;

        @BindView(R.id.itemQuantity)
        TextView mealAmount;

        @BindView(R.id.additions)
        TextView additions;


        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    } // class of MyViewHolder

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_details_item, parent, false);
        return new MealComponentAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            //  ViewCompat.setLayoutDirection(((Activity) context). getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.mealName.setTypeface(font);
            holder.mealAmount.setTypeface(font);
            holder.additions.setTypeface(font);

            try {
                holder.mealName.setText(meals.get(position).getAr_title());

                additions = meals.get(position).getAdditions();

                my_additions = new StringBuilder();

                for (int i = 0; i < additions.size(); i++) {

                    my_additions.append(additions.get(i).getAr_title());

                    if (i != additions.size() - 1) {

                        my_additions.append(" - ");

                    }

                }

                holder.additions.setText("( " + my_additions.toString() + " )");

                Log.i("QP", "length : " + my_additions.length());
            } catch (Exception e) {
                Log.i("QP", "Exception : " + e.getMessage());
            }
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.mealName.setTypeface(font);
            holder.mealAmount.setTypeface(font);
            holder.additions.setTypeface(font);

            try {
                holder.mealName.setText(meals.get(position).getEn_title());
                additions = meals.get(position).getAdditions();

                my_additions = new StringBuilder();

                for (int i = 0; i < additions.size(); i++) {

                    my_additions.append(additions.get(i).getEn_title());

                    if (i != additions.size() - 1) {

                        my_additions.append(" - ");

                    }

                }
                holder.additions.setText("( " + my_additions.toString() + " )");
                Log.i("QP", "success get data additions : " + meals.get(position).getAdditions().get(position).getEn_title());


            } catch (Exception e) {
                Log.i("QP", "Exception : " + e.getMessage());

            }

        }

        holder.mealAmount.setText(meals.get(position).getAmount());
    }

    @Override
    public int getItemCount() {

        Log.i("Array", "meal array list size: " + meals.size());

        return meals.size();
    }


}
