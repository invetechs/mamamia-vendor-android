package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ArchiveResultModel;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Activity.ClientBanquetOrderDetails;
import com.invetechs.mamamiavendorversion.View.Activity.OrderDetails;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

import static android.content.Context.MODE_PRIVATE;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    // vars
    Context context;
    ArrayList<String> orderNameList ;
    ArrayList<String> orderNumberList ;
    ArrayList<String> customerLocationList;
    ArrayList<String> costList;
    String flag = "";
    SharedPreferences sharedPreferences;
    ArrayList<String> spacecrafts;
    SharedPreferences.Editor editor;
    String location = "" , name = "";


    public OrderAdapter(Context context, ArrayList<String> spacecrafts, ArrayList<String> orderNameList, ArrayList<String> orderNumberList
            , ArrayList<String> customerLocationList, ArrayList<String> costList, String flag) {
        this.context = context;
        this.spacecrafts = spacecrafts;
        this.orderNameList = orderNameList;
        this.orderNumberList = orderNumberList;
        this.customerLocationList = customerLocationList;
        this.costList = costList;
        this.flag = flag;
    } // constructor of OrderAdapter


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemOrder_orderName)
        TextView tx_orderName;

        @BindView(R.id.itemOrder_orderNumber)
        TextView tx_orderNumber;

        @BindView(R.id.itemOrder_customerLocation)
        TextView tx_customerLocation;

        @BindView(R.id.itemOrder_cost)
        TextView tx_cost;

        @BindView(R.id.location_title)
        TextView location_title;

        @BindView(R.id.price_title)
        TextView price_title;

        @BindView(R.id.itemOrder_layout)
        LinearLayout layout_item;

        @BindView(R.id.btn_status)
        Button btn_status;

        @BindView(R.id.order_name_title)
        TextView order_name_title;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    } // class  of MyViewHolder

    @NonNull
    @Override
    public OrderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_item, parent, false);


        return new OrderAdapter.MyViewHolder(view);
    } // on create function

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tx_orderName.setTypeface(font);
            holder.tx_orderName.setEllipsize(TextUtils.TruncateAt.START);
            holder.tx_orderNumber.setTypeface(font);
            holder.tx_customerLocation.setTypeface(font);
            holder.tx_cost.setTypeface(font);
            holder.btn_status.setTypeface(font);
            holder.location_title.setTypeface(font);
            holder.order_name_title.setTypeface(font);
            holder.price_title.setTypeface(font);

        }
        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tx_orderName.setTypeface(font);
            holder.tx_orderName.setEllipsize(TextUtils.TruncateAt.END);
            holder.tx_orderNumber.setTypeface(font);
            holder.tx_customerLocation.setTypeface(font);
            holder.tx_cost.setTypeface(font);
            holder.btn_status.setTypeface(font);
            holder.order_name_title.setTypeface(font);
            holder.location_title.setTypeface(font);
            holder.price_title.setTypeface(font);

        }
        holder.tx_orderNumber.setText(orderNumberList.get(position));


        String order_name = orderNameList.get(position);
        name = "";

        for (int i = 0 ; i < 20 ; i++)
        {
            if (order_name != null)
                if (order_name.length() > 20)
                {
                    if (i == 19)
                    {
                        name = name + "...";
                    }
                    else
                        name = name + order_name.charAt(i);
                }
                else if (order_name.length() <= 20)
                {
                    name = order_name;
                }
        }
        holder.tx_orderName.setText(name);

        String location_customer =  customerLocationList.get(position);

        location = "";
        for (int i = 0 ; i < 20 ; i++)
        {
            if (location_customer != null)
            if(location_customer.length() > 20)
            {
                if(i == 19)
                    location = location+"...";
                   else
                location = location + location_customer.charAt(i);
            }

            else if (location_customer.length() <= 20)
            {
               location = location_customer;

            }
        }

        holder.tx_customerLocation.setText(location);
        holder.tx_cost.setText(costList.get(position));
        holder.layout_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (flag.equals("order")) {
                    Intent intent = new Intent(view.getContext(), OrderDetails.class);
                    intent.putExtra("orderId", orderNumberList.get(position));
                    intent.putExtra("archive", "0");
                    view.getContext().startActivity(intent);
                } // order Details
                else if (flag.equals("banquet")) {
                    Intent intent = new Intent(view.getContext(), ClientBanquetOrderDetails.class);
                    view.getContext().startActivity(intent);
                } // client banquet order details
            }
        }); // click on order item
    } // on bind view function

    @Override
    public int getItemCount() {
        return orderNameList.size();
    } // get item count


} // class of OrderAdapter