package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.Hours;
import com.invetechs.mamamiavendorversion.R;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

import static android.content.Context.MODE_PRIVATE;
import static java.security.AccessController.getContext;

public class KitchenDetailAdapter extends RecyclerView.Adapter<KitchenDetailAdapter.MyViewHolder> {

    // vars
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;
    FromToAdapter adapter;
    ArrayList<String> dayIdList;
    ArrayList<ArrayList<Hours.FromToClass>> allFromToList;

    public KitchenDetailAdapter(Context context, ArrayList<ArrayList<Hours.FromToClass>> allFromToList, ArrayList<String> dayIdList) {
        this.context = context;
        this.allFromToList = allFromToList;
        this.dayIdList = dayIdList;
    } // constructor


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.kitchen_detail_item, parent, false);
        return new MyViewHolder(view);
    } // on create function


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context). getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tvDay.setTypeface(font);
            holder.fromToText.setTypeface(font);
        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tvDay.setTypeface(font);
            holder.fromToText.setTypeface(font);

        }


        Log.i("QA","size in adapter : "+allFromToList.get(position).size());

        String text="";
        for(int i=0;i<allFromToList.get(position).size();i++)
        {
            text= text + (context.getResources().getString(R.string.from)+": "+allFromToList.get(position).get(i).getFrom()+" - "+
                    context.getResources().getString(R.string.to)+": ")+allFromToList.get(position).get(i).getTo()+"\n";
        }
        holder.fromToText.setText(text);

        if(dayIdList.get(position).equals("0"))
        {
            holder.tvDay.setText(R.string.saturday);
        } // saturday
        else if (dayIdList.get(position).equals("1")) {
            holder.tvDay.setText(R.string.sunday);
        } // sunday
        else if (dayIdList.get(position).equals("2")) {
            holder.tvDay.setText(R.string.monday);
        } // monday
        else if (dayIdList.get(position).equals("3")) {
            holder.tvDay.setText(R.string.tuesday);
        } // tuesday
        else if (dayIdList.get(position).equals("4")) {
            holder.tvDay.setText(R.string.wednesday);
        } // wednesday
        else if (dayIdList.get(position).equals("5")) {
            holder.tvDay.setText(R.string.thursday);
        } // thursday
        else if (dayIdList.get(position).equals("6")) {
            holder.tvDay.setText(R.string.friday);
        } // friday


    } // on bind view function

    @Override
    public int getItemCount() {
        return dayIdList.size();
    } // get item count

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_day)
        TextView tvDay;

        @BindView(R.id.fromToText)
        TextView fromToText;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    } // view holder


}
