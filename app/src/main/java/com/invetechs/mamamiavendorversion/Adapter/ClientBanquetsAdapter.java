package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi.RejectOrderApi;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BanquetsResutlModel;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Activity.ClientBanquetOrderDetails;
import com.invetechs.mamamiavendorversion.View.Activity.HomePageScreen;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import static android.content.Context.MODE_PRIVATE;

public class ClientBanquetsAdapter extends RecyclerView.Adapter<ClientBanquetsAdapter.MyViewHolder> {
    // vars
    Context context;
    String flag = "";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    List<BanquetsResutlModel.BanquetsBean> clientBanquets;
    ArrayList<String> spacecrafts;
    CustomDialogProgress progress;
    Handler handler;
    Dialog acceptAndRejectDialog;
    LinearLayout layout;
    TextView message;
    String location;
    String location_customer = "";

    public ClientBanquetsAdapter(Context context, ArrayList<String> spacecrafts, List<BanquetsResutlModel.BanquetsBean> clientBanquets, String flag) {
        this.context = context;
        this.spacecrafts = spacecrafts;
        this.flag = flag;
        this.clientBanquets = clientBanquets;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemOrder_orderName)
        TextView tx_orderName;

        @BindView(R.id.order_num_title)
        TextView order_num_title;

        @BindView(R.id.itemOrder_orderNumber)
        TextView tx_orderNumber;

        @BindView(R.id.location_title)
        TextView location_title;

        @BindView(R.id.itemOrder_customerLocation)
        TextView tx_customerLocation;

        @BindView(R.id.bt_view)
        Button bt_view;

        @BindView(R.id.bt_reject)
        Button bt_reject;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    } // class  of MyViewHolder


    @NonNull
    @Override
    public ClientBanquetsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.banquet_item, parent, false);
        initAcceptAndRejectDialog();
        return new MyViewHolder(view);
    } // on create function

    private void initAcceptAndRejectDialog() {
        acceptAndRejectDialog = new Dialog(context);
        acceptAndRejectDialog.setContentView(R.layout.accept_refuse_dialog);
        layout = acceptAndRejectDialog.findViewById(R.id.layoutDialog);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        acceptAndRejectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        acceptAndRejectDialog.getWindow();
        message = acceptAndRejectDialog.findViewById(R.id.tv_message);

    }

    @Override
    public void onBindViewHolder(@NonNull ClientBanquetsAdapter.MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tx_orderName.setText(clientBanquets.get(position).getAr_partyname());
            holder.tx_orderNumber.setTypeface(font);
            holder.tx_orderName.setEllipsize(TextUtils.TruncateAt.START);
            holder.tx_customerLocation.setTypeface(font);
            holder.tx_orderName.setTypeface(font);
            holder.location_title.setTypeface(font);
            holder.order_num_title.setTypeface(font);
            holder.bt_view.setTypeface(font);
            holder.tx_orderName.setText(clientBanquets.get(position).getEn_partyname());
            holder.bt_reject.setTypeface(font);
        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tx_orderNumber.setTypeface(font);
            holder.tx_customerLocation.setTypeface(font);
            holder.tx_orderName.setEllipsize(TextUtils.TruncateAt.END);
            holder.tx_orderName.setTypeface(font);
            holder.order_num_title.setTypeface(font);
            holder.location_title.setTypeface(font);
            holder.bt_view.setTypeface(font);
            holder.bt_reject.setTypeface(font);
            holder.tx_orderName.setText(clientBanquets.get(position).getEn_partyname());

        }

        holder.tx_orderNumber.setText(String.valueOf(clientBanquets.get(position).getId()));

        Log.i("QP", "customer location: " + holder.tx_customerLocation);

        location_customer = clientBanquets.get(position).getPartylocation();
        location = "";

        for (int i = 0; i < 20; i++) {
            if (location_customer != null)
                if (location_customer.length() > 20) {
                    if (i == 19)
                        location = location + "...";
                    else
                        location = location + location_customer.charAt(i);
                } else if (location_customer.length() <= 20) {
                    location = location_customer;
                }


        }

        holder.tx_customerLocation.setText(location);


        holder.bt_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (flag.equals("banquet")) {
                    String order_number = String.valueOf(clientBanquets.get(position).getId());
                    Intent intent = new Intent(view.getContext(), ClientBanquetOrderDetails.class);
                    intent.putExtra("client_banquets_id", order_number);
                    intent.putExtra("archive", "0");
                    Log.i("QP", "send client_banquets_id : " + order_number);

                    context.startActivity(intent);
                } // client banquet order details


            }
        }); // click on order item

        holder.bt_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejectOrder(String.valueOf(clientBanquets.get(position).getId()));

            }
        });
    } // on bind view function

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    public void rejectOrder(final String order_number) {
        progress = new CustomDialogProgress();
        progress.init(context);
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };

        new Thread() {
            public void run() {
//Retrofit

                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final RejectOrderApi orderApi = retrofit.create(RejectOrderApi.class);

                final Call<ResultModelUser> getInterestConnection = orderApi.rejectOrderApi(order_number);

                getInterestConnection.enqueue(new Callback<ResultModelUser>() {
                    @Override
                    public void onResponse(Call<ResultModelUser> call, Response<ResultModelUser> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);
                            if (code.equals("200")) {
                                acceptAndRejectDialog.show();
                                message.setText(context.getString(R.string.orderRejected));
                                Intent intent = new Intent(context, HomePageScreen.class);
                                intent.putExtra("fragment", "banquet");
                                context.startActivity(intent);
                                ((Activity) context).finish();

                            } //  success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<ResultModelUser> call, Throwable t) {

                        Toast.makeText(context, context.getString(R.string.retry),
                                Toast.LENGTH_LONG).show();
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

            }

        }.start();
    } // button click reject Order


    @Override
    public int getItemCount() {

        return clientBanquets.size();

    } // get item count


} // class of OrderAdapter
