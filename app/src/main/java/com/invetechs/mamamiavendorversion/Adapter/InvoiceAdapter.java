package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ArchiveResultModel;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Activity.InvoiceDetailsBanquetsActivity;
import com.invetechs.mamamiavendorversion.View.Activity.InvoiceDetailsOrdersActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {

    //vars
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;
    ArrayList<String> spacecrafts;
    List<ArchiveResultModel.ArchivesBean> invoices;
    String type = "";

    public InvoiceAdapter(Context context, ArrayList<String> spacecrafts, List<ArchiveResultModel.ArchivesBean> invoices) {
        this.context = context;
        this.spacecrafts = spacecrafts;
        this.invoices = invoices;
    } //Constructor

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.invoice_item, parent, false);
        return new MyViewHolder(view);

    } //onCreateView Function


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            holder.tvDate.setTypeface(font);
            holder.tvInvoicePrice.setTypeface(font);
            holder.tvOrderNumber.setTypeface(font);
            holder.order_text.setTypeface(font);
            holder.date_title.setTypeface(font);
            holder.total_title.setTypeface(font);
            holder.tvDate.setText((context.getString(R.string.date)) + "  " + invoices.get(position).getEn_orderDate() + " " + invoices.get(position).getEn_orderTime());
        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(((Activity) context).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.tvDate.setTypeface(font);
            holder.tvInvoicePrice.setTypeface(font);
            holder.tvOrderNumber.setTypeface(font);
            holder.order_text.setTypeface(font);
            holder.date_title.setTypeface(font);
            holder.total_title.setTypeface(font);
            holder.tvDate.setText((context.getString(R.string.date)) + "  " + invoices.get(position).getEn_orderDate() + " " + invoices.get(position).getEn_orderTime());

        }

        holder.tvInvoicePrice.setText(context.getString(R.string.invoice_price) + "  " + invoices.get(position).getTotalprice());
        holder.tvOrderNumber.setText(String.valueOf(invoices.get(position).getId()));

        type = invoices.get(position).getType();

        if (type.equals("meal")) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String order_number = String.valueOf(invoices.get(position).getId());
                    String order_date = invoices.get(position).getEn_title();
                    Intent invoiceDetail = new Intent(context, InvoiceDetailsOrdersActivity.class);
                    invoiceDetail.putExtra("invoice_id", order_number);
                    invoiceDetail.putExtra("invoice_date", order_date);
                    invoiceDetail.putExtra("flag", "view");
                    Log.i("QP", "send order number : " + order_number);
                    context.startActivity(invoiceDetail);
                }
            }); // view

//            holder.bt_print.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String order_number =  String.valueOf(invoices.get(position).getId());
//                    String order_date = invoices.get(position).getEn_title();
//                    Intent invoiceDetail = new Intent(context,InvoiceDetailsOrdersActivity.class);
//                    invoiceDetail.putExtra("invoice_id",order_number);
//                    invoiceDetail.putExtra("invoice_date",order_date);
//                    invoiceDetail.putExtra("flag","print");
//                    Log.i("QP","send order number : "  + order_number);
//                    context.startActivity(invoiceDetail);
//                }
//            }); // print

        } // meal
        else if (type.equals("banquet")) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String order_number = String.valueOf(invoices.get(position).getId());
                    String order_date = invoices.get(position).getEn_title();
                    Intent invoiceDetail = new Intent(context, InvoiceDetailsBanquetsActivity.class);
                    invoiceDetail.putExtra("invoice_id", order_number);
                    invoiceDetail.putExtra("invoice_date", order_date);
                    invoiceDetail.putExtra("flag", "view");
                    Log.i("QP", "send order number : " + order_number);
                    context.startActivity(invoiceDetail);
                }
            }); // view

//            holder.bt_print.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String order_number =  String.valueOf(invoices.get(position).getId());
//                    String order_date = invoices.get(position).getEn_title();
//                    Intent invoiceDetail = new Intent(context,InvoiceDetailsBanquetsActivity.class);
//                    invoiceDetail.putExtra("invoice_id",order_number);
//                    invoiceDetail.putExtra("invoice_date",order_date);
//                    invoiceDetail.putExtra("flag","print");
//                    Log.i("QP","send order number : "  + order_number);
//                    context.startActivity(invoiceDetail);
//                }
//            }); // print
        } // banquet

    } //onBindView Function

    @Override
    public int getItemCount() {
        return invoices.size();
    } // getItem count function

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_date)
        TextView tvDate;

        @BindView(R.id.total_title)
        TextView total_title;

        @BindView(R.id.order_text)
        TextView order_text;

        @BindView(R.id.date_title)
        TextView date_title;

        @BindView(R.id.tv_invoice_price)
        TextView tvInvoicePrice;

        @BindView(R.id.tv_order_number)
        TextView tvOrderNumber;

//        @BindView(R.id.bt_print)
//        ImageButton bt_print ;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    } //view holder function
}
