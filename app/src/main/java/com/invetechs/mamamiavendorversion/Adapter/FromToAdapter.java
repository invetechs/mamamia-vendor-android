package com.invetechs.mamamiavendorversion.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.Hours;
import com.invetechs.mamamiavendorversion.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

import static android.content.Context.MODE_PRIVATE;

public class FromToAdapter extends RecyclerView.Adapter<FromToAdapter.MyViewHolder>
{

    SharedPreferences sharedPreferences;
    Context context;
    ArrayList<Hours.FromToClass> FromToList = new ArrayList<>();

    public FromToAdapter(Context context, ArrayList<Hours.FromToClass> FromToList)
    {
        this.context = context;
        this.FromToList = FromToList;
    } // constructor
    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_from)
        TextView tvFrom;

        @BindView(R.id.tv_to)
        TextView tvTo;
        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);

        }
    } // class of MyViewHolder

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.from_to_item , parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Log.i("QA","from : " + FromToList.get(position).getFrom()
        +"   to : "+FromToList.get(position).getTo());



        holder.tvFrom.setText(FromToList.get(position).getFrom());
        holder.tvTo.setText(FromToList.get(position).getTo());
    }

    @Override
    public int getItemCount() {

        return FromToList.size();
    }


} // class of FromToAdapter
