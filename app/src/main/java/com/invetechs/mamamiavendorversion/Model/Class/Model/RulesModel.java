package com.invetechs.mamamiavendorversion.Model.Class.Model;

public class RulesModel {

    private String rule_number;
    private String branches;
    private String price;
    private String points_number;


    public RulesModel() {
    }

    public RulesModel(String rule_number, String branches, String price, String points_number) {
        this.rule_number = rule_number;
        this.branches = branches;
        this.price = price;
        this.points_number = points_number;
    }


    public String getRule_number() {
        return rule_number;
    }

    public String getBranches() {
        return branches;
    }

    public String getPrice() {
        return price;
    }

    public String getPoints_number() {
        return points_number;
    }
}
