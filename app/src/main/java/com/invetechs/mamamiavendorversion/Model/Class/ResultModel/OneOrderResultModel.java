package com.invetechs.mamamiavendorversion.Model.Class.ResultModel;

import java.util.List;

public class OneOrderResultModel {


    private String code;
    private String Status;
    private String message;
    private OneorderBean oneorder;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OneorderBean getOneorder() {
        return oneorder;
    }

    public void setOneorder(OneorderBean oneorder) {
        this.oneorder = oneorder;
    }

    public static class OneorderBean {


        private String id;
        private String component;
        private int user_id;
        private int vendor_id;
        private String lat;
        private String lng;
        private String userplace;
        private String orderdate;
        private String ordertime;
        private String usernote;
        private String vendornote;
        private String neededtime;
        private String deliverytotalprice;
        private String totalprice;
        private String distancetouser;
        private String mealstotalprice;
        private String status;
        private String type;
        private String en_partyname;
        private String ar_partyname;
        private String partydate;
        private String partytime;
        private String partytype;
        private String partylocation;
        private int guestnum;
        private String timetodeliver;
        private String priceVat;
        private String discountpoint;
        private String created_at;
        private String updated_at;
        private int kitchen_id;
        private UserBean user;
        private KitchenBean kitchen;
        private List<MealsBean> meals;

        public String getMealstotalprice() {
            return mealstotalprice;
        }

        public String getPriceVat() {
            return priceVat;
        }

        public String getDiscountpoint() {
            return discountpoint;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getComponent() {
            return component;
        }

        public void setComponent(String component) {
            this.component = component;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getVendor_id() {
            return vendor_id;
        }

        public void setVendor_id(int vendor_id) {
            this.vendor_id = vendor_id;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getUserplace() {
            return userplace;
        }

        public void setUserplace(String userplace) {
            this.userplace = userplace;
        }

        public String getOrderdate() {
            return orderdate;
        }

        public void setOrderdate(String orderdate) {
            this.orderdate = orderdate;
        }

        public String getOrdertime() {
            return ordertime;
        }

        public void setOrdertime(String ordertime) {
            this.ordertime = ordertime;
        }

        public String getUsernote() {
            return usernote;
        }

        public void setUsernote(String usernote) {
            this.usernote = usernote;
        }

        public String getVendornote() {
            return vendornote;
        }

        public void setVendornote(String vendornote) {
            this.vendornote = vendornote;
        }

        public String getNeededtime() {
            return neededtime;
        }

        public void setNeededtime(String neededtime) {
            this.neededtime = neededtime;
        }

        public String getDeliverytotalprice() {
            return deliverytotalprice;
        }

        public void setDeliverytotalprice(String deliverytotalprice) {
            this.deliverytotalprice = deliverytotalprice;
        }

        public String getTotalprice() {
            return totalprice;
        }

        public void setTotalprice(String totalprice) {
            this.totalprice = totalprice;
        }

        public String getDistancetouser() {
            return distancetouser;
        }

        public void setDistancetouser(String distancetouser) {
            this.distancetouser = distancetouser;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEn_partyname() {
            return en_partyname;
        }

        public void setEn_partyname(String en_partyname) {
            this.en_partyname = en_partyname;
        }

        public String getAr_partyname() {
            return ar_partyname;
        }

        public void setAr_partyname(String ar_partyname) {
            this.ar_partyname = ar_partyname;
        }

        public String getPartydate() {
            return partydate;
        }

        public void setPartydate(String partydate) {
            this.partydate = partydate;
        }

        public String getPartytime() {
            return partytime;
        }

        public void setPartytime(String partytime) {
            this.partytime = partytime;
        }

        public String getPartytype() {
            return partytype;
        }

        public void setPartytype(String partytype) {
            this.partytype = partytype;
        }

        public String getPartylocation() {
            return partylocation;
        }

        public void setPartylocation(String partylocation) {
            this.partylocation = partylocation;
        }

        public int getGuestnum() {
            return guestnum;
        }

        public void setGuestnum(int guestnum) {
            this.guestnum = guestnum;
        }

        public String getTimetodeliver() {
            return timetodeliver;
        }

        public void setTimetodeliver(String timetodeliver) {
            this.timetodeliver = timetodeliver;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getKitchen_id() {
            return kitchen_id;
        }

        public void setKitchen_id(int kitchen_id) {
            this.kitchen_id = kitchen_id;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public KitchenBean getKitchen() {
            return kitchen;
        }

        public void setKitchen(KitchenBean kitchen) {
            this.kitchen = kitchen;
        }

        public List<MealsBean> getMeals() {
            return meals;
        }

        public void setMeals(List<MealsBean> meals) {
            this.meals = meals;
        }

        public static class UserBean {
            /**
             * id : 17
             * name : Yasser Ahmed
             * email : null
             * mobilenumber : 0563258964
             * image : null
             * status : 0
             * isverified : 0
             * category : vendor
             * created_at : 2018-07-25 14:51:01
             * updated_at : 2018-07-25 14:51:01
             */

            private int id;
            private String name;
            private Object email;
            private String mobilenumber;
            private Object image;
            private String status;
            private String isverified;
            private String category;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getMobilenumber() {
                return mobilenumber;
            }

            public void setMobilenumber(String mobilenumber) {
                this.mobilenumber = mobilenumber;
            }

            public Object getImage() {
                return image;
            }

            public void setImage(Object image) {
                this.image = image;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getIsverified() {
                return isverified;
            }

            public void setIsverified(String isverified) {
                this.isverified = isverified;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class KitchenBean {
            /**
             * id : 38
             * ar_title : شاورما الريم
             * en_title : shawerma el reem
             * ar_description : dii
             * en_description : his had oj
             * kitchencategory_id : 2
             * user_id : 35
             * minicharge : 54
             * status : 0
             * openstatus : 1
             * deliveryflag : 1
             * deliverypriceperkilo : 0
             * delivermaxbill : 0
             * image :
             * phone : 468
             * phone2 : 666
             * address : cairo
             * lat : 1
             * lng : 1
             * remember_token : null
             * deleted_at : null
             * created_at : 2018-08-08 13:58:40
             * updated_at : 2018-08-08 13:58:40
             */

            private int id;
            private String ar_title;
            private String en_title;
            private String ar_description;
            private String en_description;
            private int kitchencategory_id;
            private int user_id;
            private String minicharge;
            private String status;
            private String openstatus;
            private String deliveryflag;
            private String deliverypriceperkilo;
            private String delivermaxbill;
            private String image;
            private String phone;
            private String phone2;
            private String address;
            private String lat;
            private String lng;
            private Object remember_token;
            private Object deleted_at;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getAr_title() {
                return ar_title;
            }

            public void setAr_title(String ar_title) {
                this.ar_title = ar_title;
            }

            public String getEn_title() {
                return en_title;
            }

            public void setEn_title(String en_title) {
                this.en_title = en_title;
            }

            public String getAr_description() {
                return ar_description;
            }

            public void setAr_description(String ar_description) {
                this.ar_description = ar_description;
            }

            public String getEn_description() {
                return en_description;
            }

            public void setEn_description(String en_description) {
                this.en_description = en_description;
            }

            public int getKitchencategory_id() {
                return kitchencategory_id;
            }

            public void setKitchencategory_id(int kitchencategory_id) {
                this.kitchencategory_id = kitchencategory_id;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public String getMinicharge() {
                return minicharge;
            }

            public void setMinicharge(String minicharge) {
                this.minicharge = minicharge;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getOpenstatus() {
                return openstatus;
            }

            public void setOpenstatus(String openstatus) {
                this.openstatus = openstatus;
            }

            public String getDeliveryflag() {
                return deliveryflag;
            }

            public void setDeliveryflag(String deliveryflag) {
                this.deliveryflag = deliveryflag;
            }

            public String getDeliverypriceperkilo() {
                return deliverypriceperkilo;
            }

            public void setDeliverypriceperkilo(String deliverypriceperkilo) {
                this.deliverypriceperkilo = deliverypriceperkilo;
            }

            public String getDelivermaxbill() {
                return delivermaxbill;
            }

            public void setDelivermaxbill(String delivermaxbill) {
                this.delivermaxbill = delivermaxbill;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getPhone2() {
                return phone2;
            }

            public void setPhone2(String phone2) {
                this.phone2 = phone2;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public Object getRemember_token() {
                return remember_token;
            }

            public void setRemember_token(Object remember_token) {
                this.remember_token = remember_token;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }


        public static class MealsBean {
            /**
             * ar_title : حاواوشى
             * en_title : hawashe
             * amount : 1
             * additions : [{"ar_title":"روززززوا","en_title":"rice"}]
             */

            private String ar_title;
            private String en_title;
            private String amount;
            private List<AdditionsBean> additions;

            public String getAr_title() {
                return ar_title;
            }

            public void setAr_title(String ar_title) {
                this.ar_title = ar_title;
            }

            public String getEn_title() {
                return en_title;
            }

            public void setEn_title(String en_title) {
                this.en_title = en_title;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public List<AdditionsBean> getAdditions() {
                return additions;
            }

            public void setAdditions(List<AdditionsBean> additions) {
                this.additions = additions;
            }

            public static class AdditionsBean {
                /**
                 * ar_title : روززززوا
                 * en_title : rice
                 */

                private String ar_title;
                private String en_title;

                public String getAr_title() {
                    return ar_title;
                }

                public void setAr_title(String ar_title) {
                    this.ar_title = ar_title;
                }

                public String getEn_title() {
                    return en_title;
                }

                public void setEn_title(String en_title) {
                    this.en_title = en_title;
                }
            }
        }
    }
} // class of OneOrderResultModel
