package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.ArchiveApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ArchiveResultModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface GetArchiveApi
{
    @FormUrlEncoded
    @POST("NewArchivesByVender")
    Call<ArchiveResultModel> getAllOrders(
            @Field("vendor_id") String id,
            @Field("offset") int offset,
            @Field("from") String from,
            @Field("to") String to,
            @Field("id") String orderId ,
            @Field("type") String type

    );
}

