package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.AllOrdersResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SelectAllOrderApi
{
    @FormUrlEncoded
    @POST("NewOrdersByVender")
    Call<AllOrdersResultModel> getAllOrders(
            @Field("vendor_id") String id,
            @Field("offset") int offset,
            @Field("from") String from,
            @Field("to") String to,
            @Field("id") String orderId
    );
}
