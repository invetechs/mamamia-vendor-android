package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.LoginApi;


import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginApi
{
    @FormUrlEncoded
    @POST("login")
    retrofit2.Call<ResultModelUser> login(@Field("mobilenumber") String mobilenumber,
                                          @Field("password") String password ,
                                          @Field("category") String category
    );  // user login

} // interface of LoginApi
