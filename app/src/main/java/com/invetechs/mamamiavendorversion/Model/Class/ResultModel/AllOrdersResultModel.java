package com.invetechs.mamamiavendorversion.Model.Class.ResultModel;

import java.util.List;

public class AllOrdersResultModel
{


    private String code;
    private String Status;
    private String message;
    private int count ;
    private List<NewOrdersBean> NewOrders;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NewOrdersBean> getNewOrders() {
        return NewOrders;
    }

    public void setNewOrders(List<NewOrdersBean> NewOrders) {
        this.NewOrders = NewOrders;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static class NewOrdersBean {
        /**
         * totalprice : 21133143123432
         * userplace : giza
         * id : 1
         * ar_title : وجبة شيش طاووق
         * count : 2
         * en_title : shesh tawok
         */

        private String totalprice;
        private String userplace;
        private int id;
        private String ar_title;
        private int count;
        private String en_title;

        public String getTotalprice() {
            return totalprice;
        }

        public void setTotalprice(String totalprice) {
            this.totalprice = totalprice;
        }

        public String getUserplace() {
            return userplace;
        }

        public void setUserplace(String userplace) {
            this.userplace = userplace;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }
    }
} // class of AllOrdersResultModel
