package com.invetechs.mamamiavendorversion.Model.Class.Model;

public class User
{

    String name , password , email , category ,  mobilenumber , id , image;
    double lat , lng ;

    public User() {} // default constructor

    public User(String mobilenumber, String name, String password, String email, String category, double lat, double lng) {
        this.mobilenumber = mobilenumber;
        this.name = name;
        this.password = password;
        this.email = email;
        this.category = category;
        this.lat = lat;
        this.lng = lng;
    } // param constructor




    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
} // class of User
