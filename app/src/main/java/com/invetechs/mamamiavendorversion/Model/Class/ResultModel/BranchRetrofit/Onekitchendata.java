package com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit;

import java.util.List;

public class Onekitchendata
{

    /**
     * code : 200
     * Status : success
     * message : kitchen selected
     * onekitchendata : {"id":140,"ar_title":"بيتزا كينج","en_title":"Pizza King","ar_description":"بيتزا كينج هو أحد مطاعم وجبات سريعة الأمريكية وله فروع في العديد من الدول حول العالم.و هو مطعم مختص بوجبات البيتزا بأنواعها ، الا انه يقدم وجبات أخرى.كما انه يقدم خدمة توصيل الطلبات الخارجية إضافة إلى وصول الأكل في المطعم","en_description":"Pizza King is an American fast food restaurant with branches in many countries around the world. It is a restaurant specializing in pizza meals of all kinds, but it offers other meals. It also offers external delivery service as well as eating at the restaurant","kitchencategory_id":1,"user_id":63,"minicharge":"50","status":"1","openstatus":"1","deliveryflag":"1","deliverypriceperkilo":"100","delivermaxbill":"100","image":null,"phone":"0111110100","phone2":null,"address":"3 st el kamal","lat":null,"lng":null,"remember_token":null,"deleted_at":null,"created_at":"2018-09-21 02:29:46","updated_at":"2018-09-24 00:23:20","hours":[{"id":2,"attribute_id":140,"day_id":0,"from":"13","to":"13","attribute_category":"kitchen","created_at":"2018-08-16 12:20:04","updated_at":"2018-08-16 12:20:04"},{"id":26,"attribute_id":140,"day_id":0,"from":"13","to":"13","attribute_category":"kitchen","created_at":"2018-08-27 20:19:22","updated_at":"2018-08-27 20:19:22"},{"id":27,"attribute_id":140,"day_id":0,"from":"13","to":"13","attribute_category":"kitchen","created_at":"2018-08-27 20:25:22","updated_at":"2018-08-27 20:25:22"}],"kitchencategory":{"id":1,"title":"Resturant","image":"","created_at":"2018-07-30 14:33:39","updated_at":"2018-07-30 14:38:04"},"kitchenimages":[{"id":55,"image":"18-08-12-11-37-1492e303be80661589cdc5a7cf003bdd37abd6379e.jpg","kitchen_id":140,"created_at":"2018-08-12 11:37:14","updated_at":"2018-08-12 11:37:14"},{"id":56,"image":"18-08-12-11-37-15bc89525811ceb466a857bab6441667012a44c7c7.jpg","kitchen_id":140,"created_at":"2018-08-12 11:37:15","updated_at":"2018-08-12 11:37:15"},{"id":60,"image":"18-08-27-20-19-29bc199cab35b038b5d55c9d5502f7817f77fb6c80.jpg","kitchen_id":140,"created_at":"2018-08-27 20:19:29","updated_at":"2018-08-27 20:19:29"},{"id":61,"image":"18-08-27-20-25-560c0fed704bbe55ef4a809c17ee7a4ed257fc2dcc.jpg","kitchen_id":140,"created_at":"2018-08-27 20:25:56","updated_at":"2018-08-27 20:25:56"}],"kitchentags":[{"id":1,"title":"Banquet ","image":"18-07-31-08-44-4464c51bb8166a734d32ee12279e09299a2112dacb.jpg","created_at":"2018-07-31 08:38:55","updated_at":"2018-07-31 08:44:44","pivot":{"kitchen_id":140,"kitchentag_id":1}},{"id":2,"title":"Candy","image":"18-07-31-08-44-4464c51bb8166a734d32ee12279e09299a2112dacb.jpg","created_at":"2018-07-31 08:38:55","updated_at":"2018-07-31 08:44:44","pivot":{"kitchen_id":140,"kitchentag_id":2}}]}
     */

    private String code;
    private String Status;
    private String message;
    private OnekitchendataBean onekitchendata;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OnekitchendataBean getOnekitchendata() {
        return onekitchendata;
    }

    public void setOnekitchendata(OnekitchendataBean onekitchendata) {
        this.onekitchendata = onekitchendata;
    }

    public static class OnekitchendataBean {
        /**
         * id : 140
         * ar_title : بيتزا كينج
         * en_title : Pizza King
         * ar_description : بيتزا كينج هو أحد مطاعم وجبات سريعة الأمريكية وله فروع في العديد من الدول حول العالم.و هو مطعم مختص بوجبات البيتزا بأنواعها ، الا انه يقدم وجبات أخرى.كما انه يقدم خدمة توصيل الطلبات الخارجية إضافة إلى وصول الأكل في المطعم
         * en_description : Pizza King is an American fast food restaurant with branches in many countries around the world. It is a restaurant specializing in pizza meals of all kinds, but it offers other meals. It also offers external delivery service as well as eating at the restaurant
         * kitchencategory_id : 1
         * user_id : 63
         * minicharge : 50
         * status : 1
         * openstatus : 1
         * deliveryflag : 1
         * deliverypriceperkilo : 100
         * delivermaxbill : 100
         * image : null
         * phone : 0111110100
         * phone2 : null
         * address : 3 st el kamal
         * lat : null
         * lng : null
         * remember_token : null
         * deleted_at : null
         * created_at : 2018-09-21 02:29:46
         * updated_at : 2018-09-24 00:23:20
         * hours : [{"id":2,"attribute_id":140,"day_id":0,"from":"13","to":"13","attribute_category":"kitchen","created_at":"2018-08-16 12:20:04","updated_at":"2018-08-16 12:20:04"},{"id":26,"attribute_id":140,"day_id":0,"from":"13","to":"13","attribute_category":"kitchen","created_at":"2018-08-27 20:19:22","updated_at":"2018-08-27 20:19:22"},{"id":27,"attribute_id":140,"day_id":0,"from":"13","to":"13","attribute_category":"kitchen","created_at":"2018-08-27 20:25:22","updated_at":"2018-08-27 20:25:22"}]
         * kitchencategory : {"id":1,"title":"Resturant","image":"","created_at":"2018-07-30 14:33:39","updated_at":"2018-07-30 14:38:04"}
         * kitchenimages : [{"id":55,"image":"18-08-12-11-37-1492e303be80661589cdc5a7cf003bdd37abd6379e.jpg","kitchen_id":140,"created_at":"2018-08-12 11:37:14","updated_at":"2018-08-12 11:37:14"},{"id":56,"image":"18-08-12-11-37-15bc89525811ceb466a857bab6441667012a44c7c7.jpg","kitchen_id":140,"created_at":"2018-08-12 11:37:15","updated_at":"2018-08-12 11:37:15"},{"id":60,"image":"18-08-27-20-19-29bc199cab35b038b5d55c9d5502f7817f77fb6c80.jpg","kitchen_id":140,"created_at":"2018-08-27 20:19:29","updated_at":"2018-08-27 20:19:29"},{"id":61,"image":"18-08-27-20-25-560c0fed704bbe55ef4a809c17ee7a4ed257fc2dcc.jpg","kitchen_id":140,"created_at":"2018-08-27 20:25:56","updated_at":"2018-08-27 20:25:56"}]
         * kitchentags : [{"id":1,"title":"Banquet ","image":"18-07-31-08-44-4464c51bb8166a734d32ee12279e09299a2112dacb.jpg","created_at":"2018-07-31 08:38:55","updated_at":"2018-07-31 08:44:44","pivot":{"kitchen_id":140,"kitchentag_id":1}},{"id":2,"title":"Candy","image":"18-07-31-08-44-4464c51bb8166a734d32ee12279e09299a2112dacb.jpg","created_at":"2018-07-31 08:38:55","updated_at":"2018-07-31 08:44:44","pivot":{"kitchen_id":140,"kitchentag_id":2}}]
         */

        private int id;
        private String ar_title;
        private String en_title;
        private String ar_description;
        private String en_description;
        private int kitchencategory_id;
        private int user_id;
        private String minicharge;
        private String status;
        private String openstatus;
        private String deliveryflag;
        private String deliverypriceperkilo;
        private String delivermaxbill;
        private Object image;
        private String phone;
        private Object phone2;
        private String address;
        private Object lat;
        private Object lng;
        private Object remember_token;
        private Object deleted_at;
        private String created_at;
        private String updated_at;
        private KitchencategoryBean kitchencategory;
        private List<HoursBean> hours;
        private List<KitchenimagesBean> kitchenimages;
        private List<KitchentagsBean> kitchentags;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public String getAr_description() {
            return ar_description;
        }

        public void setAr_description(String ar_description) {
            this.ar_description = ar_description;
        }

        public String getEn_description() {
            return en_description;
        }

        public void setEn_description(String en_description) {
            this.en_description = en_description;
        }

        public int getKitchencategory_id() {
            return kitchencategory_id;
        }

        public void setKitchencategory_id(int kitchencategory_id) {
            this.kitchencategory_id = kitchencategory_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getMinicharge() {
            return minicharge;
        }

        public void setMinicharge(String minicharge) {
            this.minicharge = minicharge;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOpenstatus() {
            return openstatus;
        }

        public void setOpenstatus(String openstatus) {
            this.openstatus = openstatus;
        }

        public String getDeliveryflag() {
            return deliveryflag;
        }

        public void setDeliveryflag(String deliveryflag) {
            this.deliveryflag = deliveryflag;
        }

        public String getDeliverypriceperkilo() {
            return deliverypriceperkilo;
        }

        public void setDeliverypriceperkilo(String deliverypriceperkilo) {
            this.deliverypriceperkilo = deliverypriceperkilo;
        }

        public String getDelivermaxbill() {
            return delivermaxbill;
        }

        public void setDelivermaxbill(String delivermaxbill) {
            this.delivermaxbill = delivermaxbill;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Object getPhone2() {
            return phone2;
        }

        public void setPhone2(Object phone2) {
            this.phone2 = phone2;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Object getLat() {
            return lat;
        }

        public void setLat(Object lat) {
            this.lat = lat;
        }

        public Object getLng() {
            return lng;
        }

        public void setLng(Object lng) {
            this.lng = lng;
        }

        public Object getRemember_token() {
            return remember_token;
        }

        public void setRemember_token(Object remember_token) {
            this.remember_token = remember_token;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public KitchencategoryBean getKitchencategory() {
            return kitchencategory;
        }

        public void setKitchencategory(KitchencategoryBean kitchencategory) {
            this.kitchencategory = kitchencategory;
        }

        public List<HoursBean> getHours() {
            return hours;
        }

        public void setHours(List<HoursBean> hours) {
            this.hours = hours;
        }

        public List<KitchenimagesBean> getKitchenimages() {
            return kitchenimages;
        }

        public void setKitchenimages(List<KitchenimagesBean> kitchenimages) {
            this.kitchenimages = kitchenimages;
        }

        public List<KitchentagsBean> getKitchentags() {
            return kitchentags;
        }

        public void setKitchentags(List<KitchentagsBean> kitchentags) {
            this.kitchentags = kitchentags;
        }

        public static class KitchencategoryBean {
            /**
             * id : 1
             * title : Resturant
             * image :
             * created_at : 2018-07-30 14:33:39
             * updated_at : 2018-07-30 14:38:04
             */

            private int id;
            private String title;
            private String image;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class HoursBean {
            /**
             * id : 2
             * attribute_id : 140
             * day_id : 0
             * from : 13
             * to : 13
             * attribute_category : kitchen
             * created_at : 2018-08-16 12:20:04
             * updated_at : 2018-08-16 12:20:04
             */

            private int id;
            private int attribute_id;
            private int day_id;
            private String from;
            private String to;
            private String attribute_category;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getAttribute_id() {
                return attribute_id;
            }

            public void setAttribute_id(int attribute_id) {
                this.attribute_id = attribute_id;
            }

            public int getDay_id() {
                return day_id;
            }

            public void setDay_id(int day_id) {
                this.day_id = day_id;
            }

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }

            public String getAttribute_category() {
                return attribute_category;
            }

            public void setAttribute_category(String attribute_category) {
                this.attribute_category = attribute_category;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class KitchenimagesBean {
            /**
             * id : 55
             * image : 18-08-12-11-37-1492e303be80661589cdc5a7cf003bdd37abd6379e.jpg
             * kitchen_id : 140
             * created_at : 2018-08-12 11:37:14
             * updated_at : 2018-08-12 11:37:14
             */

            private int id;
            private String image;
            private int kitchen_id;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public int getKitchen_id() {
                return kitchen_id;
            }

            public void setKitchen_id(int kitchen_id) {
                this.kitchen_id = kitchen_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class KitchentagsBean {
            /**
             * id : 1
             * title : Banquet
             * image : 18-07-31-08-44-4464c51bb8166a734d32ee12279e09299a2112dacb.jpg
             * created_at : 2018-07-31 08:38:55
             * updated_at : 2018-07-31 08:44:44
             * pivot : {"kitchen_id":140,"kitchentag_id":1}
             */

            private int id;
            private String title;
            private String image;
            private String created_at;
            private String updated_at;
            private PivotBean pivot;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public PivotBean getPivot() {
                return pivot;
            }

            public void setPivot(PivotBean pivot) {
                this.pivot = pivot;
            }

            public static class PivotBean {
                /**
                 * kitchen_id : 140
                 * kitchentag_id : 1
                 */

                private int kitchen_id;
                private int kitchentag_id;

                public int getKitchen_id() {
                    return kitchen_id;
                }

                public void setKitchen_id(int kitchen_id) {
                    this.kitchen_id = kitchen_id;
                }

                public int getKitchentag_id() {
                    return kitchentag_id;
                }

                public void setKitchentag_id(int kitchentag_id) {
                    this.kitchentag_id = kitchentag_id;
                }
            }
        }
    }
}
