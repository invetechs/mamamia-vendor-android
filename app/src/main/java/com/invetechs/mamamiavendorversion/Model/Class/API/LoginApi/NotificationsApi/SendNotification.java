package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.NotificationsApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.NotificationResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SendNotification {

    @FormUrlEncoded
    @POST("convertViewsNotificationVendor")
    Call<NotificationResponse> sendNotification(@Field("id") String id);
}
