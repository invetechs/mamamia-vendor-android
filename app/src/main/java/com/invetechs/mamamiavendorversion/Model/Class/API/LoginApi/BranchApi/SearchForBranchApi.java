package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.BranchApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SearchForBranchApi
{
    @FormUrlEncoded
    @POST("kitchensearch")
    retrofit2.Call<ResultModelUser> searchForKitchenAPI(@Field("keyword") String keyword
    );
} // interface of SearchForBranchApi
