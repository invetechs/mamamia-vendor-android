package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.InvoiceApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ArchiveResultModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface GetInnvoiceApi
{

    @FormUrlEncoded
    @POST("InvoiceByVender")
    Call<ArchiveResultModel> getAllInvoices(
            @Field("vendor_id") String id,
            @Field("offset") int offset,
            @Field("from") String from,
            @Field("to") String to,
            @Field("id") String orderId
    );
}
