package com.invetechs.mamamiavendorversion.Model.Class.Model;

public class InvoiceModel {

    private String date;
    private String price;
    private String order_num;

    public InvoiceModel() {
    }

    public InvoiceModel(String date, String price, String order_num) {
        this.date = date;
        this.price = price;
        this.order_num = order_num;
    }

    public String getOrder_num() {
        return order_num;
    }

    public String getDate() {
        return date;
    }

    public String getPrice() {
        return price;
    }
}
