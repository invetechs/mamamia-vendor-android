package com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit;

public class Kitchentags
{
    private String id;

    private Pivot pivot;

    private String title;

    private String updated_at;

    private String created_at;

    private String image;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Pivot getPivot ()
    {
        return pivot;
    }

    public void setPivot (Pivot pivot)
    {
        this.pivot = pivot;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", pivot = "+pivot+", title = "+title+", updated_at = "+updated_at+", created_at = "+created_at+", image = "+image+"]";
    }
}


