package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.ProfileApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ProfileApi  {

    @FormUrlEncoded
    @POST("Getvendorprofile")
    retrofit2.Call<ResultModelUser> show_profile(@Field("id") String id);
}
