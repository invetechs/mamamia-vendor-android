package com.invetechs.mamamiavendorversion.Model.Class.Model;

public class AvailableDay
{
    String day_id , from , to  ;

    public AvailableDay() {} // default constructor

    public AvailableDay(String day_id, String from, String to) {
        this.day_id = day_id;
        this.from = from;
        this.to = to;
    } // param constructor


    public AvailableDay(String from, String to) {
        this.from = from;
        this.to = to;
    } // param constructor


    public String getDay_id() {
        return day_id;
    }

    public void setDay_id(String day_id) {
        this.day_id = day_id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
} // class  of AvailableDay
