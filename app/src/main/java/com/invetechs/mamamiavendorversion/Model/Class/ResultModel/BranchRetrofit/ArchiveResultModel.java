package com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit;

import java.util.List;

public class ArchiveResultModel
{

    /**
     * code : 200
     * Status : success
     * message : Archives
     * Archives : [{"totalprice":"21133143123432","userplace":"giza","id":6,"status":"1","type":"meal","ar_title":"g","count":2,"en_title":"g"},{"totalprice":"21133143123432","userplace":"giza","id":7,"status":"-1","type":"banquet","ar_title":"g","count":1,"en_title":"g"},{"totalprice":"21133143123432","userplace":"giza","id":8,"status":"1","type":"meal","ar_title":"g","count":1,"en_title":"g"}]
     */

    private String code;
    private String Status;
    private String message;
    private List<ArchivesBean> Archives;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ArchivesBean> getArchives() {
        return Archives;
    }

    public void setArchives(List<ArchivesBean> Archives) {
        this.Archives = Archives;
    }

    public static class ArchivesBean {
        /**
         * totalprice : 21133143123432
         * userplace : giza
         * id : 6
         * status : 1
         * type : meal
         * ar_title : g
         * count : 2
         * en_title : g
         */

        private String totalprice;
        private String userplace;
        private int id;
        private String status;
        private String type;
        private String ar_title;
        private int count;
        private String en_title;

        public String getTotalprice() {
            return totalprice;
        }

        public void setTotalprice(String totalprice) {
            this.totalprice = totalprice;
        }

        public String getUserplace() {
            return userplace;
        }

        public void setUserplace(String userplace) {
            this.userplace = userplace;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }
    }
}
