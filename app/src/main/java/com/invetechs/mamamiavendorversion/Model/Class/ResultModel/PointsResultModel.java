package com.invetechs.mamamiavendorversion.Model.Class.ResultModel;

import java.util.List;

public class PointsResultModel
{


    /**
     * code : 200
     * Status : success
     * message : all rules
     * alluserspoint : [{"id":1,"created_at":"2018-09-18 00:00:00","updated_at":"2018-09-11 00:00:00","user_id":16,"vendor_id":63,"kitchen_id":140,"points":100,"user":{"id":16,"name":"ahh sigh","email":null,"mobilenumber":"0563258964","image":null,"status":"0","isverified":"0","category":"vendor","created_at":"2018-07-25 14:51:01","updated_at":"2018-07-25 14:51:01"}},{"id":2,"created_at":"2018-09-19 00:00:00","updated_at":"2018-09-20 00:00:00","user_id":28,"vendor_id":63,"kitchen_id":140,"points":1000,"user":{"id":28,"name":"ahlawi","email":null,"mobilenumber":"0112291654","image":null,"status":"0","isverified":"0","category":"vendor","created_at":"2018-07-26 12:46:32","updated_at":"2018-07-26 12:46:32"}}]
     */

    private String code;
    private String Status;
    private String message;
    private List<AlluserspointBean> alluserspoint;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AlluserspointBean> getAlluserspoint() {
        return alluserspoint;
    }

    public void setAlluserspoint(List<AlluserspointBean> alluserspoint) {
        this.alluserspoint = alluserspoint;
    }

    public static class AlluserspointBean {
        /**
         * id : 1
         * created_at : 2018-09-18 00:00:00
         * updated_at : 2018-09-11 00:00:00
         * user_id : 16
         * vendor_id : 63
         * kitchen_id : 140
         * points : 100
         * user : {"id":16,"name":"ahh sigh","email":null,"mobilenumber":"0563258964","image":null,"status":"0","isverified":"0","category":"vendor","created_at":"2018-07-25 14:51:01","updated_at":"2018-07-25 14:51:01"}
         */

        private int id;
        private String created_at;
        private String updated_at;
        private int user_id;
        private int vendor_id;
        private int kitchen_id;
        private int points;
        private UserBean user;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getVendor_id() {
            return vendor_id;
        }

        public void setVendor_id(int vendor_id) {
            this.vendor_id = vendor_id;
        }

        public int getKitchen_id() {
            return kitchen_id;
        }

        public void setKitchen_id(int kitchen_id) {
            this.kitchen_id = kitchen_id;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public static class UserBean {
            /**
             * id : 16
             * name : ahh sigh
             * email : null
             * mobilenumber : 0563258964
             * image : null
             * status : 0
             * isverified : 0
             * category : vendor
             * created_at : 2018-07-25 14:51:01
             * updated_at : 2018-07-25 14:51:01
             */

            private int id;
            private String name;
            private Object email;
            private String mobilenumber;
            private Object image;
            private String status;
            private String isverified;
            private String category;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getMobilenumber() {
                return mobilenumber;
            }

            public void setMobilenumber(String mobilenumber) {
                this.mobilenumber = mobilenumber;
            }

            public Object getImage() {
                return image;
            }

            public void setImage(Object image) {
                this.image = image;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getIsverified() {
                return isverified;
            }

            public void setIsverified(String isverified) {
                this.isverified = isverified;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
