package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AcceptOrderApi
{
    @FormUrlEncoded
    @POST("acceptorder")
    retrofit2.Call<ResultModelUser> acceptOrderApi(@Field("id") String order_id,
                                                   @Field("vendornote") String vendornote
    );  // accept order

    @FormUrlEncoded
    @POST("OrderAccept")
    retrofit2.Call<ResultModelUser> send_order(@Field("id") String order_id,
                                                   @Field("vendornote") String vendornote ,
                                                   @Field("neededTime") String neededTime ,
                                                   @Field("price") String price ,
                                                   @Field("status") int status ,
                                                   @Field("deliverytotalprice") String deliverytotalprice
    );  // accept order

}
