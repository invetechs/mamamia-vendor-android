package com.invetechs.mamamiavendorversion.Model.Class.Model;

public class Kitchentags
{
    String id ;

    public Kitchentags(){}

    public Kitchentags(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
