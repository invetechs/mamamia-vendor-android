package com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit;

import java.util.ArrayList;

public class Hours
{
    String day_id ;
    String from , to ;

    ArrayList<FromToClass> fromToList ;

    public  Hours () {} // class of Hours

    public Hours(String day_id, String from, String to) {
        this.day_id = day_id;
        this.from = from;
        this.to = to;
    } // param constructor
    public Hours(String from , String to)
    {
        this.from = from;
        this.to = to;
    }

    public Hours(ArrayList<FromToClass> fromToList)
    {
        this.fromToList = fromToList ;
    }
    public String getDay_id() {
        return day_id;
    }

    public void setDay_id(String  day_id) {
        this.day_id = day_id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public ArrayList<FromToClass> getFromToList() {
        return fromToList;
    }

    public void setFromToList(ArrayList<FromToClass> fromToList) {
        this.fromToList = fromToList;
    }

    public static class FromToClass
    {
        String from , to ;

        public FromToClass(String from,String to)
        {
            this.from = from;
            this.to = to;
        }
        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }
    } // class of FromToClass
} // class of Hours

