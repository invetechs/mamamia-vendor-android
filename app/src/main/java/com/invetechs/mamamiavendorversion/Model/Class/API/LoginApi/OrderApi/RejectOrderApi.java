package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RejectOrderApi
{
    @FormUrlEncoded
    @POST("ignororder")
    retrofit2.Call<ResultModelUser> rejectOrderApiWithVendorNotes(@Field("order_id") String order_id ,
                                                   @Field("vendornote") String vendornote);  // reject order

    @FormUrlEncoded
    @POST("ignororder")
    retrofit2.Call<ResultModelUser> rejectOrderApi(@Field("order_id") String order_id);  // reject order
}
