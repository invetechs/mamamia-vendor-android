package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.NotificationsApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.NotificationResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface NotificationRequest {

    @FormUrlEncoded
    @POST("NotificationByUser")
    Call<NotificationResponse> showNotifications(@Field("vendor_id") String user_id);
}
