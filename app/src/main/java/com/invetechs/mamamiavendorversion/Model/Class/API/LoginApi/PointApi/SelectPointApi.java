package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.PointApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.PointsResultModel;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SelectPointApi
{
    @FormUrlEncoded
    @POST("Selectuserspoints")
    Call<PointsResultModel> getAllPoints(@Field("vendor_id") String id ,
                                         @Field("kitchen_id") int kid
    );
}
