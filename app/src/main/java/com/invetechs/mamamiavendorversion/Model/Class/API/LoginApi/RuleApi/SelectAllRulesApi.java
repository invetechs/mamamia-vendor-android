package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.RuleApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.RulesResultModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SelectAllRulesApi
{
    @FormUrlEncoded
    @POST("Selectonevendorrules")
    Call<RulesResultModel> getAllRuless(@Field("vendor_id") String id,
                                        @Field("kitchen_id") int kid
    );
}
