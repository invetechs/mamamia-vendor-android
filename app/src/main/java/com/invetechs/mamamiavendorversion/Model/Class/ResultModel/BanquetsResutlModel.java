package com.invetechs.mamamiavendorversion.Model.Class.ResultModel;

import java.util.List;

public class BanquetsResutlModel
{

    /**
     * code : 200
     * Status : success
     * message : Banquets
     * Banquets : [{"partylocation":null,"id":3,"ar_partyname":"Banquet wedding","en_partyname":"وليمة زواج"}]
     */

    private String code;
    private String Status;
    private String message;
    private int count ;
    private List<BanquetsBean> Banquets;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BanquetsBean> getBanquets() {
        return Banquets;
    }

    public void setBanquets(List<BanquetsBean> Banquets) {
        this.Banquets = Banquets;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static class BanquetsBean {
        /**
         * partylocation : null
         * id : 3
         * ar_partyname : Banquet wedding
         * en_partyname : وليمة زواج
         */

        private String partylocation;
        private int id;
        private String ar_partyname;
        private String en_partyname;

        public String getPartylocation() {
            return partylocation;
        }

        public void setPartylocation(String partylocation) {
            this.partylocation = partylocation;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAr_partyname() {
            return ar_partyname;
        }

        public void setAr_partyname(String ar_partyname) {
            this.ar_partyname = ar_partyname;
        }

        public String getEn_partyname() {
            return en_partyname;
        }

        public void setEn_partyname(String en_partyname) {
            this.en_partyname = en_partyname;
        }
    }
}
