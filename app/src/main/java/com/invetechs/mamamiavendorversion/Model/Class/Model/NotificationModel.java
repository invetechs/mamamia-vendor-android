package com.invetechs.mamamiavendorversion.Model.Class.Model;

public class NotificationModel {

    String title;
    String desc;
    String time;



    public NotificationModel(String title, String desc, String time) {
        this.title = title;
        this.desc = desc;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getTime() {
        return time;
    }
}
