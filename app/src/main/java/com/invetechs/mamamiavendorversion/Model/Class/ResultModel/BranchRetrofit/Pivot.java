package com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit;

public class Pivot
{
    private String kitchentag_id;

    private String kitchen_id;

    public String getKitchentag_id ()
    {
        return kitchentag_id;
    }

    public void setKitchentag_id (String kitchentag_id)
    {
        this.kitchentag_id = kitchentag_id;
    }

    public String getKitchen_id ()
    {
        return kitchen_id;
    }

    public void setKitchen_id (String kitchen_id)
    {
        this.kitchen_id = kitchen_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [kitchentag_id = "+kitchentag_id+", kitchen_id = "+kitchen_id+"]";
    }
}
