package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.AllOrdersResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.OneOrderResultModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SelectOneOrderDetails
{
    @FormUrlEncoded
    @POST("Orderdetails")
    Call<OneOrderResultModel> getOneOrderDetailsApi(@Field("id") String id);
}
