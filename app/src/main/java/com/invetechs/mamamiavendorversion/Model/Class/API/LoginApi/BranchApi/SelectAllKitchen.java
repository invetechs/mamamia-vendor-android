package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.BranchApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.SelectAllKitchenResultModel;






import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SelectAllKitchen {

    @FormUrlEncoded
    @POST("Selectonvendorkitchens")
    Call<SelectAllKitchenResultModel> getAllKitchen(@Field("user_id") String i);

}
