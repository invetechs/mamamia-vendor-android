package com.invetechs.mamamiavendorversion.Model.Class.Model;



import android.media.Image;

import java.util.ArrayList;
import java.util.List;


public class Branch
{

    //String [] kitcehntags ; // dessert , banquets , meals


    List<String> kitcehntags ;

    ArrayList<Image> images;

    List<AvailableDay> availabledays;

    String id,ar_title , ar_description , en_title , en_description , kitchencategory_id , minicharge , phone,phone2, address , lat , lng , deliverypriceperkilo
            ,deliveryflag , delivermaxbill , image ;

    // kitchencategory_id ----> Home Or Restaurant


    public Branch(List<String> kitcehntags, String id, String arabicName, String arabicDescription, String englishName, String englishDescription, String typeOfKitchen, String minCharge, String mobileNumber, String anotherMobile, String address, String lat, String lng, String costOfDeliveryPerKilo, String delivery, String invoicePriceToFreeDelivery){} // default constructor


    public Branch(List<String> kitcehntags, List<AvailableDay> availabledays, String id, String ar_title, String ar_description, String en_title, String en_description, String kitchencategory_id, String minicharge, String phone, String phone2, String address, String lat, String lng, String deliverypriceperkilo, String deliveryflag, String delivermaxbill) {
        this.kitcehntags = kitcehntags;
        this.availabledays = availabledays;
        this.id = id;
        this.ar_title = ar_title;
        this.ar_description = ar_description;
        this.en_title = en_title;
        this.en_description = en_description;
        this.kitchencategory_id = kitchencategory_id;
        this.minicharge = minicharge;
        this.phone = phone;
        this.phone2 = phone2;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.deliverypriceperkilo = deliverypriceperkilo;
        this.deliveryflag = deliveryflag;
        this.delivermaxbill = delivermaxbill;
    } // constructor param


    public List<String> getKitcehntags() {
        return kitcehntags;
    }

    public void setKitcehntags(List<String> kitcehntags) {
        this.kitcehntags = kitcehntags;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public List<AvailableDay> getAvailabledays() {
        return availabledays;
    }

    public void setAvailabledays(List<AvailableDay> availabledays) {
        this.availabledays = availabledays;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAr_title() {
        return ar_title;
    }

    public void setAr_title(String ar_title) {
        this.ar_title = ar_title;
    }

    public String getAr_description() {
        return ar_description;
    }

    public void setAr_description(String ar_description) {
        this.ar_description = ar_description;
    }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
    }

    public String getEn_description() {
        return en_description;
    }

    public void setEn_description(String en_description) {
        this.en_description = en_description;
    }

    public String getKitchencategory_id() {
        return kitchencategory_id;
    }

    public void setKitchencategory_id(String kitchencategory_id) {
        this.kitchencategory_id = kitchencategory_id;
    }

    public String getMinicharge() {
        return minicharge;
    }

    public void setMinicharge(String minicharge) {
        this.minicharge = minicharge;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDeliverypriceperkilo() {
        return deliverypriceperkilo;
    }

    public void setDeliverypriceperkilo(String deliverypriceperkilo) {
        this.deliverypriceperkilo = deliverypriceperkilo;
    }

    public String getDeliveryflag() {
        return deliveryflag;
    }

    public void setDeliveryflag(String deliveryflag) {
        this.deliveryflag = deliveryflag;
    }

    public String getDelivermaxbill() {
        return delivermaxbill;
    }

    public void setDelivermaxbill(String delivermaxbill) {
        this.delivermaxbill = delivermaxbill;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
} // class of Branch
