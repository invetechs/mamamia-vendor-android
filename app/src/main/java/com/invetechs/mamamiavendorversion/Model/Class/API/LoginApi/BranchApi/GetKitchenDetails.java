package com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.BranchApi;

import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.BranchRetrofitResult;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface GetKitchenDetails
{
    @FormUrlEncoded
    @POST("SelectonecKitchendata")
    retrofit2.Call<BranchRetrofitResult> getKitchenDetailsApi(@Field("kitchen_id") int kitchen_id
    );
} // interface of GetKitchenDetails
