package com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit;

import java.util.List;

public class SelectAllKitchenResultModel
{

    /**
     * code : 200
     * Status : success
     * message :  kitchens selected
     * kitchensdata : [{"id":139,"ar_title":"بيتزا هت","en_title":"MCdonalds","image":null},{"id":140,"ar_title":"بيتزا كينج","en_title":"Pizza King","image":null},{"id":141,"ar_title":"بيتزا هت","en_title":"KFC","image":null},{"id":142,"ar_title":"بيتزا هت","en_title":"MCdonalds","image":null},{"id":143,"ar_title":"بيتزا كينج","en_title":"Pizza King","image":null},{"id":144,"ar_title":"بيتزا هت","en_title":"KFC","image":null}]
     */

    private String code;
    private String Status;
    private String message;
    private List<KitchensdataBean> kitchensdata;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<KitchensdataBean> getKitchensdata() {
        return kitchensdata;
    }

    public void setKitchensdata(List<KitchensdataBean> kitchensdata) {
        this.kitchensdata = kitchensdata;
    }

    public static class KitchensdataBean {
        /**
         * id : 139
         * ar_title : بيتزا هت
         * en_title : MCdonalds
         * image : null
         */

        private int id;
        private String ar_title;
        private String en_title;
        private Object image;

        public KitchensdataBean (){}

        public KitchensdataBean (int id , String ar_title)
        {
            this.id = id ;
            this.ar_title = ar_title ;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        @Override
        public String toString() {
            return ar_title;
        }
    }
}
