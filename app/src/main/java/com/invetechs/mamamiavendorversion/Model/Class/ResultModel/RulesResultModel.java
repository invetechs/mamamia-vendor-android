package com.invetechs.mamamiavendorversion.Model.Class.ResultModel;

import java.util.List;

// FIXME generate failure  field _$AllRules150
public class RulesResultModel
{

    /**
     * code : 200
     * Status : success
     * message : all rules
     * allrules : [{"id":1,"rule":50,"points":5,"vendor_id":63,"kitchen_id":140,"created_at":"2018-09-26 00:00:00","updated_at":"2018-09-28 00:00:00"},{"id":2,"rule":100,"points":10,"vendor_id":63,"kitchen_id":140,"created_at":"2018-09-26 00:00:00","updated_at":"2018-09-28 00:00:00"},{"id":3,"rule":200,"points":20,"vendor_id":63,"kitchen_id":140,"created_at":"2018-09-26 00:00:00","updated_at":"2018-09-28 00:00:00"},{"id":4,"rule":10,"points":1,"vendor_id":63,"kitchen_id":140,"created_at":"2018-09-26 00:00:00","updated_at":"2018-09-28 00:00:00"},{"id":5,"rule":150,"points":15,"vendor_id":63,"kitchen_id":140,"created_at":"2018-09-26 00:00:00","updated_at":"2018-09-28 00:00:00"},{"id":6,"rule":250,"points":25,"vendor_id":63,"kitchen_id":140,"created_at":"2018-09-26 00:00:00","updated_at":"2018-09-28 00:00:00"}]
     */

    private String code;
    private String Status;
    private String message;
    private List<AllrulesBean> allrules;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AllrulesBean> getAllrules() {
        return allrules;
    }

    public void setAllrules(List<AllrulesBean> allrules) {
        this.allrules = allrules;
    }

    public static class AllrulesBean {
        /**
         * id : 1
         * rule : 50
         * points : 5
         * vendor_id : 63
         * kitchen_id : 140
         * created_at : 2018-09-26 00:00:00
         * updated_at : 2018-09-28 00:00:00
         */

        private int id;
        private int rule;
        private int points;
        private int vendor_id;
        private int kitchen_id;
        private int discount;
        private String ar_title;
        private String en_title;
        private String created_at;
        private String updated_at;


        public String getAr_title() {
            return ar_title;
        }

        public void setAr_title(String ar_title) {
            this.ar_title = ar_title;
        }

        public String getEn_title() {
            return en_title;
        }

        public void setEn_title(String en_title) {
            this.en_title = en_title;
        }

        public int getDiscount() {
            return discount;
        }

        public void setDiscount(int discount) {
            this.discount = discount;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getRule() {
            return rule;
        }

        public void setRule(int rule) {
            this.rule = rule;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        public int getVendor_id() {
            return vendor_id;
        }

        public void setVendor_id(int vendor_id) {
            this.vendor_id = vendor_id;
        }

        public int getKitchen_id() {
            return kitchen_id;
        }

        public void setKitchen_id(int kitchen_id) {
            this.kitchen_id = kitchen_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
