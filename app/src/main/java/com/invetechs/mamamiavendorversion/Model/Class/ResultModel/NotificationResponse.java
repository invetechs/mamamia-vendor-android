package com.invetechs.mamamiavendorversion.Model.Class.ResultModel;

import java.util.List;

public class NotificationResponse {



    private String code;
    private String Status;
    private String message;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {


        private String id;
        private Object ar_title;
        private Object en_title;
        private String ar_description;
        private String en_description;
        private String partyName;
        private String typeOrder;
        private int views;
        private int user_id;
        private int vendor_id;
        private int order_id;
        private String created_at;
        private String updated_at;
        private String status;
        private String ar_ago;
        private String en_ago;

        public String getPartyName() {
            return partyName;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Object getAr_title() {
            return ar_title;
        }

        public void setAr_title(Object ar_title) {
            this.ar_title = ar_title;
        }

        public Object getEn_title() {
            return en_title;
        }

        public void setEn_title(Object en_title) {
            this.en_title = en_title;
        }

        public String getAr_description() {
            return ar_description;
        }

        public void setAr_description(String ar_description) {
            this.ar_description = ar_description;
        }

        public String getEn_description() {
            return en_description;
        }

        public void setEn_description(String en_description) {
            this.en_description = en_description;
        }

        public String getTypeOrder() {
            return typeOrder;
        }

        public int getViews() {
            return views;
        }

        public void setViews(int views) {
            this.views = views;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getVendor_id() {
            return vendor_id;
        }

        public void setVendor_id(int vendor_id) {
            this.vendor_id = vendor_id;
        }

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAr_ago() {
            return ar_ago;
        }

        public void setAr_ago(String ar_ago) {
            this.ar_ago = ar_ago;
        }

        public String getEn_ago() {
            return en_ago;
        }

        public void setEn_ago(String en_ago) {
            this.en_ago = en_ago;
        }
    }
}
