package com.invetechs.mamamiavendorversion.View.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.invetechs.mamamiavendorversion.Adapter.NotificationstAdapter;
import com.invetechs.mamamiavendorversion.Config.ToolbarConfig;
import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.NotificationsApi.NotificationRequest;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.NotificationResponse;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NotificationsActivity extends Language {


    // Bind views
    @BindView(R.id.notification_recycler_view)
    RecyclerView notification_recycler_view;

    // vars
    ToolbarConfig toolBarConfig;
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    NotificationstAdapter adapter;
    ImageView img_notifications;
    SharedPreferences prefs;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String vendor_id = "";
    Handler handler;
    CustomDialogProgress progress;
    List<NotificationResponse.DataBean> arrayList;
    ShowDialog showDialog = new ShowDialog();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        getVendorID();
        setToolBarConfig();
        getNotifications();
       // getArrayListNotification();
        initLanguage();
        img_notifications = findViewById(R.id.img_notification);
        img_notifications.setOnClickListener(null);

    }

    private void getVendorID() {
        prefs = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        vendor_id = prefs.getString("id", "0");
        Log.i("QP", " invoice userId : " + vendor_id);

    } // getVendorID function

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    @SuppressLint("HandlerLeak")
    private void getNotifications() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();

    /*    new Thread() {
            public void run() {*/

                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final NotificationRequest notificationApi = retrofit.create(NotificationRequest.class);

                final Call<NotificationResponse> getInterestConnection = notificationApi.showNotifications(vendor_id);

                getInterestConnection.enqueue(new Callback<NotificationResponse>() {
                    @Override
                    public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                        try {
                            String code = response.body().getCode();

                            Log.i("QP", "code" + code);

                            response.body();

                            Log.i("QP", "response" + response.body().getCode());

                            if (code.equals("200")) {

                                arrayList = response.body().getData();

                                initRecyclerView();
                            } // login success

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<NotificationResponse> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), NotificationsActivity.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
         /*   }

        }.start();*/
    } // show notifications

    private void initRecyclerView() {

        adapter = new NotificationstAdapter(this,arrayList);
        notification_recycler_view.setHasFixedSize(true);
        notification_recycler_view.setLayoutManager(layoutManager);
        notification_recycler_view.setAdapter(adapter);
    }

    private void initLanguage() {
        sharedPreferences = this.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }
    } // function of checkLanguage

    private void setToolBarConfig() {
        toolBarConfig = new ToolbarConfig(this , this,"notification");
        toolBarConfig.setTitle(getString(R.string.notifications));
        toolBarConfig.setBack(this);
        toolBarConfig.addNotification();

    } // function of setToolBarConfig

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.onDestroy();

    }
}
