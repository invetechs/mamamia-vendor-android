package com.invetechs.mamamiavendorversion.View.Fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ddd.daterangepicker.DateRangePicker;
import com.invetechs.mamamiavendorversion.Adapter.ArchivesAdapter;
import com.invetechs.mamamiavendorversion.Adapter.Paginator;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.ArchiveApi.GetArchiveApi;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ArchiveResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.os.Build.VERSION_CODES.LOLLIPOP_MR1;

public class OrderArchivesFragment extends Fragment {

    // Bind view
    @BindView(R.id.recycler_view_orders)
    RecyclerView recyclerView;

    @BindView(R.id.bt_orders)
    Button bt_orders;

    @BindView(R.id.bt_banquets)
    Button bt_banquets;

    @BindView(R.id.from_date)
    LinearLayout from_date;

    @BindView(R.id.to_date)
    LinearLayout to_date;

    @BindView(R.id.order_linear)
    LinearLayout order_linear;

    @BindView(R.id.all_linear)
    LinearLayout all_linear;

    @BindView(R.id.tv_to_date)
    TextView tv_to;

    @BindView(R.id.tv_from_date)
    TextView tv_from_date;

    @BindView(R.id.search_click)
    TextView search_click;

    @BindView(R.id.to_title)
    TextView to_title;

    @BindView(R.id.from_title)
    TextView from_title;

    @BindView(R.id.order_title)
    TextView order_title;


    @BindView(R.id.rb_date)
    RadioButton rb_date;

    @BindView(R.id.linerPagination)
    RelativeLayout linerPagination;

    @BindView(R.id.tv_previous)
    TextView tv_previous;

    @BindView(R.id.tv_next)
    TextView tv_next;

    @BindView(R.id.rb_order_num)
    RadioButton rb_order_num;

    @BindView(R.id.tv_page)
    TextView tv_page;

    @BindView(R.id.tv_page_number)
    TextView tv_page_number;

    @BindView(R.id.et_order_number)
    EditText et_order_number;

    // vars
    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences.Editor editor;
    ArchivesAdapter orderAdapter, banquetAdapter;
    String code = "";
    SharedPreferences sharedPreferences;
    boolean isClicked = true;
    String archive_type = "meal";
    Paginator p = new Paginator();
    private int totalPages = 0;
    private int currentPage = 0;
    int numberOfPages = 0;
    String dateFrom = "0", dateTo = "0", orderId = "0", flag = "date", type = "order", date_picker_type = "";
    SharedPreferences prefs;
    public  final String MY_PREFS_NAME = "MyPrefsFile";
    String id = "";
    List<ArchiveResultModel.ArchivesBean> archiveArrayList = new ArrayList<>(),
            acceptedAndRejectedArrayList = new ArrayList<>(),
            orderArchiveList = new ArrayList<>(), banquetArchiveList = new ArrayList<>();
    ShowDialog showDialog = new ShowDialog();
    Date date1, date2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_archives, container, false);
        ButterKnife.bind(this, view);
        checkLanguage();
        getUserId();
        tv_previous.setEnabled(false);
        bt_orders.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        bt_banquets.setBackgroundColor(getResources().getColor(R.color.gray));
        getArchiveInvoiceFromApi(type, currentPage, dateFrom, dateTo, orderId, archive_type); // get all data from api
        return view;
    } // onCreateView

    @OnClick(R.id.tv_previous)
    public void goPrevious() {
        if (!(currentPage == 0 && totalPages == 0)) {
            currentPage -= 1;
        int c = currentPage + 1;

            tv_page_number.setText(c + "");

            getArchiveInvoiceFromApi(type, currentPage, dateFrom, dateTo, orderId, archive_type);

        }

        Log.i("QW", "total : " + totalPages + "   :  current : " + currentPage);
        toggleButtons();
    } // click on previos

    @OnClick(R.id.tv_next)
    public void goNext() {
        if (!(currentPage == 0 && totalPages == 0)) {
            currentPage += 1;
            int c = currentPage + 1;

            tv_page_number.setText(c + "");
            getArchiveInvoiceFromApi(type, currentPage, dateFrom, dateTo, orderId, archive_type);

        }

        toggleButtons();

    } // click on next

    private void toggleButtons() {
        if (currentPage == totalPages) {
            tv_next.setEnabled(false);
            tv_previous.setEnabled(true);
        } else if (currentPage == 0) {
            tv_previous.setEnabled(false);
            tv_next.setEnabled(true);
        } else if (currentPage >= 1 && currentPage <= totalPages) {
            tv_next.setEnabled(true);
            tv_previous.setEnabled(true);
        }
    } // toogle function

    @OnClick(R.id.img_search)
    public void searchClick() {
        if (isClicked) {
            all_linear.setVisibility(View.VISIBLE);

        } else {
            all_linear.setVisibility(View.GONE);

        }

        isClicked = !isClicked;
    }

    @OnClick(R.id.rb_date)
    public void radioDate() {
        from_date.setVisibility(View.VISIBLE);
        to_date.setVisibility(View.VISIBLE);
        order_linear.setVisibility(View.GONE);
        flag = "date";
    }

    @OnClick(R.id.rb_order_num)
    public void radioOrderNum() {
        from_date.setVisibility(View.GONE);
        to_date.setVisibility(View.GONE);
        order_linear.setVisibility(View.VISIBLE);
        linerPagination.setVisibility(View.GONE);
        flag = "num";

    }

    @OnClick(R.id.from_date)
    public void openFromDatePicker() {

        initDateRangePicker();

    } // open date picker dialog to choose from date

    public String convertToEnglish(String value) {
        String newValue = (((((((((((value + "")
                .replaceAll("١", "1")).replaceAll("٢", "2"))
                .replaceAll("٣", "3")).replaceAll("٤", "4"))
                .replaceAll("٥", "5")).replaceAll("٦", "6"))
                .replaceAll("٧", "7")).replaceAll("٨", "8"))
                .replaceAll("٩", "9")).replaceAll("٠", "0"));

        return newValue;
    } // convert arabic date to english to send to api

    private void initDateRangePicker() {
        if (Build.VERSION.SDK_INT > LOLLIPOP_MR1) {

            date_picker_type = "new_version";
            // Call some material design APIs here
            final Range dateRangePicker = new Range(getContext(), new DateRangePicker.OnCalenderClickListener() {
                @Override
                public void onDateSelected(String selectedStartDate, String selectedEndDate) {

                    tv_from_date.setText(selectedStartDate);
                    tv_to.setText(selectedEndDate);
                    dateFrom = tv_from_date.getText().toString().trim();
                    dateTo = tv_to.getText().toString().trim();
                    currentPage = 0;

                    dateFrom = convertToEnglish(selectedStartDate);
                    dateTo = convertToEnglish(selectedEndDate);
                }
            });

            dateRangePicker.show();
            dateRangePicker.setBtnPositiveText(getString(R.string.btn_confirm));
            dateRangePicker.setBtnNegativeText(getString(R.string.cancel));
        } else {
            date_picker_type = "old_version";
            showRandomDate();
            // Implement this feature without material design
        }

    } // initialize date picker


    private void showRandomDate() {

        from_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DatePickerFragment date = new DatePickerFragment();
                Calendar calender = Calendar.getInstance();
                Bundle args = new Bundle();
                args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
                args.putInt("month", calender.get(Calendar.MONTH));
                args.putInt("year", calender.get(Calendar.YEAR));
                date.setArguments(args);
                date.setCallBack(ondate);
                date.show(getFragmentManager(), "Date Picker");
            }

            DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();

                    if (sharedPreferences.getString("language", "ar").equals("ar")) {
                        tv_from_date.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" +
                                String.valueOf(dayOfMonth));

                    } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                        tv_from_date.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                                + "-" + String.valueOf(year));

                    }
                    dateFrom = tv_from_date.getText().toString().trim();

                    SimpleDateFormat simple_from = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        date1 = simple_from.parse(String.valueOf(dateFrom));


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            };
        });

        to_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DatePickerFragment date = new DatePickerFragment();
                Calendar calender = Calendar.getInstance();
                Bundle args = new Bundle();
                args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
                args.putInt("month", calender.get(Calendar.MONTH));
                args.putInt("year", calender.get(Calendar.YEAR));
                date.setArguments(args);
                date.setCallBack(ondate);
                date.show(getFragmentManager(), "Date Picker");
            }

            DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();

                    if (sharedPreferences.getString("language", "ar").equals("ar")) {
                        tv_to.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" +
                                String.valueOf(dayOfMonth));

                    } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                        tv_to.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                                + "-" + String.valueOf(year));

                    }

                    dateTo = tv_to.getText().toString().trim();

                    SimpleDateFormat simple_to = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        date2 = simple_to.parse(String.valueOf(dateTo));


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Log.i("QP", " date 1 : " + date1);
                    Log.i("QP", " date 2: " + date2);
                }


            };

        });


    } // show random date


    private void checkLanguage() {
        sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getActivity().getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            search_click.setTypeface(font);
            tv_previous.setTypeface(font);
            tv_next.setTypeface(font);
            tv_page.setTypeface(font);
            tv_page_number.setTypeface(font);
            rb_date.setTypeface(font);
            rb_order_num.setTypeface(font);
            tv_from_date.setTypeface(font);
            tv_to.setTypeface(font);
            et_order_number.setTypeface(font);
            to_title.setTypeface(font);
            from_title.setTypeface(font);
            order_title.setTypeface(font);
            bt_orders.setTypeface(font);
            bt_banquets.setTypeface(font);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            search_click.setTypeface(font);
            tv_previous.setTypeface(font);
            tv_next.setTypeface(font);
            tv_page.setTypeface(font);
            tv_page_number.setTypeface(font);
            rb_date.setTypeface(font);
            rb_order_num.setTypeface(font);
            tv_from_date.setTypeface(font);
            tv_to.setTypeface(font);
            et_order_number.setTypeface(font);
            to_title.setTypeface(font);
            from_title.setTypeface(font);
            order_title.setTypeface(font);
            bt_orders.setTypeface(font);
            bt_banquets.setTypeface(font);

        }
    } // checkLanguage

    private void initRecyclerView(List<ArchiveResultModel.ArchivesBean> list, String flag) {

        if (flag == "order") {
            orderAdapter = new ArchivesAdapter(getContext(), p.generatePage(currentPage), list, flag);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(orderAdapter);
            Log.i("QP", " archive recycler view : a  " + recyclerView);

        } else if (flag == "banquet") {
            banquetAdapter = new ArchivesAdapter(getContext(), p.generatePage(currentPage), list, flag);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(banquetAdapter);
            Log.i("QP", " archive recycler view : a  " + recyclerView);

        }

    } // intialize reclerView with archive orders

    private void getUserId() {
        prefs = getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        id = prefs.getString("id", "0");
        Log.i("QP", " invoice userId : " + id);

    } // getUserId function

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getArchiveInvoiceFromApi(final String kind, final int page, final String from, final String to, final String oId
            , final String type) {

        Log.i("QP","size : newwwwwwwwwwwwwww"+archiveArrayList.size()+" : "+orderArchiveList.size()+" : "+banquetArchiveList.size());

        if (orderArchiveList.size() > 0) {
            orderArchiveList.clear();
            archiveArrayList.clear();
            orderAdapter.notifyDataSetChanged();
            Log.i("QP","clear Order");
        }

        if (banquetArchiveList.size() > 0) {
            banquetArchiveList.clear();
            archiveArrayList.clear();
            banquetAdapter.notifyDataSetChanged();
            Log.i("QP","clear Banquet");
        }

        progress = new CustomDialogProgress();
        progress.init(getActivity());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
      /*  new Thread() {
            public void run() {*/
//Retrofit
                acceptedAndRejectedArrayList = new ArrayList<>();
                orderArchiveList = new ArrayList<>();
                banquetArchiveList = new ArrayList<>();

                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final GetArchiveApi orderApi = retrofit.create(GetArchiveApi.class);

                final Call<ArchiveResultModel> getInterestConnection = orderApi.getAllOrders(id, page, from, to, oId, type);

                getInterestConnection.enqueue(new Callback<ArchiveResultModel>() {
                    @Override
                    public void onResponse(Call<ArchiveResultModel> call, Response<ArchiveResultModel> response) {
                        try {

                            String code = response.body().getCode();

                            Log.i("QP", " invoice code : " + code
                                    + "\n VendorId : " + id
                                    + "\n currentPage : " + page
                                    + "\n from : " + from
                                    + "\n to : " + to
                                    + "\n orderId : " + oId);

                            if (code.equals("200")) {
                                linerPagination.setVisibility(View.VISIBLE);

                                if (response.body() != null) {
                                    archiveArrayList = response.body().getArchives();
                                    numberOfPages = response.body().getCount();
                                    Log.i("QP", "number of pages: " + numberOfPages);

                                    totalPages = numberOfPages / 10;

                                    if ((numberOfPages % 10) == 0)
                                        totalPages -= 1;

                                    int c = currentPage + 1;
                                    tv_page.setText(totalPages + 1 + " / " + c);
                                    tv_page_number.setText(currentPage + 1 + "");

                                    if (kind.equals("order")) {
                                        fillOrderArchiveData();
                                        Log.i("QP", "order");
                                    } else if (kind.equals("banquet")) {
                                        fillBanquetArchiveData();
                                        Log.i("QP", "banquet");
                                    }
                                } else {
                                    showDialog.initDialog(getString(R.string.retry), getActivity());

                                }

                            } // get invoices success
                            else if (code.equals("1313")) {
                                linerPagination.setVisibility(View.INVISIBLE);
                                showDialog.initDialog(getString(R.string.no_archives), getActivity());
                            } // no invoices

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<ArchiveResultModel> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

     /*       }

        }.start();*/
    } // function of get All Invoices From Api


    @OnClick(R.id.bt_orders)
    public void buttonOrderClick() {
        bt_orders.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        bt_banquets.setBackgroundColor(getResources().getColor(R.color.gray));
        dateFrom = "0";
        dateTo = "0";
        orderId = "0";
        type = "order";
        currentPage = 0;
        archive_type = "meal";

        getArchiveInvoiceFromApi(type, currentPage, dateFrom, dateTo, orderId, archive_type);


    } // function of button order click

    @OnClick(R.id.bt_banquets)
    public void buttonBanquetClick() {
        bt_orders.setBackgroundColor(getResources().getColor(R.color.gray));
        bt_banquets.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        dateFrom = "0";
        dateTo = "0";
        orderId = "0";
        type = "banquet";
        currentPage = 0;
        archive_type = "banquet";


        getArchiveInvoiceFromApi(type, currentPage, dateFrom, dateTo, orderId, archive_type);

    }// function of button banquet click

    private void fillOrderArchiveData() {

        initRecyclerView(orderArchiveList, "order");
        for (int i = 0; i < archiveArrayList.size(); i++) {
            if (archiveArrayList.get(i).getType().equals("meal")) {
                orderArchiveList.add(archiveArrayList.get(i));
            }
        } // for loop
        orderAdapter.notifyDataSetChanged();
    } // fill order archive

    private void fillBanquetArchiveData() {
        initRecyclerView(banquetArchiveList, "banquet");
        Log.i("QP", " size : " + archiveArrayList.size());
        for (int i = 0; i < archiveArrayList.size(); i++) {
            if (archiveArrayList.get(i).getType().equals("banquet")) {
                banquetArchiveList.add(archiveArrayList.get(i));
            }
        } // for loop


        banquetAdapter.notifyDataSetChanged();
        Log.i("QP", " size banquetList : " + banquetArchiveList.size());
    } // fill banquet archive

    @OnClick(R.id.search_click)
    public void onClickSearch() {
        InputMethodManager inputManager =
                (InputMethodManager) getContext().
                        getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);

        if (flag.equals("num")) {
            currentPage = 0;
            dateFrom = "0";
            dateTo = "0";

            if (et_order_number.getText().toString().equals("")) {
                showDialog.initDialog(getString(R.string.enterOrderNum), getActivity());
            } else {
                orderId = et_order_number.getText().toString();
                getArchiveInvoiceFromApi(type, currentPage, dateFrom, dateTo, orderId, archive_type);
            }
        } // choose search with order number
        else if (flag.equals("date")) {
            orderId = "0";
            if (dateFrom.equals("0") || dateTo.equals("0")) {
                showDialog.initDialog(getString(R.string.enterVaildDate), getActivity());
            } else {
                if (date_picker_type.equals("new_version")) {
                    getArchiveInvoiceFromApi(type, currentPage, dateFrom, dateTo, orderId, archive_type);
                } else if (date_picker_type.equals("old_version")) {
                    if (date2.before(date1)) {
                        showDialog.initDialog(getString(R.string.enterVaildDate), getActivity());
                        return;
                    } else {
                        getArchiveInvoiceFromApi(type, currentPage, dateFrom, dateTo, orderId, archive_type);
                    }
                }
                getArchiveInvoiceFromApi(type, currentPage, dateFrom, dateTo, orderId, archive_type);
            }
        } // choose search with date
        else if (flag.equals("")) {
            showDialog.initDialog(getString(R.string.field_cant_empty), getActivity());
        }

    } // function of onClickSearch


} // class of OrderArchivesFragment
