package com.invetechs.mamamiavendorversion.View.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.R;

public class ShowDialog extends AppCompatActivity {

    // vars
    Dialog acceptAndRejectDialog;
    TextView messageShow;
    LinearLayout layout;
    SharedPreferences sharedPreferences;
    Context context ;


    private static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth


    public void initDialog(String message, Context context) {
        this.context = context ;
        acceptAndRejectDialog = new Dialog(context);
        acceptAndRejectDialog.setContentView(R.layout.accept_refuse_dialog);
        layout = acceptAndRejectDialog.findViewById(R.id.layoutDialog);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        acceptAndRejectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        acceptAndRejectDialog.getWindow().setLayout((int) (getScreenWidth((Activity) context) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
        acceptAndRejectDialog.getWindow();
        acceptAndRejectDialog.show();
        messageShow = acceptAndRejectDialog.findViewById(R.id.tv_message);
        messageShow.setText(message);

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            messageShow.setTypeface(font);

        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            messageShow.setTypeface(font);

        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                acceptAndRejectDialog.dismiss();
            }
        }, 3000);
    }


    public  void  destroy()
    {
        if(context != null) {
            context = null;
        }
        Thread.interrupted();
    }

}
