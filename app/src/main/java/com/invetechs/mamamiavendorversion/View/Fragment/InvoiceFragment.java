package com.invetechs.mamamiavendorversion.View.Fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ddd.daterangepicker.DateRangePicker;
import com.invetechs.mamamiavendorversion.Adapter.InvoiceAdapter;
import com.invetechs.mamamiavendorversion.Adapter.Paginator;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.InvoiceApi.GetInnvoiceApi;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ArchiveResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.os.Build.VERSION_CODES.LOLLIPOP_MR1;


public class InvoiceFragment extends Fragment {

    // Bind views
    @BindView(R.id.invoice_recycler_view)
    RecyclerView invoiceRecyclerView;

    @BindView(R.id.from_date)
    LinearLayout from_date;

    @BindView(R.id.to_date)
    LinearLayout to_date;

    @BindView(R.id.order_linear)
    LinearLayout order_linear;

    @BindView(R.id.all_linear)
    LinearLayout all_linear;

    @BindView(R.id.tv_to_date)
    TextView tv_to;

    @BindView(R.id.search_click)
    TextView search_click;

    @BindView(R.id.tv_from_date)
    TextView tv_from_date;

    @BindView(R.id.rb_date)
    RadioButton rb_date;

    @BindView(R.id.rb_order_num)
    RadioButton rb_order_num;

    @BindView(R.id.tv_previous)
    TextView tv_previous;

    @BindView(R.id.tv_next)
    TextView tv_next;

    @BindView(R.id.from_title)
    TextView from_title;

    @BindView(R.id.to_title)
    TextView to_title;

    @BindView(R.id.order_title)
    TextView order_title;

    @BindView(R.id.tv_page)
    TextView tv_page;

    @BindView(R.id.tv_page_number)
    TextView tv_page_number;

    @BindView(R.id.linerPagination)
    RelativeLayout linerPagination;

    @BindView(R.id.et_order_number)
    EditText et_order_number;

    // vars
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    List<ArchiveResultModel.ArchivesBean> invoiceArrayList = new ArrayList<>(), orderArrayList = new ArrayList<>();
    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences prefs;
    public final String MY_PREFS_NAME = "MyPrefsFile";
    String id = "";
    View view;
    boolean isClicked = true;
    Paginator p = new Paginator();
    private int totalPages = 0;
    private int currentPage = 0;
    int numberOfPages = 0;
    String dateFrom = "0", dateTo = "0", orderId = "0", flag = "date" ,  date_picker_type = "";
    InvoiceAdapter invoiceAdapter;
    ShowDialog showDialog = new ShowDialog();
    Date date1, date2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.invoice_fragment, container, false);
        ButterKnife.bind(this, view);
        initLanguage();
        getUserId();
        tv_previous.setEnabled(false);
        getAllInvoiceFromApi(currentPage, dateTo, dateTo, orderId);
        linerPagination.setVisibility(View.VISIBLE);
        initRecyclerView();
        return view;
    }


    @OnClick(R.id.tv_previous)
    public void goPrevious() {
        Log.i("QW", "total in: " + totalPages + "   :  current in: " + currentPage);
        if (!(currentPage == 0 && totalPages == 0)) {
            currentPage -= 1;
            int c = currentPage + 1;

            tv_page_number.setText(c + "");

            getAllInvoiceFromApi(currentPage, dateFrom, dateTo, orderId);

        }
        toggleButtons();
    } // click on previos

    @OnClick(R.id.tv_next)
    public void goNext() {

        if (!(currentPage == 0 && totalPages == 0)) {
            currentPage += 1;
            int c = currentPage + 1;

            tv_page_number.setText(c + "");
            getAllInvoiceFromApi(currentPage, dateFrom, dateTo, orderId);

        }

        toggleButtons();

    } // click on next

    private void toggleButtons() {
        if (currentPage == totalPages) {
            tv_next.setEnabled(false);
            tv_previous.setEnabled(true);
        } else if (currentPage == 0) {
            tv_previous.setEnabled(false);
            tv_next.setEnabled(true);
        } else if (currentPage >= 1 && currentPage <= totalPages) {
            tv_next.setEnabled(true);
            tv_previous.setEnabled(true);
        }
    } // toogle function

    @OnClick(R.id.img_search)
    public void searchClick() {

        if (isClicked) {
            all_linear.setVisibility(View.VISIBLE);

        } else {
            all_linear.setVisibility(View.GONE);
            dateFrom = "0";
            dateTo = "0";
            orderId = "0";
            flag = "date";
            linerPagination.setVisibility(View.VISIBLE);
        }

        isClicked = !isClicked;
    } // on search click

    @OnClick(R.id.rb_date)
    public void radioDate() {
        from_date.setVisibility(View.VISIBLE);
        to_date.setVisibility(View.VISIBLE);
        order_linear.setVisibility(View.GONE);
        linerPagination.setVisibility(View.VISIBLE);
        flag = "date";


    } // click on date radio button

    @OnClick(R.id.rb_order_num)
    public void radioOrderNum() {
        from_date.setVisibility(View.GONE);
        to_date.setVisibility(View.GONE);
        order_linear.setVisibility(View.VISIBLE);
        linerPagination.setVisibility(View.INVISIBLE);
        flag = "num";

    } // click on order number radio button

    private void showRandomDate() {

        from_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DatePickerFragment date = new DatePickerFragment();
                Calendar calender = Calendar.getInstance();
                Bundle args = new Bundle();
                args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
                args.putInt("month", calender.get(Calendar.MONTH));
                args.putInt("year", calender.get(Calendar.YEAR));
                date.setArguments(args);
                date.setCallBack(ondate);
                date.show(getFragmentManager(), "Date Picker");
            }

            DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();

                    if (sharedPreferences.getString("language", "ar").equals("ar")) {
                        tv_from_date.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" +
                                String.valueOf(dayOfMonth));

                    } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                        tv_from_date.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                                + "-" + String.valueOf(year));

                    }
                    dateFrom = tv_from_date.getText().toString().trim();

                    SimpleDateFormat simple_from = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        date1 = simple_from.parse(String.valueOf(dateFrom));


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            };
        });

        to_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DatePickerFragment date = new DatePickerFragment();
                Calendar calender = Calendar.getInstance();
                Bundle args = new Bundle();
                args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
                args.putInt("month", calender.get(Calendar.MONTH));
                args.putInt("year", calender.get(Calendar.YEAR));
                date.setArguments(args);
                date.setCallBack(ondate);
                date.show(getFragmentManager(), "Date Picker");
            }

            DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();

                    if (sharedPreferences.getString("language", "ar").equals("ar")) {
                        tv_to.setText(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" +
                                String.valueOf(dayOfMonth));

                    } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                        tv_to.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                                + "-" + String.valueOf(year));

                    }

                    dateTo = tv_to.getText().toString().trim();

                    SimpleDateFormat simple_to = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        date2 = simple_to.parse(String.valueOf(dateTo));


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Log.i("QP", " date 1 : " + date1);
                    Log.i("QP", " date 2: " + date2);
                }


            };

        });


    } // show random date

    @OnClick(R.id.from_date)
    public void openFromDatePicker() {

        initDateRangePicker();

    } // open date picker dialog to choose from date


    public String convertToEnglish(String value) {
        String newValue = (((((((((((value + "")
                .replaceAll("١", "1")).replaceAll("٢", "2"))
                .replaceAll("٣", "3")).replaceAll("٤", "4"))
                .replaceAll("٥", "5")).replaceAll("٦", "6"))
                .replaceAll("٧", "7")).replaceAll("٨", "8"))
                .replaceAll("٩", "9")).replaceAll("٠", "0"));

        return newValue;
    } // convert arabic date to english to send to api

    private void initDateRangePicker() {

        if (Build.VERSION.SDK_INT > LOLLIPOP_MR1) {

            date_picker_type = "new_version";
            // Call some material design APIs here
            final Range dateRangePicker = new Range(getContext(), new DateRangePicker.OnCalenderClickListener() {
                @Override
                public void onDateSelected(String selectedStartDate, String selectedEndDate) {

                    tv_from_date.setText(selectedStartDate);
                    tv_to.setText(selectedEndDate);
                    dateFrom = tv_from_date.getText().toString().trim();
                    dateTo = tv_to.getText().toString().trim();
                    currentPage = 0;

                    dateFrom = convertToEnglish(selectedStartDate);
                    dateTo = convertToEnglish(selectedEndDate);
                }
            });

            dateRangePicker.show();
            dateRangePicker.setBtnPositiveText(getString(R.string.btn_confirm));
            dateRangePicker.setBtnNegativeText(getString(R.string.cancel));
        } else {
            date_picker_type = "old_version";
            showRandomDate();
            // Implement this feature without material design
        }

    }

    private void initRecyclerView() {
        invoiceAdapter = new InvoiceAdapter(getContext(), p.generatePage(currentPage), invoiceArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        invoiceRecyclerView.setHasFixedSize(true);
        invoiceRecyclerView.setLayoutManager(linearLayoutManager);
        invoiceRecyclerView.setAdapter(invoiceAdapter);

    } //initialize recycler view

    private void initLanguage() {

        sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            search_click.setTypeface(font);
            tv_previous.setTypeface(font);
            tv_next.setTypeface(font);
            tv_page.setTypeface(font);
            tv_page_number.setTypeface(font);
            rb_date.setTypeface(font);
            rb_order_num.setTypeface(font);
            tv_from_date.setTypeface(font);
            tv_to.setTypeface(font);
            et_order_number.setTypeface(font);
            to_title.setTypeface(font);
            from_title.setTypeface(font);
            order_title.setTypeface(font);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            search_click.setTypeface(font);
            tv_previous.setTypeface(font);
            tv_next.setTypeface(font);
            tv_page.setTypeface(font);
            tv_page_number.setTypeface(font);
            rb_date.setTypeface(font);
            rb_order_num.setTypeface(font);
            tv_from_date.setTypeface(font);
            tv_to.setTypeface(font);
            et_order_number.setTypeface(font);
            to_title.setTypeface(font);
            from_title.setTypeface(font);
            order_title.setTypeface(font);

        }


    } //initialization of language

    private void getUserId() {
        prefs = getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        id = prefs.getString("id", "0");
        Log.i("QP", " invoice userId : " + id);

    } // getUserId function

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getAllInvoiceFromApi(final int pageNumber, final String from, final String to, final String ordId) {

        if (orderArrayList.size() > 0 && invoiceArrayList.size() > 0) {
            orderArrayList.clear();
            invoiceArrayList.clear();
            invoiceAdapter.notifyDataSetChanged();
        }

        progress = new CustomDialogProgress();
        progress.init(getActivity());

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
       /* new Thread() {
            public void run() {*/
//Retrofit

                RetrofiUrlConnection connection = new RetrofiUrlConnection(getActivity());
                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final GetInnvoiceApi orderApi = retrofit.create(GetInnvoiceApi.class);

                Log.i("QP", "current page : " + currentPage
                        + "\n total pages : " + totalPages
                        + "\n dateFrom response : " + from
                        + "\n dateTo response: " + to);

                final Call<ArchiveResultModel> getInterestConnection = orderApi.getAllInvoices(id, pageNumber, from, to, ordId);

                getInterestConnection.enqueue(new Callback<ArchiveResultModel>() {
                    @Override
                    public void onResponse(Call<ArchiveResultModel> call, Response<ArchiveResultModel> response) {
                        try {

                            //  Log.i("QP"," from new response : "  + response.body());


                            String code = response.body().getCode();

                            if (code.equals("200")) {

                                linerPagination.setVisibility(View.VISIBLE);
                                response.body().getCode();

                                if (response.body() != null) {
                                    Log.i("QP", " invoice response : " + response.body().getCode());
                                    Log.i("QP", " from new response : " + response.body().getFrom());

                                    orderArrayList = response.body().getArchives();

                                    numberOfPages = response.body().getCount();

                                    for (int i = 0; i < orderArrayList.size(); i++) {
                                        if (orderArrayList.get(i).getStatus().equals("1")) {
                                            invoiceArrayList.add(orderArrayList.get(i));

                                        }
                                    }

                                    totalPages = numberOfPages / 10;

                                    if ((numberOfPages % 10) == 0)
                                        totalPages -= 1;

                                    int c = currentPage + 1;
                                    tv_page.setText(totalPages + 1 + " / " + c);
                                    tv_page_number.setText(currentPage + 1 + "");
                                    invoiceAdapter.notifyDataSetChanged();
                                } else {
                                    showDialog.initDialog(getString(R.string.retry), getActivity());

                                }

                            } // get invoices success
                            else if (code.equals("1313")) {
                                linerPagination.setVisibility(View.INVISIBLE);
                                showDialog.initDialog(getString(R.string.no_invoice), getActivity());
                            } // no invoices


                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<ArchiveResultModel> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

         /*   }

        }.start();*/
    } // function of get All Invoices From Api

    @OnClick(R.id.search_click)
    public void onClickSearch()
    {
        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        if (flag.equals("num")) {
            dateFrom = "0";
            dateTo = "0";
            currentPage = 0;
            if (et_order_number.getText().toString().equals("")) {
                progress = new CustomDialogProgress();
                progress.init(getActivity());
                showDialog.initDialog(getString(R.string.enterOrderNum), getActivity());
            }
            else {
                orderId = "0";
                orderId = et_order_number.getText().toString();
                getAllInvoiceFromApi(currentPage, dateFrom, dateTo, orderId);
            }
        } // choose search with order number
        else if (flag.equals("date")) {
            if (dateFrom.equals("0") || dateTo.equals("0")) {
                showDialog.initDialog(getString(R.string.enterVaildDate), getActivity());
            }
            else
                {
                if (date_picker_type.equals("new_version")) {
                    getAllInvoiceFromApi(currentPage, dateFrom, dateTo, orderId);
                }
                else if (date_picker_type.equals("old_version")){
                    if (date2.before(date1)) {
                        showDialog.initDialog(getString(R.string.enterVaildDate), getActivity());
                        return;
                    }
                    else
                    {
                        getAllInvoiceFromApi(currentPage, dateFrom, dateTo, orderId);

                    }
                }
            }
        } // choose search with date

    } // function of onClickSearch


}
