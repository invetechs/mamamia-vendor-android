package com.invetechs.mamamiavendorversion.View.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.DatePicker;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Config.ToolbarConfig;
import com.invetechs.mamamiavendorversion.Language.CustomTypefaceSpan;
import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.Language.MyContextWrapper;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;
import com.invetechs.mamamiavendorversion.View.Fragment.ChangeLanguageFragment;
import com.invetechs.mamamiavendorversion.View.Fragment.ClientBanquetOrderFragment;
import com.invetechs.mamamiavendorversion.View.Fragment.ContactUsFragment;
import com.invetechs.mamamiavendorversion.View.Fragment.InvoiceFragment;
import com.invetechs.mamamiavendorversion.View.Fragment.KitchenFragment;
import com.invetechs.mamamiavendorversion.View.Fragment.OrderArchivesFragment;
import com.invetechs.mamamiavendorversion.View.Fragment.OrderFragment;
import com.invetechs.mamamiavendorversion.View.Fragment.PointsAndRulesFragment;
import com.invetechs.mamamiavendorversion.View.Fragment.ProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;

public class HomePageScreen extends Language implements
        NavigationView.OnNavigationItemSelectedListener {


    // bind views
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.tool_bar)
    Toolbar tool_bar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.nav_view)
    NavigationView nav_view;

    // vars
    InvoiceFragment invoiceFragment;
    String fragment = "", userType = "vendor";
    SharedPreferences preferences;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    SharedPreferences.Editor editor;
    ToolbarConfig toolBarConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_screen);
        checkLanguage(); // function of checkLanguage
        ButterKnife.bind(this);
        setToolBarConfig();
        getIntetnt();
        initNavigationMenu(savedInstanceState); // navigation menu
        invoiceFragment = new InvoiceFragment();
    } // onCreate function


    private void setToolBarConfig() {
        toolBarConfig = new ToolbarConfig(this);
        toolBarConfig.setTitle(getString(R.string.home));
        toolBarConfig.addNotification();

        Log.i("QW", "menu activity notification");

    } // function of setToolBarConfig

    private void getIntetnt() {
        Intent intent = getIntent();
        if (intent != null) {
            fragment = intent.getStringExtra("fragment");
            if (fragment == null) {
                fragment = "";
            }
            if (fragment.equals("login")) {
                userType = intent.getStringExtra("userType");
            }
            Log.i("QP", "userType : " + userType);
        }
        Log.i("QP", "fragment 1: " + fragment);
    } // function of grtIntent

    @Override
    public void onBackPressed() {
//        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
//            drawer_layout.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new KitchenFragment()).commit();
        tv_title.setText(getString(R.string.home));
        tv_title.setTextColor(getResources().getColor(R.color.black));
    }// onBackPressed

    private void checkLanguage() {

        preferences = getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (preferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        } else if (preferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);

        }
    } // function of checkLanguage

    private void initNavigationMenu(Bundle savedInstanceState) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer_layout, tool_bar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.addDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(this);
        Log.i("QP", "fragment 2: " + fragment);

        if (userType.equals("vendor")) {
            if (savedInstanceState == null && (fragment.equals("") || fragment.equals("login"))) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new KitchenFragment()).commit();
                nav_view.setCheckedItem(R.id.home);
                tv_title.setText(getString(R.string.home));
                tv_title.setTextColor(getResources().getColor(R.color.black));
            }
            if (fragment.equals("order")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrderFragment()).commit();
                tv_title.setText(getString(R.string.orders));
                tv_title.setTextColor(getResources().getColor(R.color.black));
            }
            if (fragment.equals("banquet")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ClientBanquetOrderFragment()).commit();
                tv_title.setText(getString(R.string.clientBanquetOrders));
                tv_title.setTextColor(getResources().getColor(R.color.black));
            }
        } // user login vendor

        else if (userType.equals("cashier")) {
            if (savedInstanceState == null && fragment.equals("login")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrderFragment()).commit();
//                nav_view.setCheckedItem(R.id.home);
                tv_title.setText(getString(R.string.orders));
                tv_title.setTextColor(getResources().getColor(R.color.black));
            }
            if (fragment.equals("order")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrderFragment()).commit();
                tv_title.setText(getString(R.string.orders));
                tv_title.setTextColor(getResources().getColor(R.color.black));
            }
            if (fragment.equals("banquet")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ClientBanquetOrderFragment()).commit();
                tv_title.setText(getString(R.string.clientBanquetOrders));
                tv_title.setTextColor(getResources().getColor(R.color.black));
            }
        } // user login as cashier

        NavigationView navView = findViewById(R.id.nav_view);
        Menu m = navView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            //for aapplying a font to subMenu ...
            if (userType.equals("cashier"))
                if (mi.getItemId() == R.id.nav_orders) {
                    mi.setTitle(getString(R.string.kitchens));
                }
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);

                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
    } // initialize menu

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.home:
                if (userType.equals("vendor")) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new KitchenFragment()).commit();
                    tv_title.setText(getString(R.string.restaurant_menu));
                    tv_title.setTextColor(getResources().getColor(R.color.black));
                    break;
                } else if (userType.equals("cashier")) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrderFragment()).commit();
                    tv_title.setText(getString(R.string.orders));
                    tv_title.setTextColor(getResources().getColor(R.color.black));
                    break;
                }

            case R.id.nav_orders:
                if (userType.equals("vendor")) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrderFragment()).commit();
                    tv_title.setText(getString(R.string.orders));
                    tv_title.setTextColor(getResources().getColor(R.color.black));
                    break;
                } else if (userType.equals("cashier")) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new KitchenFragment()).commit();
                    tv_title.setText(getString(R.string.restaurant_menu));
                    tv_title.setTextColor(getResources().getColor(R.color.black));
                    break;
                }


            case R.id.client_meal:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ClientBanquetOrderFragment()).commit();
                tv_title.setText(getString(R.string.clientBanquetOrders));
                tv_title.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.nav_archive:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrderArchivesFragment()).commit();
                tv_title.setText(getString(R.string.archive));
                tv_title.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.my_point:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new PointsAndRulesFragment()).commit();
                tv_title.setText(getString(R.string.rules));
                tv_title.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.nav_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProfileFragment()).commit();
                tv_title.setText(getString(R.string.profile));
                tv_title.setTextColor(getResources().getColor(R.color.black));
                break;

            case R.id.nav_invoice:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new InvoiceFragment()).commit();
                tv_title.setText(getString(R.string.invoice));
                tv_title.setTextColor(getResources().getColor(R.color.black));

                break;

            case R.id.nav_language:
                tv_title.setTextColor(getResources().getColor(R.color.black));
                tv_title.setText(getString(R.string.language));
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ChangeLanguageFragment()).commit();
                break;

            case R.id.nav_contact_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ContactUsFragment()).commit();
                tv_title.setText(getString(R.string.contact_us));
                break;

            case R.id.nav_share:
                Intent ii = new Intent(android.content.Intent.ACTION_SEND);
                ii.setType("text/plain");
                String shareBodyText = "Your shearing message goes here";
                ii.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject/Title");
                ii.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(ii, "Choose sharing method"));
                break;

            case R.id.nav_rate_us:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.youtube"));
                startActivity(intent);
                break;

            case R.id.nav_logout:
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("isLogin", "false");
                editor.putString("id", "0");
                editor.apply();
                Log.i("QP", "language : log out : " + preferences.getString("language", "en"));
                Intent log_out = new Intent(HomePageScreen.this, LoginScreen.class);
                startActivity(log_out);
                finish();
                break;
        }

        drawer_layout.closeDrawer(GravityCompat.START);
        return true;
    } // function of onNavigationItemSelected

    private void applyFontToMenuItem(MenuItem mi) {

        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/GESSTwoMedium.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);

    } // function font


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.onDestroy();

    }
} // class of HomePageScreen
