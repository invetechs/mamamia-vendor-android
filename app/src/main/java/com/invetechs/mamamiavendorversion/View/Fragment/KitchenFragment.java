package com.invetechs.mamamiavendorversion.View.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.invetechs.mamamiavendorversion.Adapter.KitchenAdapter;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.BranchApi.SelectAllKitchen;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.SelectAllKitchenResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class KitchenFragment extends Fragment {

    // bind views
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    // vars
    SharedPreferences sharedPreferences;
    List<SelectAllKitchenResultModel.KitchensdataBean> branchArrayList = new ArrayList<>();
    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences.Editor editor;
    KitchenAdapter kitchenAdapter;
    String code= "";
    SharedPreferences prefs ;
    public final String MY_PREFS_NAME = "MyPrefsFile";
    String id ;
    RetrofiUrlConnection connection;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kitchen,container,false);
        ButterKnife.bind(this,view);
        getData();
        initLanguage();
        getAllKitchenData();
        return view;
    } // onCreateView

    private void getData() {
        prefs = getContext().getSharedPreferences(MY_PREFS_NAME,Context.MODE_PRIVATE);
        id = prefs.getString("id","0");
    } // get id

    private void initLanguage() {
        sharedPreferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getActivity().getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);

        }

        else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);

        }

    } // initialize language

    private void initRecyclerView() {

        kitchenAdapter = new KitchenAdapter(getContext(), branchArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(kitchenAdapter);
    } // initialize recycler view

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    public void getAllKitchenData()
    {
        progress = new CustomDialogProgress();
        progress.init(getActivity());

        handler =   new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
       /* new Thread() {
            public void run() {*/
//Retrofit
                connection = new RetrofiUrlConnection(getActivity());
                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                SelectAllKitchen selectAllKitchen = retrofit.create(SelectAllKitchen.class);

                final Call<SelectAllKitchenResultModel> getInterestConnection = selectAllKitchen.getAllKitchen(id);

                getInterestConnection.enqueue(new Callback<SelectAllKitchenResultModel>() {
                    @Override
                    public void onResponse(Call<SelectAllKitchenResultModel> call, Response<SelectAllKitchenResultModel> response) {
                        try {

                            String code = response.body().getCode();
                            if (code.equals("200"))
                            {
                                if (response.body() != null)
                                {
                                    branchArrayList = response.body().getKitchensdata();
                                    initRecyclerView();

                                }
                                else
                                {
                                    //showDialog.initDialog(getString(R.string.retry), getActivity());

                                }

                            }
                            else if (code.equals("1313"))
                            {
                                //showDialog.initDialog(getString(R.string.no_kitchen), getActivity());

                            }

                            Log.i("QP", "code : " +code+" : userId : "+id);
                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<SelectAllKitchenResultModel> call, Throwable t) {
                        //showDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
                // Retrofit
       /*     }
        }.start();*/

    } // function of getAllKitchen

    @Override
    public void onDestroy() {
        super.onDestroy();
        connection.onDestroy();
        Thread.interrupted();
        //showDialog.destroy();
        handler.removeCallbacksAndMessages(null);
    }
} // class of KitchenFragment
