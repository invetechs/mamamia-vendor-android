package com.invetechs.mamamiavendorversion.View.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.LoginApi.LoginApi;
import com.invetechs.mamamiavendorversion.Model.Class.Model.User;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;

import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginScreen extends Language {

    // bind view
    @BindView(R.id.et_mobile)
    EditText et_mobile;

    @BindView(R.id.et_password)
    EditText et_password;

    @BindView(R.id.btn_login)
    Button btn_login;

    Handler handler;
    Dialog progress;

    String mobile, password , userType = "vendor";

    // vars
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    LinearLayout layout;
    ContentLoadingProgressBar contentLoadingProgressBar;
    ShowDialog showDialog = new ShowDialog();
    String categoty = "vendor,cashier";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        initLanguage();
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    } // function of onCreate

    private void initLanguage() {

        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        Log.i("QP", "language : login " + sharedPreferences.getString("language", "en"));

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }
    } // initialize language and font

    @OnClick(R.id.btn_login)
    public void clickLogin() {

        if (!ValidateMobile() | !ValidatePassword()) {

            return;
        }

        mobile = et_mobile.getText().toString();
        password = et_password.getText().toString();
        login();

    } // button login click

    public boolean ValidatePassword() {
        String passwordInput = et_password.getText().toString().trim();
        if (passwordInput.isEmpty()) {
            et_password.setError(getString(R.string.field_cant_empty));
            return false;
        } else if (passwordInput.length() < 4) {
            et_password.setError(getString(R.string.valid_password_number));
            return false;
        } else {
            et_password.setError(null);
            return true;
        }
    } // validation for password


    private boolean ValidateMobile() {

        String Mobile = et_mobile.getText().toString().trim();

        if (Mobile.isEmpty()) {
            et_mobile.setError(getString(R.string.field_cant_empty));
            return false;
        }

        if (Mobile.length() == 10)
        {
            if (!(Mobile.charAt(0) == '0' && Mobile.charAt(1) == '5')) {
                et_mobile.setError(getString(R.string.enter_correct_number));
                return false;
            }
        }
        else
            {
            et_mobile.setError(getString(R.string.enter_correct_number));
        }


        return true;
    } // validation for mobile phone

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void login() {
        progress = new Dialog(this);
        progress.setContentView(R.layout.progress_loading_dialog);
        progress.setCancelable(false);

        layout = progress.findViewById(R.id.layoutDialog);
        contentLoadingProgressBar = progress.findViewById(R.id.progressLoading);
        contentLoadingProgressBar.setVisibility(View.VISIBLE);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progress.getWindow()
                .setLayout((int) (getScreenWidth(this) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);


        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
       /* new Thread() {
            public void run() {*/
//Retrofit
                RetrofiUrlConnection connection = new RetrofiUrlConnection(LoginScreen.this);
                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final LoginApi userApi = retrofit.create(LoginApi.class);

                final Call<ResultModelUser> getInterestConnection = userApi.login(mobile, password , categoty);

                getInterestConnection.enqueue(new Callback<ResultModelUser>() {
                    @Override
                    public void onResponse(Call<ResultModelUser> call, Response<ResultModelUser> response) {
                        try {

                            String code = response.body().getCode();

                            Log.i("QP", "code" + code);

                            response.body();

                            if (code.equals("200")) {
                                if (response.body() != null) {
                                    ResultModelUser.UserdataBean user = response.body().getUserdata();

                                    userType = user.getCategory();
                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("isLogin", "true");
                                    editor.putString("id", user.getId());
                                    editor.putString("token",user.getToken());
                                    editor.putString("userType",userType);
                                    editor.apply();
                                    Intent intent = new Intent(LoginScreen.this, HomePageScreen.class);
                                    intent.putExtra("userType",userType);
                                    intent.putExtra("fragment","login");
                                    startActivity(intent);
                                    finish();
                                } else {
                                    showDialog.initDialog(getString(R.string.retry), LoginScreen.this);

                                }

                            } // login success

                            else if (code.equals("1313")) {
                                showDialog.initDialog(getString(R.string.admin_confirm), LoginScreen.this);
                                progress.dismiss();
                            } // user need admin confirmation
                            else if (code.equals("1314")) {
                                showDialog.initDialog(getString(R.string.verify_code), LoginScreen.this);
                                progress.dismiss();

                            } // user need to verify code
                            else if (code.equals("1315")) {
                                showDialog.initDialog(getString(R.string.password_invalid), LoginScreen.this);
                                progress.dismiss();

                            } // invaild password
                            else if (code.equals("1316"))
                            {
                                showDialog.initDialog(getString(R.string.invalid_mobile), LoginScreen.this);
                                progress.dismiss();

                            } // invaild mobile
                            progress.dismiss();
                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<ResultModelUser> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), LoginScreen.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit
          /*  }

        }.start();*/
    } // function of login

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
    }
} // class of LoginScreen
