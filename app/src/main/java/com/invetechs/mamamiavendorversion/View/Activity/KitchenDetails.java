package com.invetechs.mamamiavendorversion.View.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.invetechs.mamamiavendorversion.Adapter.KitchenDetailAdapter;
import com.invetechs.mamamiavendorversion.Config.ToolbarConfig;
import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.BranchApi.GetKitchenDetails;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.BranchRetrofitResult;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.Hours;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.Kitchenimages;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.Kitchentags;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class KitchenDetails extends Language {


    // bind views
    @BindView(R.id.rec_view)
    RecyclerView recyclerView;

    @BindView(R.id.kitchen_name)
    TextInputEditText kitchen_name;

    @BindView(R.id.KD_enDes)
    TextInputEditText tx_englishDescription;

    @BindView(R.id.KD_mobile)
    TextInputEditText tx_mobile;

    @BindView(R.id.KD_anotherMobile)
    TextInputEditText tx_anotherMobile;

    @BindView(R.id.KD_minCharge)
    TextInputEditText tx_minCharge;

    @BindView(R.id.KD_address)
    TextInputEditText tx_address;

    @BindView(R.id.KD_deliveryCostPerKilo)
    TextInputEditText tx_deliveryCostPerKilo;

    @BindView(R.id.KD_maxInvoice)
    TextInputEditText tx_invoicePriceToFreeDelivery;

    @BindView(R.id.ch_meal)
    CheckBox ch_meal;

    @BindView(R.id.ch_banquet)
    CheckBox ch_banquet;

    @BindView(R.id.ch_dessert)
    CheckBox ch_dessert;

    @BindView(R.id.ch_beverge)
    CheckBox ch_beverge;

    // vars
    SharedPreferences sharedPreferences;
    private static int kitchenId = 0;
    ArrayList<String> categoryTag = new ArrayList<>();
    Handler handler;
    CustomDialogProgress progress;
    List<BranchRetrofitResult.OnekitchendataBean.KitchentagsBean> kitcehntags;
    List<BranchRetrofitResult.OnekitchendataBean.KitchenimagesBean> kitchenimages;
    ArrayList<Integer> imagesListId = new ArrayList<>();
    ArrayList<String> imagesListPath = new ArrayList<>();
    HashSet<String> dayIdSet = new HashSet<>();
    ArrayList<String> dayIdList = new ArrayList<>();
    List<BranchRetrofitResult.OnekitchendataBean.HoursBean> hours;
    String id, ar_title, ar_description, en_title, en_description, minicharge, phone, phone2, address, deliverypriceperkilo, delivermaxbill,defaultImage;
    ArrayList<Hours.FromToClass> fromToList = new ArrayList<>();
    ArrayList<ArrayList<Hours.FromToClass>> allFromToList = new ArrayList<>();
    KitchenDetailAdapter kitchenDetailAdapter;
    ShowDialog showDialog = new ShowDialog();
    ToolbarConfig toolBarConfig;
    TextSliderView textSliderView ;
    SliderLayout sliderShow ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitchen_details);
        ButterKnife.bind(this);
        setToolBarConfig();
        checkLanguage(); // check ar or en language
        initRecyclerView(); // list of times
        getKitchenIdFromKitchenList(); // get KitchenId from click on item
        getKitchenDetails(); // get details of kitchen from server
        initSliderImage();

    } // onCreate function

    private  void  initSliderImage()
    {
        textSliderView = new TextSliderView(getApplicationContext());
        sliderShow = findViewById(R.id.slider);
    } // function of initSliderImage

    private void setToolBarConfig()
    {
        toolBarConfig = new ToolbarConfig(this);
        toolBarConfig.setTitle(getString(R.string.restaurant_details));
        toolBarConfig.setBack(this);
        toolBarConfig.addNotification();
        Log.i("QW", "menu activity notification");

    } // function of setToolBarConfig

    @OnClick(R.id.img_back)
    public void getBack() {
        onBackPressed();
    } // press on back button

    private void checkLanguage() {
        sharedPreferences = this.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }
    } // function of checkLanguage

    private void initRecyclerView() {

        kitchenDetailAdapter = new KitchenDetailAdapter(this, allFromToList, dayIdList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(kitchenDetailAdapter);
        recyclerView.setNestedScrollingEnabled(false);


    } // function od initReclerView

    private void getKitchenIdFromKitchenList() {
        Intent iin = getIntent();
        Bundle b = iin.getExtras();


        if (b != null) {
            kitchenId = b.getInt("kitchenId");
        }
    } // function of getKitchenIdFromKitchenList

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getKitchenDetails() {

        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
      /*  new Thread() {
            public void run() {*/
//Retrofit
                RetrofiUrlConnection connection = new RetrofiUrlConnection(KitchenDetails.this);
                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final GetKitchenDetails kitchenApi = retrofit.create(GetKitchenDetails.class);

                final Call<BranchRetrofitResult> getConnection = kitchenApi.getKitchenDetailsApi(kitchenId);

                getConnection.enqueue(new Callback<BranchRetrofitResult>() {
                    @Override
                    public void onResponse(Call<BranchRetrofitResult> call, Response<BranchRetrofitResult> response) {
                        try {

                            response.body();
                            String code = response.body().getCode();
                            ar_title = response.body().getOnekitchendata().getAr_title();
                            ar_description = response.body().getOnekitchendata().getAr_description();
                            en_title = response.body().getOnekitchendata().getEn_title();
                            en_description = response.body().getOnekitchendata().getEn_description();
                            phone = response.body().getOnekitchendata().getPhone();
                            phone2 = response.body().getOnekitchendata().getPhone2();
                            minicharge = response.body().getOnekitchendata().getMinicharge();
                            deliverypriceperkilo = response.body().getOnekitchendata().getDeliverypriceperkilo();
                            delivermaxbill = response.body().getOnekitchendata().getDelivermaxbill();
                            address = response.body().getOnekitchendata().getAddress();
                            kitcehntags = response.body().getOnekitchendata().getKitchentags();
                            kitchenimages = response.body().getOnekitchendata().getKitchenimages();
                            hours = response.body().getOnekitchendata().getHours();
                            for (int i = 0; i < hours.size(); i++) {
                                dayIdSet.add(hours.get(i).getDay_id());
                                kitchenDetailAdapter.notifyDataSetChanged();
                            } // for loop

                            for (String s : dayIdSet) {
                                dayIdList.add(s);
                                kitchenDetailAdapter.notifyDataSetChanged();
                                Log.i("QS", "s : " + s);
                            }
                            for (int i = 0; i < dayIdList.size(); i++) {
                                fromToList = new ArrayList<>();

                                for (int j = 0; j < hours.size(); j++) {
                                    if (dayIdList.get(i).equals(hours.get(j).getDay_id())) {
                                        Hours.FromToClass h = new Hours.FromToClass(hours.get(j).getFrom(), hours.get(j).getTo());
                                        fromToList.add(h);
                                    }
                                } // for loop

                                allFromToList.add(fromToList);
                                kitchenDetailAdapter.notifyDataSetChanged();
                            } // for loop

                            defaultImage = response.body().getOnekitchendata().getImage();
                            imageSlider(kitchenimages);

                            for (int i = 0; i < kitcehntags.size(); i++) {
                                categoryTag.add(kitcehntags.get(i).getId());
                            }
                            Log.i("QP", " KitchenDetails : code : " + code);
                            setDataToKitchenDetailsActivity(); // set data to activity
                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString() + "   : kitchenId : " + kitchenId);
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<BranchRetrofitResult> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), KitchenDetails.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit
        /*    }

        }.start();*/
    } // function of getKitchenDEtails


    private void setDataToKitchenDetailsActivity() {

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            kitchen_name.setText(ar_title);
            tx_englishDescription.setText(ar_description);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            kitchen_name.setText(en_title);
            tx_englishDescription.setText(en_description);
        }


        tx_mobile.setText(phone);
        tx_anotherMobile.setText(phone2);
        tx_invoicePriceToFreeDelivery.setText(delivermaxbill);
        tx_minCharge.setText(minicharge);
        tx_address.setText(address);
        tx_deliveryCostPerKilo.setText(deliverypriceperkilo);

        for (int i = 0; i < kitcehntags.size(); i++) {
            String ID = kitcehntags.get(i).getId();
            if (ID.equals("1")) {
                ch_meal.setChecked(true);
                ch_meal.setEnabled(false);
            } // user choose meal
            if (ID.equals("2")) {
                ch_banquet.setChecked(true);
                ch_banquet.setEnabled(false);
            } // user choose banquet
            if (ID.equals("3")) {
                ch_dessert.setChecked(true);
                ch_dessert.setEnabled(false);
            } // user choose dessert
            if (ID.equals("4")) {
                ch_beverge.setChecked(true);
                ch_beverge.setEnabled(false);
            } // user choose dessert
        } // for loop
    } // function of setDataToKitchenDetailsActivity


    public void imageSlider(List<BranchRetrofitResult.OnekitchendataBean.KitchenimagesBean> urls) {

        if(urls.size() <= 0)
        {

            textSliderView
                    .image("https://mammamiaa.com/cpanel/upload/kitchen/" + defaultImage);
            sliderShow.addSlider(textSliderView);
            Log.e("QP", "Image : " + defaultImage);
        } // load default image

        else {
            for (int i = 0; i < urls.size(); i++) {

                imagesListPath.add(urls.get(i).getImage());
                imagesListId.add(urls.get(i).getId());
                textSliderView
                        .image("https://mammamiaa.com/cpanel/upload/kitchen/" + urls.get(i).getImage());
                sliderShow.addSlider(textSliderView);
                Log.e("QP", "Image : " + urls.get(i).getImage());
                textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                    @Override
                    public void onSliderClick(BaseSliderView slider) {
                  /*  Intent intent = new Intent(getApplicationContext(),image_item.class);
                    intent.putExtra("imageList",imagesUrl);
                    startActivity(intent)*/
                        ;
                    }
                });
            }
        }


    } // imageSlider function

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.onDestroy();
        sliderShow.removeAllSliders();
        sliderShow.stopAutoCycle();

    }
} // class of KitchenDetails
