package com.invetechs.mamamiavendorversion.View.Activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Adapter.MealComponentAdapter;
import com.invetechs.mamamiavendorversion.Config.ToolbarConfig;
import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi.SelectOneOrderDetails;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.OneOrderResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class InvoiceDetailsOrdersActivity extends Language {

    // Bind view

    @BindView(R.id.order_num)
    TextInputEditText order_num;

    @BindView(R.id.order_date)
    TextInputEditText order_date;

    @BindView(R.id.ed_day)
    TextView ed_day;

    @BindView(R.id.ed_hour)
    TextView ed_hour;

    @BindView(R.id.ed_minutes)
    TextView ed_minutes;

    @BindView(R.id.order_time)
    TextInputEditText order_time;

//    @BindView(R.id.tv_order_name)
//    TextView tv_order_name;

    @BindView(R.id.client_name)
    TextView client_name;

    @BindView(R.id.client_number)
    TextView client_number;

    @BindView(R.id.delivery_time)
    TextView delivery_time;

    @BindView(R.id.space_to_client)
    TextInputEditText space_to_client;

    @BindView(R.id.branch)
    TextInputEditText branch;

    @BindView(R.id.client_location)
    TextInputEditText client_location;

    @BindView(R.id.et_discount)
    TextInputEditText et_discount;

    @BindView(R.id.linear_discount)
    LinearLayout linear_discount;

    @BindView(R.id.customer_notes)
    TextInputEditText customer_notes;

    @BindView(R.id.vendor_notes)
    TextInputEditText vendor_notes;

    @BindView(R.id.total_price)
    TextInputEditText total_price;

    @BindView(R.id.total_invoice_price)
    TextInputEditText total_invoice_price;

    @BindView(R.id.order_name_recycler)
    RecyclerView recyclerView;

    @BindView(R.id.layout_invoice)
    LinearLayout layout_invoice;

    @BindView(R.id.ed_priceVat)
    TextInputEditText ed_priceVat;

    @BindView(R.id.ed_orderPrice)
    TextInputEditText ed_orderPrice;

    @BindView(R.id.tv_prepare_time)
    TextView tv_prepare_time;

    // vars
    MealComponentAdapter adapter;
    List<OneOrderResultModel.OneorderBean.MealsBean> mealsList = new ArrayList<>(), meals = new ArrayList<>();
    List<OneOrderResultModel.OneorderBean.MealsBean.AdditionsBean> additionsBeans = new ArrayList<>(), additions = new ArrayList<>();
    String orderNumber, needTime = "", day = "", hour = "", minute = "", discount = "" ,  price_vat = "" , order_price = "";
    String orderDate;
    Handler handler;
    CustomDialogProgress progress;
    OneOrderResultModel.OneorderBean oneorder;
    SharedPreferences.Editor editor;
    ShowDialog showDialog = new ShowDialog();
    ToolbarConfig toolBarConfig;
    String flag = "";

//    LinkedList<TableItem> datalist;
//    TableItem tableItem1 ,tableItem2 , tableItem3  ;
//    String [] textPrintTitle , textPrintInvoice , textPrint ;
//    BaseApp baseApp ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_details_orders);
        ButterKnife.bind(this);
        setToolBarConfig();
        checkLanguage();
        getOrderNumber();
        getOneInvoiceDetails();
        initMealComponentList(); // intialize recycler meal component
       // initPrinter();
    }

    private void setToolBarConfig() {
        toolBarConfig = new ToolbarConfig(this);
        toolBarConfig.setTitle(getString(R.string.invoice_details));
        toolBarConfig.setBack(this);
        toolBarConfig.addNotification();
        Log.i("QW", "menu activity notification");

    } // function of setToolBarConfig

    private void checkLanguage() {
        sharedPreferences = this.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }
    } // function of checkLanguage

    private void initMealComponentList() {
        adapter = new MealComponentAdapter(getApplicationContext(), mealsList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

    }// intialize recycler meal component

    private void fillMealComponentList() {

        for (int i = 0; i < meals.size(); i++) {
            mealsList.add(meals.get(i));
            adapter.notifyDataSetChanged();
        } // for loop

    } // fill component meal from api


    private void fillAdditions() {

        for (int i = 0; i < additions.size(); i++) {
            additionsBeans = oneorder.getMeals().get(i).getAdditions();
            additionsBeans.add(additions.get(i));
            adapter.notifyDataSetChanged();
        } // for loop
        Log.i("QP", "addition list " + additions.size());


    } // fill component meal from api

    private void getOrderNumber() {
        if (getIntent() != null) {
            orderNumber = getIntent().getStringExtra("invoice_id");
            orderDate = getIntent().getStringExtra("invoice_date");
            flag = getIntent().getStringExtra("flag");
            Log.i("QP", "recieve order number : " + orderNumber);
            Log.i("QP", "recieve order date : " + orderDate);

        }
        displayViewOrPrintScreen(flag);
    } // get order number to fetch invoice details

    private  void displayViewOrPrintScreen(String flag)
    {
        if(flag.equals("print"))
        {
            layout_invoice.setBackgroundColor(getResources().getColor(R.color.grayN));
        }
    } // function of displayViewOrPrintScreen

    @OnClick(R.id.img_back)
    public void backImage() {
        onBackPressed();
    } // get back image

    private void getDetailsData() {

        if (sharedPreferences.getString("language", "ar").equals("en")) {
//            if ((oneorder.getMeals().size() != 0)) {
//                tv_order_name.setText(oneorder.getMeals().get(0).getEn_title());
//            }

            branch.setText(oneorder.getKitchen().getEn_title());

        } else if (sharedPreferences.getString("language", "ar").equals("ar")) {
//            if ((oneorder.getMeals().size() != 0)) {
//                tv_order_name.setText(oneorder.getMeals().get(0).getAr_title());
//
//            }
            branch.setText(oneorder.getKitchen().getAr_title());

        }
        needTime = oneorder.getNeededtime();
        int d = 0, h = 0, m = 0;
        d = (Integer.parseInt(needTime)) / (24 * 60);
        h = ((Integer.parseInt(needTime)) - (d * (24 * 60))) / 60;
        m = ((Integer.parseInt(needTime)) - (d * (24 * 60))) - (h * 60);
        day = String.valueOf(d);
        hour = String.valueOf(h);
        minute = String.valueOf(m);
        Log.i("QP", "needed time" + needTime);
        meals = oneorder.getMeals();


        if (oneorder.getDiscountpoint().equals("0")) {
            linear_discount.setVisibility(View.GONE);
        } else if (!oneorder.getDiscountpoint().equals("0")) {
            discount = oneorder.getDiscountpoint();

        }

        price_vat = oneorder.getPriceVat();

        double priceVat = Double.parseDouble(price_vat);
        Log.i("QP", "priceVat : " + priceVat);

        double totalDilevery = Double.parseDouble(oneorder.getDeliverytotalprice());
        Log.i("QP", "totalDilevery : " + totalDilevery);

        order_price = oneorder.getMealstotalprice();
        Log.i("QP", "order price price : " + order_price);

        setText();
        fillMealComponentList();
        //fillAdditions();

    } // set data to text

    private void setText() {
        ed_day.setText(day + " " + getResources().getString(R.string.dayO));
        ed_hour.setText(hour + " " + getResources().getString(R.string.hourO));
        ed_minutes.setText(minute + " " + getResources().getString(R.string.Minute));
        String dateTime = oneorder.getCreated_at();
        String[] timeString = dateTime.split(" ");
        //order_num.setText(oneorder.getId());
        order_num.setText(orderNumber);
        order_date.setText(timeString[0]);
        order_time.setText(timeString[1]);
        client_name.setText(oneorder.getUser().getName());
        client_number.setText(oneorder.getUser().getMobilenumber());
        delivery_time.setText(String.valueOf(oneorder.getTimetodeliver()) + "   " + getString(R.string.minuteO));
        space_to_client.setText(oneorder.getDistancetouser());
        client_location.setText(oneorder.getUserplace());
        customer_notes.setText(oneorder.getUsernote());
        vendor_notes.setText(oneorder.getVendornote());
        total_price.setText(oneorder.getDeliverytotalprice());
        total_invoice_price.setText(oneorder.getTotalprice());
        et_discount.setText(discount);
      //      delivery_time.setText(oneorder.getAr_partyname());
        ed_priceVat.setText(price_vat + "" );
        ed_orderPrice.setText(order_price + "");

    } // set fields with data

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getOneInvoiceDetails() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
    /*    new Thread() {
            public void run() {*/
                //Retrofit

                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final SelectOneOrderDetails orderApi = retrofit.create(SelectOneOrderDetails.class);

                final Call<OneOrderResultModel> getInterestConnection = orderApi.getOneOrderDetailsApi(orderNumber);

                getInterestConnection.enqueue(new Callback<OneOrderResultModel>() {
                    @Override
                    public void onResponse(Call<OneOrderResultModel> call, Response<OneOrderResultModel> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "invoice details code" + code);
                            Log.i("QP", "invoice details response" + response.body().getOneorder());

                            if (code.equals("200")) {
                                if (!(response.body().equals(null))) {
                                    oneorder = response.body().getOneorder();
                                    getDetailsData();
                                } else {
                                    showDialog.initDialog(getString(R.string.retry), InvoiceDetailsOrdersActivity.this);
                                }

                            } // get the details

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<OneOrderResultModel> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), InvoiceDetailsOrdersActivity.this);
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit
           /* }

        }.start();*/
    } // function of get details

//    @OnClick(R.id.print)
//    public void printInvoice()
//    {
  //      if (baseApp.isAidl()) {
//
//            Log.i("QP","size : if : "+datalist.size());
//            AidlUtil.getInstance().printTable(datalist);
//        } else {
//
//            BluetoothUtil.sendData(ESCUtil.printBitmap(BytesUtil.initTable(6, 12)));
//            Log.i("QP","size : else : "+datalist.size());
//
//        }
//    } // function of printInvoice

//    private void initPrinter()
//    {
//        AidlUtil.getInstance().connectPrinterService(this);
//        AidlUtil.getInstance().initPrinter();
//
//
//        tableItem1 = new TableItem();
//        tableItem2 = new TableItem();
//        tableItem3 = new TableItem();
//
//        datalist = new LinkedList<>();
//        baseApp = new BaseApp();
//
//        textPrintTitle = new String[] {"MamaMia"};
//        tableItem1.setText(textPrintTitle);
//        tableItem1.setWidth(new int[]{12});
//        tableItem1.setAlign(new int[]{0});
//        datalist.add(tableItem1);
//        textPrintInvoice = new String[]{"Invoice : ",orderNumber};
//        tableItem2.setText(textPrintInvoice);
//        tableItem2.setWidth(new int[]{6,6});
//        tableItem2.setAlign(new int[]{2,5});
//        datalist.add(tableItem2);
//        textPrint = new String[]{"mobile : +023123","delivery : 50 SR","total price : 3000 SR"};
//        tableItem3.setText(textPrint);
//        tableItem3.setWidth(new int[]{1,2,3});
//        tableItem3.setAlign(new int[]{1,7,9});
//        datalist.add(tableItem3);
//
//    } // function of initPrinter

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.onDestroy();

    }
}
