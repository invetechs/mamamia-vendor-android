package com.invetechs.mamamiavendorversion.View.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;

import static android.content.Context.MODE_PRIVATE;

public class ContactUsFragment extends Fragment {


    // bind views
    @BindView(R.id.tx_title)
    TextView tx_title;

    @BindView(R.id.tx_mobile)
    TextView tx_mobile;

    @BindView(R.id.tx_website)
    TextView tx_website;

    @BindView(R.id.tx_mail)
    TextView tx_mail;

    //vars
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_us_fragment, container, false);
        ButterKnife.bind(this, view);
        initLanguage();
        return view;
    }

    private void initLanguage() {

        sharedPreferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            tx_title.setTypeface(font);
            tx_mobile.setTypeface(font);
            tx_mail.setTypeface(font);
            tx_website.setTypeface(font);

            Log.i("QP", "ar");

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            tx_title.setTypeface(font);
            tx_mobile.setTypeface(font);
            tx_mail.setTypeface(font);
            tx_website.setTypeface(font);
            Log.i("QP", "en");
        }


    }//initialize language

    @OnClick(R.id.img_map)
    public void openMap() {
        String url = "https://www.google.com/maps/place/24%C2%B041'52.4%22N+46%C2%B043'22.9%22E/@24.697895,46.723015,16z/data=!4m5!3m4!1s0x0:0x0!8m2!3d24.6978951!4d46.7230148?hl=en-US";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);

    } //open map location on click

    @OnClick(R.id.linear_website)
    public void openWebsite() {
        String url = "http://www.invetechs.com/";
        Intent openWebSite = new Intent(Intent.ACTION_VIEW);
        openWebSite.setData(Uri.parse(url));
        startActivity(openWebSite);

    } //open website on click

    @OnClick(R.id.linear_mobile)
    public void makeCall() {
        String number = "(+966) 558359836";
        Intent dialNumber = new Intent(Intent.ACTION_DIAL);
        dialNumber.setData(Uri.parse("tel:" + number));
        startActivity(dialNumber);
    } // make call on click

    @OnClick(R.id.img_instagram)
    public void goToInstagram() {
        String url = "https://www.instagram.com/";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open instagram link on click

    @OnClick(R.id.img_twitter)
    public void goToTwitter() {
        String url = "https://twitter.com/";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open twitter link on click

    @OnClick(R.id.img_facebook)
    public void goToFacebook() {
        String url = "http://www.facebook.com/invetechs";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open facebook link on click

    @OnClick(R.id.img_youtube)
    public void goToYoutube() {
        String url = "https://www.youtube.com/";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open youtube link on click

    @OnClick(R.id.img_linkedIn)
    public void goToLinkedIn() {
        String url = "https://www.linkedin.com/invetechs";
        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse(url));
        startActivity(website);
    } // open linkedIn link on click
}
