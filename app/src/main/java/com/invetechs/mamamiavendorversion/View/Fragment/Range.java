//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.invetechs.mamamiavendorversion.View.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.CalendarView.OnDateChangeListener;

import com.ddd.daterangepicker.DateRangePicker;
import com.ddd.daterangepicker.R.id;
import com.ddd.daterangepicker.R.layout;
import com.invetechs.mamamiavendorversion.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Range extends Dialog implements OnClickListener, OnTabSelectedListener {
    final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    DateRangePicker.OnCalenderClickListener onCalenderClickListener;

    Context context;
    ViewFlipper viewFlipper;
    CalendarView startDateCalendarView;
    CalendarView endDateCalendarView;
    TextView startDate;
    TextView endDate;
    TextView btnNegative;
    TextView btnPositive;
    long selectedFromDate;
    long selectedToDate = 0L;
    Calendar startDateCal = Calendar.getInstance();
    Calendar endDateCal = Calendar.getInstance();
    TabLayout tabLayout;
    String startDateTitle = getContext().getString(R.string.from);
    String endDateTitle = getContext().getString(R.string.to);
    String startDateError = getContext().getString(R.string.select_start_date);
    String endDateError = getContext().getString(R.string.select_end_date);

    public Range(@NonNull Context context, DateRangePicker.OnCalenderClickListener onCalenderClickListener) {
        super(context);
        this.context = context;
        this.onCalenderClickListener = onCalenderClickListener;

    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(1);
        this.setContentView(layout.date_range_picker);
        this.initView();
    }

    private void initView() {
        this.tabLayout = (TabLayout) this.findViewById(id.drp_tabLayout);
        this.viewFlipper = (ViewFlipper) this.findViewById(id.drp_viewFlipper);
        this.startDateCalendarView = (CalendarView) this.findViewById(id.drp_calStartDate);
        this.endDateCalendarView = (CalendarView) this.findViewById(id.drp_calEndDate);
        this.startDate = (TextView) this.findViewById(id.drp_tvStartDate);
        this.endDate = (TextView) this.findViewById(id.drp_tvEndDate);
        this.btnNegative = (TextView) this.findViewById(id.drp_btnNegative);
        this.btnPositive = (TextView) this.findViewById(id.drp_btnPositive);
        this.startDateCalendarView.setOnDateChangeListener(new OnDateChangeListener() {
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                Range.this.startDateCal.set(year, month, dayOfMonth);
                Range.this.selectedFromDate = Range.this.startDateCal.getTimeInMillis();
                Range.this.startDate.setText(Range.this.dateFormatter.format(Range.this.startDateCal.getTime()));
                Range.this.tabLayout.getTabAt(1).select();
            }
        });
        this.endDateCalendarView.setOnDateChangeListener(new OnDateChangeListener() {
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                Range.this.endDateCal.set(year, month, dayOfMonth);
                Range.this.selectedToDate = Range.this.endDateCal.getTimeInMillis();
                Range.this.endDate.setText(Range.this.dateFormatter.format(Range.this.endDateCal.getTime()));
            }
        });
        this.tabLayout.addTab(this.tabLayout.newTab().setText(this.startDateTitle), true);
        this.tabLayout.addTab(this.tabLayout.newTab().setText(this.endDateTitle));
        this.btnPositive.setOnClickListener(this);
        this.btnNegative.setOnClickListener(this);
        this.tabLayout.addOnTabSelectedListener(this);
    }

    public void onClick(View view) {
        if (view == this.btnPositive) {
            if (TextUtils.isEmpty(this.startDate.getText().toString())) {
                Snackbar.make(this.startDate, this.startDateError, -1).show();
            } else if (TextUtils.isEmpty(this.endDate.getText().toString())) {
                Snackbar.make(this.endDate, this.endDateError, -1).show();
            } else {
                this.onCalenderClickListener.onDateSelected(this.startDate.getText().toString(), this.endDate.getText().toString());
                this.dismiss();
            }
        } else if (view == this.btnNegative) {
            this.dismiss();
        }

    }

    public void onTabSelected(Tab tab) {
        if (tab.getPosition() == 0) {
            this.viewFlipper.showPrevious();
        } else {
            this.showToDateCalender();
        }

    }

    private void showToDateCalender() {
        this.endDateCalendarView.setMinDate(0L);
        this.endDateCalendarView.setMinDate(this.selectedFromDate);
        if (this.selectedToDate != 0L) {
            this.endDateCalendarView.setDate(this.selectedToDate);
        }

        this.viewFlipper.showNext();
        if (!TextUtils.isEmpty(this.endDate.getText()) && this.endDateCal.before(this.startDateCal)) {
            this.endDate.setText(this.startDate.getText().toString());
        }

    }

    public void onTabUnselected(Tab tab) {
    }

    public void onTabReselected(Tab tab) {
    }

    public void setBtnPositiveText(String text) {
        this.btnPositive.setText(text);
    }

    public void setBtnNegativeText(String text) {
        this.btnNegative.setText(text);
    }

    public SimpleDateFormat getDateFormatter() {
        return this.dateFormatter;
    }

    public String getStartDateTitle() {
        return this.startDateTitle;
    }

    public void setStartDateTitle(String startDateTitle) {
        this.startDateTitle = startDateTitle;
    }

    public CalendarView getStartDateCalendarView() {
        return this.startDateCalendarView;
    }

    public CalendarView getEndDateCalendarView() {
        return this.endDateCalendarView;
    }

    public TextView getStartDate() {
        return this.startDate;
    }

    public TextView getEndDate() {
        return this.endDate;
    }

    public TextView getBtnNegative() {
        return this.btnNegative;
    }

    public TextView getBtnPositive() {
        return this.btnPositive;
    }

    public TabLayout getTabLayout() {
        return this.tabLayout;
    }

    public String getEndDateTitle() {
        return this.endDateTitle;
    }

    public void setEndDateTitle(String endDateTitle) {
        this.endDateTitle = endDateTitle;
    }

    public String getStartDateError() {
        return this.startDateError;
    }

    public void setStartDateError(String startDateError) {
        this.startDateError = startDateError;
    }

    public String getEndDateError() {
        return this.endDateError;
    }

    public void setEndDateError(String endDateError) {
        this.endDateError = endDateError;
    }

    public interface OnCalenderClickListener {
        void onDateSelected(String var1, String var2);
    }
}
