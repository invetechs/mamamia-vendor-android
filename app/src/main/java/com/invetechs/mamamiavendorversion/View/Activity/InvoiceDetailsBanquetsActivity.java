package com.invetechs.mamamiavendorversion.View.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Adapter.MealComponentAdapter;
import com.invetechs.mamamiavendorversion.Config.ToolbarConfig;
import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi.SelectOneOrderDetails;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.OneOrderResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class InvoiceDetailsBanquetsActivity extends Language {

    // Bind view

    @BindView(R.id.order_num)
    TextInputEditText order_num;

    @BindView(R.id.party_date)
    TextInputEditText party_date;

    @BindView(R.id.party_time)
    TextInputEditText party_time;

    @BindView(R.id.et_prepare_time)
    TextInputEditText et_prepare_time;

    @BindView(R.id.tv_meal_menu)
    TextView tv_meal_menu;

    @BindView(R.id.party_name)
    TextView party_name;

    @BindView(R.id.client_number)
    TextView client_number;


    @BindView(R.id.space_to_client)
    TextInputEditText space_to_client;

    @BindView(R.id.branch)
    TextInputEditText branch;

    @BindView(R.id.party_location)
    TextInputEditText party_location;

    @BindView(R.id.customer_notes)
    TextInputEditText customer_notes;

    @BindView(R.id.vendor_notes)
    TextInputEditText vendor_notes;

    @BindView(R.id.total_price)
    TextInputEditText total_price;

    @BindView(R.id.total_invoice_price)
    TextInputEditText total_invoice_price;

    // vars
    MealComponentAdapter adapter;
    List<OneOrderResultModel.OneorderBean.MealsBean> mealsList = new ArrayList<>(), meals = new ArrayList<>();
    String orderDate = "", orderNumber = "", prepare_time = "";
    Handler handler;
    CustomDialogProgress progress;
    OneOrderResultModel.OneorderBean oneorder;
    SharedPreferences.Editor editor;
    ShowDialog showDialog = new ShowDialog();
    ToolbarConfig toolBarConfig;

    String flag = "";

    @BindView(R.id.layout_invoice)
    LinearLayout layout_invoice;


//    LinkedList<TableItem> datalist;
//    TableItem tableItem1 ,tableItem2 , tableItem3  ;
//    String [] textPrintTitle , textPrintInvoice , textPrint ;
//    BaseApp baseApp ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_details_banquets);
        ButterKnife.bind(this);
        setToolBarConfig();
        checkLanguage();
        getOrderNumber();
        getOneInvoiceDetails();
        //  initPrinter();
    }

    private void setToolBarConfig() {
        toolBarConfig = new ToolbarConfig(this);
        toolBarConfig.setTitle(getString(R.string.invoice_details));
        toolBarConfig.setBack(this);
        toolBarConfig.addNotification();
        Log.i("QW", "menu activity notification");

    } // function of setToolBarConfig


    private void checkLanguage() {
        sharedPreferences = this.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }
    } // function of checkLanguage

    private void fillMealComponentList() {

        for (int i = 0; i < meals.size(); i++) {
            mealsList.add(meals.get(i));
            adapter.notifyDataSetChanged();
        } // for loop

    } // fill component meal from api

    private void getOrderNumber() {
        if (getIntent() != null) {
            orderNumber = getIntent().getStringExtra("invoice_id");
            orderDate = getIntent().getStringExtra("invoice_date");
            flag = getIntent().getStringExtra("flag");
            Log.i("QP", "recieve order number : " + orderNumber);
            Log.i("QP", "recieve order date : " + orderDate);

        }
        displayViewOrPrintScreen(flag);
    } // get order number to fetch invoice details

    private void displayViewOrPrintScreen(String flag) {
        if (flag.equals("print")) {
            layout_invoice.setBackgroundColor(getResources().getColor(R.color.grayN));
        }
    } // function of displayViewOrPrintScreen

    @OnClick(R.id.img_back)
    public void backImage() {
        onBackPressed();
    } // get back image

    private void getDetailsData() {
        if (sharedPreferences.getString("language", "ar").equals("en")) {
            branch.setText(oneorder.getKitchen().getEn_title());
        } else if (sharedPreferences.getString("language", "ar").equals("ar")) {

            branch.setText(oneorder.getKitchen().getAr_title());
        }
        prepare_time = oneorder.getNeededtime();
        meals = oneorder.getMeals();
        setText();
        fillMealComponentList();

    } // set data to text

    private void setText() {
        party_name.setText(oneorder.getEn_partyname());
        tv_meal_menu.setText(oneorder.getComponent());
        et_prepare_time.setText(prepare_time);
        order_num.setText(orderNumber);
        party_date.setText(oneorder.getPartydate());
        party_time.setText(oneorder.getPartytime());
        client_number.setText(oneorder.getUser().getMobilenumber());
        //  delivery_time.setText(String.valueOf(oneorder.getTimetodeliver()) + "   " + getString(R.string.minuteO));
        space_to_client.setText(oneorder.getDistancetouser());
        party_location.setText(oneorder.getPartylocation());
        customer_notes.setText(oneorder.getUsernote());
        vendor_notes.setText(oneorder.getVendornote());
        total_price.setText(oneorder.getDeliverytotalprice());
        total_invoice_price.setText(oneorder.getTotalprice());

    } // set fields with data

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getOneInvoiceDetails() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
     /*   new Thread() {
            public void run() {*/
        //Retrofit

        Retrofit retrofit = RetrofiUrlConnection.connectWith();

        final SelectOneOrderDetails orderApi = retrofit.create(SelectOneOrderDetails.class);

        final Call<OneOrderResultModel> getInterestConnection = orderApi.getOneOrderDetailsApi(orderNumber);

        getInterestConnection.enqueue(new Callback<OneOrderResultModel>() {
            @Override
            public void onResponse(Call<OneOrderResultModel> call, Response<OneOrderResultModel> response) {
                try {

                    String code = response.body().getCode();
                    Log.i("QP", "invoice details code" + code);
                    Log.i("QP", "invoice details response" + response.body().getOneorder());

                    if (code.equals("200")) {
                        if (!(response.body().equals(null))) {
                            oneorder = response.body().getOneorder();
                            getDetailsData();
                        } else {
                            showDialog.initDialog(getString(R.string.retry), InvoiceDetailsBanquetsActivity.this);
                        }

                    } // get the details

                    progress.dismiss();

                } // try
                catch (Exception e) {
                    Log.i("QP", "exception : " + e.toString());
                    progress.dismiss();
                } // catch
            } // onResponse

            @Override
            public void onFailure(Call<OneOrderResultModel> call, Throwable t) {
                showDialog.initDialog(getString(R.string.retry), InvoiceDetailsBanquetsActivity.this);
                progress.dismiss();
            } // on Failure
        });
// Retrofit
           /* }

        }.start();*/
    } // function of get details


//    @OnClick(R.id.print)
//    public void printInvoice()
//    {
//        if (baseApp.isAidl()) {
//
//            Log.i("QP","size : if : "+datalist.size());
//            AidlUtil.getInstance().printTable(datalist);
//        } else {
//
//            BluetoothUtil.sendData(ESCUtil.printBitmap(BytesUtil.initTable(6, 12)));
//            Log.i("QP","size : else : "+datalist.size());
//
//        }
//    } // function of printInvoice

//    private void initPrinter()
//    {
//        AidlUtil.getInstance().connectPrinterService(this);
//        AidlUtil.getInstance().initPrinter();
//
//
//        tableItem1 = new TableItem();
//        tableItem2 = new TableItem();
//        tableItem3 = new TableItem();
//
//        datalist = new LinkedList<>();
//        baseApp = new BaseApp();
//
//        textPrintTitle = new String[] {"MamaMia"};
//        tableItem1.setText(textPrintTitle);
//        tableItem1.setWidth(new int[]{12});
//        tableItem1.setAlign(new int[]{0});
//        datalist.add(tableItem1);
//
//
//
//        textPrintInvoice = new String[]{"Invoice : ",orderNumber};
//        tableItem2.setText(textPrintInvoice);
//        tableItem2.setWidth(new int[]{6,6});
//        tableItem2.setAlign(new int[]{2,5});
//        datalist.add(tableItem2);
//
//        textPrint = new String[]{"mobile : +023123","delivery : 50 SR","total price : 3000 SR"};
//        tableItem3.setText(textPrint);
//        tableItem3.setWidth(new int[]{1,2,3});
//        tableItem3.setAlign(new int[]{1,7,9});
//        datalist.add(tableItem3);
//
//
//
//
//    } // function of initPrinter

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.onDestroy();

    }

}
