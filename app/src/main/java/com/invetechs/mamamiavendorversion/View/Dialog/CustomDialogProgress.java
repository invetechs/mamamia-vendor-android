package com.invetechs.mamamiavendorversion.View.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.R;

public class CustomDialogProgress extends Language {

    // vars
    Dialog progress;
    LinearLayout layout;
    ContentLoadingProgressBar contentLoadingProgressBar;
    TextView tv_please_wait;

    private static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth


    public void init(Context context) {
        progress = new Dialog(context);
        progress.setContentView(R.layout.progress_loading_dialog);
        progress.setCancelable(false);
        layout = progress.findViewById(R.id.layoutDialog);
        contentLoadingProgressBar = progress.findViewById(R.id.progressLoading);
        tv_please_wait = progress.findViewById(R.id.tv_please_wait);
        contentLoadingProgressBar.setVisibility(View.VISIBLE);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progress.getWindow().setLayout((int) (getScreenWidth((Activity) context) * .9), ViewGroup.LayoutParams.WRAP_CONTENT);

        sharedPreferences = context.getSharedPreferences("user", MODE_PRIVATE);

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoMedium.otf");
            tv_please_wait.setTypeface(font);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            tv_please_wait.setTypeface(font);

        }
    } // function of init

    public void show() {
        progress.show();
    } // function of show

    public void dismiss() {
        progress.dismiss();
    } // function of dismiss
}
