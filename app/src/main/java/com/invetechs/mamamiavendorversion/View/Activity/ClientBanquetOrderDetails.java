package com.invetechs.mamamiavendorversion.View.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Adapter.MealComponentAdapter;
import com.invetechs.mamamiavendorversion.Config.ToolbarConfig;
import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi.AcceptOrderApi;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi.RejectOrderApi;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi.SelectOneOrderDetails;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.OneOrderResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ClientBanquetOrderDetails extends Language {

    // Bind views

    @BindView(R.id.party_name)
    TextInputEditText order_name;

    @BindView(R.id.order_number)
    TextInputEditText order_number;

    @BindView(R.id.customer_name)
    TextInputEditText customer_name;

    @BindView(R.id.customer_number)
    TextInputEditText customer_number;

    @BindView(R.id.order_time)
    TextInputEditText order_time;

    @BindView(R.id.party_time)
    TextInputEditText party_time;

    @BindView(R.id.order_date)
    TextInputEditText order_date;

    @BindView(R.id.party_date)
    TextInputEditText party_date;

    @BindView(R.id.party_type)
    TextInputEditText party_type;

    @BindView(R.id.num_of_person)
    TextInputEditText num_of_person;

    @BindView(R.id.party_place)
    TextInputEditText party_place;

    @BindView(R.id.customer_notes)
    TextInputEditText customer_notes;

    @BindView(R.id.et_prepare_time)
    TextInputEditText et_prepare_time;

    @BindView(R.id.meal_menu)
    TextInputEditText meal_menu;

    @BindView(R.id.seller_notes)
    TextInputEditText seller_notes;

    @BindView(R.id.order_status)
    TextInputEditText order_status;

    @BindView(R.id.branch)
    TextInputEditText branch;

    @BindView(R.id.distanceToCustomer)
    TextInputEditText distanceToCustomer;

    @BindView(R.id.total_dilivery_cost)
    TextInputEditText et_total_dilivery_cost;

    @BindView(R.id.total_order)
    TextInputEditText et_total_order;

    @BindView(R.id.btn_accept)
    Button btn_accept;

    @BindView(R.id.btn_reject)
    Button btn_reject;

    //vars
    TextView message;
    List<OneOrderResultModel.OneorderBean.MealsBean> mealsList = new ArrayList<>(), meals = new ArrayList<>();
    MealComponentAdapter adapter;
    String orderNumber;
    String vendorNotes = "";
    CustomDialogProgress progress;
    Handler handler;
    OneOrderResultModel.OneorderBean oneorder;
    String preparation_time = "", archive = "0", mealName, prepareTime, totalOrder, totalDiliverySalary;
    int status = 2;
    LinearLayout layout;
    SharedPreferences.Editor editor;
    Dialog acceptAndRejectDialog;
    ShowDialog showDialog = new ShowDialog();
    ToolbarConfig toolBarConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_banquet_order_details);
        ButterKnife.bind(this);
        setToolBarConfig();
        checkLanguage();
        getOrderNumber();
        getClientBanquetsDetails();
        initAcceptAndRejectDialog();
    } // function of onCreate


    private void setToolBarConfig() {
        toolBarConfig = new ToolbarConfig(this);
        toolBarConfig.setTitle(getString(R.string.banquet_details));
        toolBarConfig.setBack(this);
        toolBarConfig.addNotification();

    } // function of setToolBarConfig

    private void initAcceptAndRejectDialog() {
        acceptAndRejectDialog = new Dialog(this);
        acceptAndRejectDialog.setContentView(R.layout.accept_refuse_dialog);
        layout = acceptAndRejectDialog.findViewById(R.id.layoutDialog);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        acceptAndRejectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        acceptAndRejectDialog.getWindow();
        message = acceptAndRejectDialog.findViewById(R.id.tv_message);

    } // show dialog

    @OnClick(R.id.img_back)
    public void backImage() {
        onBackPressed();
    } // get back image

    private void fillMealComponentList() {

        for (int i = 0; i < mealsList.size(); i++) {
            meals.add(mealsList.get(i));
            adapter.notifyDataSetChanged();
        } // for loop

    } // fill component meal from api

    private void getOrderNumber() {
        Intent intent = getIntent();
        if (intent != null) {
            archive = intent.getStringExtra("archive");
            orderNumber = getIntent().getStringExtra("client_banquets_id");
        }

        if (archive.equals("1")) {
            btn_accept.setVisibility(View.GONE);
            btn_reject.setVisibility(View.GONE);
            seller_notes.setEnabled(false);
            et_prepare_time.setEnabled(false);
            et_total_order.setEnabled(false);
            et_total_dilivery_cost.setEnabled(false);
        }

        Log.i("QP", "recieve client_banquets_id : " + orderNumber);

    } // get order number to fetch client banquet requests details

    private void checkLanguage() {
        sharedPreferences = this.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }
    } // function of checkLanguage

    private void getDetailsData() {

        if (sharedPreferences.getString("language", "ar").equals("en")) {

            order_name.setText(oneorder.getEn_partyname());
            branch.setText(oneorder.getKitchen().getEn_title());
        } else if (sharedPreferences.getString("language", "ar").equals("ar")) {
            order_name.setText(oneorder.getEn_partyname());
            branch.setText(oneorder.getKitchen().getAr_title());
        }

        if (oneorder.getStatus().equals("0") || oneorder.getStatus().equals("2")) {
            order_status.setText(getString(R.string.newStatus));
        } else if (oneorder.getStatus().equals("1")) {
            order_status.setText(getString(R.string.accepted));
        } else if (oneorder.getStatus().equals("-1")) {
            order_status.setText(getString(R.string.rejected));
        }

        mealName = oneorder.getComponent();
        meal_menu.setText(mealName);

        // needTime = oneorder.getNeededtime();
        preparation_time = oneorder.getNeededtime();
        et_prepare_time.setText(preparation_time);
        // convert need time to day , hours , minute
        //  int d = 0, h = 0, m = 0;
        //  d = (Integer.parseInt(needTime)) / (24 * 60);
        //  h = ((Integer.parseInt(needTime)) - (d * (24 * 60))) / 60;
        //  m = ((Integer.parseInt(needTime)) - (d * (24 * 60))) - (h * 60);
        //  dayValue = String.valueOf(d);
        //   hourValue = String.valueOf(h);
        //  miniutesValue = String.valueOf(m);
        order_number.setText(orderNumber);
        order_date.setText(oneorder.getOrderdate());
        customer_name.setText(oneorder.getUser().getName());
        customer_number.setText(oneorder.getUser().getMobilenumber());
        order_time.setText(oneorder.getOrdertime());
        party_time.setText(String.valueOf(oneorder.getPartytime()));
        party_date.setText(String.valueOf(oneorder.getPartydate()));
        party_type.setText(String.valueOf(oneorder.getPartytype()));
        num_of_person.setText(String.valueOf(oneorder.getGuestnum()));
        party_place.setText(String.valueOf(oneorder.getPartylocation()));
        customer_notes.setText(oneorder.getUsernote());
        seller_notes.setText(oneorder.getVendornote());
        distanceToCustomer.setText(oneorder.getDistancetouser());

        if (!oneorder.getDeliverytotalprice().equals("0.00"))
            et_total_dilivery_cost.setText(oneorder.getDeliverytotalprice());
        if (!oneorder.getTotalprice().equals("0.00"))
            et_total_order.setText(oneorder.getTotalprice());
        mealsList = oneorder.getMeals();

        Log.i("Qe", "aa : " + mealsList);
        fillMealComponentList();
    } // set data to text

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    @SuppressLint("HandlerLeak")
    private void getClientBanquetsDetails() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }
        };
        progress.show();
       /*   new Thread() {
            public void run() {*/
        //Retrofit
        Retrofit retrofit = RetrofiUrlConnection.connectWith();
        final SelectOneOrderDetails orderApi = retrofit.create(SelectOneOrderDetails.class);
        final Call<OneOrderResultModel> getInterestConnection = orderApi.getOneOrderDetailsApi(orderNumber);
        getInterestConnection.enqueue(new Callback<OneOrderResultModel>() {
            @Override
            public void onResponse(Call<OneOrderResultModel> call, Response<OneOrderResultModel> response) {
                try {
                    String code = response.body().getCode();
                    Log.i("QP", "invoice details code" + code);
                    Log.i("QP", "invoice details response" + response.body().getOneorder());
                    if (code.equals("200")) {
                        if (response.body() != null) {
                            oneorder = response.body().getOneorder();
                            getDetailsData();
                        } else {
                            showDialog.initDialog(getString(R.string.retry), ClientBanquetOrderDetails.this);
                        }
                    } // get the details
                    progress.dismiss();
                } // try
                catch (Exception e) {
                    Log.i("QP", "exception : " + e.toString());
                    progress.dismiss();
                } // catch
            } // onResponse

            @Override
            public void onFailure(Call<OneOrderResultModel> call, Throwable t) {
                showDialog.initDialog(getString(R.string.retry), ClientBanquetOrderDetails.this);
                Log.i("QP", "error : " + t.toString());
                progress.dismiss();
            } // on Failure
        });
// Retrofit
           /* }
        }.start();*/
    } // function of get details

    public void acceptOrder(View view) {

        if (!fieldsValidation()) {
            return;
        }
        acceptNewOrder();
    }// button click accept Order

    private boolean fieldsValidation() {

        totalDiliverySalary = et_total_dilivery_cost.getText().toString().trim();
        totalOrder = et_total_order.getText().toString().trim();
        vendorNotes = seller_notes.getText().toString().trim();
        prepareTime = et_prepare_time.getText().toString().trim();

        if (prepareTime.isEmpty()) {
            et_prepare_time.setError(getString(R.string.field_cant_empty));
            return false;
        }
        if (totalOrder.isEmpty()) {
            et_total_order.setError(getString(R.string.field_cant_empty));
            return false;
        }
        if (totalDiliverySalary.isEmpty()) {
            et_total_dilivery_cost.setError(getString(R.string.field_cant_empty));
            return false;
        }
        return true;
    } // validation on fields

    private void acceptNewOrder() {
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
     /*   new Thread() {
            public void run() {*/
//Retrofit
        RetrofiUrlConnection connection = new RetrofiUrlConnection(ClientBanquetOrderDetails.this);
        Retrofit retrofit = RetrofiUrlConnection.connectWith();

        final AcceptOrderApi orderApi = retrofit.create(AcceptOrderApi.class);


        final Call<ResultModelUser> getInterestConnection = orderApi.send_order(orderNumber, vendorNotes, prepareTime, totalOrder, status, totalDiliverySalary);

        getInterestConnection.enqueue(new Callback<ResultModelUser>() {
            @Override
            public void onResponse(Call<ResultModelUser> call, Response<ResultModelUser> response) {
                try {

                    String code = response.body().getCode();
                    Log.i("QP", "code" + code);

                    if (code.equals("200")) {

                        if (response.body() != null) {
                            acceptAndRejectDialog.show();
                            message.setText(getString(R.string.orderAccepted));
                            Intent intent = new Intent(ClientBanquetOrderDetails.this, HomePageScreen.class);
                            intent.putExtra("fragment", "banquet");
                            startActivity(intent);
                            finish();
                        } else {
                            showDialog.initDialog(getString(R.string.retry), ClientBanquetOrderDetails.this);
                        }

                    } //  success

                    progress.dismiss();

                } // try
                catch (Exception e) {
                    Log.i("QP", "exception : " + e.toString());
                    progress.dismiss();
                } // catch
            } // onResponse

            @Override
            public void onFailure(Call<ResultModelUser> call, Throwable t) {
                showDialog.initDialog(getString(R.string.retry), ClientBanquetOrderDetails.this);
                Log.i("QP", "error : " + t.toString());
                progress.dismiss();
            } // on Failure
        });
// Retrofit
         /*   }
        }.start();*/
    }  // button click on accept order

    public void rejectOrder(View view) {
        vendorNotes = seller_notes.getText().toString().trim();

        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
      /*  new Thread() {
            public void run() {*/
//Retrofit

        RetrofiUrlConnection connection = new RetrofiUrlConnection(ClientBanquetOrderDetails.this);
        Retrofit retrofit = RetrofiUrlConnection.connectWith();

        final RejectOrderApi orderApi = retrofit.create(RejectOrderApi.class);

        final Call<ResultModelUser> getInterestConnection = orderApi.rejectOrderApiWithVendorNotes(orderNumber, vendorNotes);

        getInterestConnection.enqueue(new Callback<ResultModelUser>() {
            @Override
            public void onResponse(Call<ResultModelUser> call, Response<ResultModelUser> response) {
                try {

                    String code = response.body().getCode();
                    Log.i("QP", "code" + code);

                    if (code.equals("200")) {
                        if (response.body() != null) {
                            acceptAndRejectDialog.show();
                            message.setText(getString(R.string.orderRejected));
                            Intent intent = new Intent(ClientBanquetOrderDetails.this, HomePageScreen.class);
                            intent.putExtra("fragment", "banquet");
                            startActivity(intent);
                            finish();
                        } else {
                            showDialog.initDialog(getString(R.string.retry), ClientBanquetOrderDetails.this);
                        }

                    } //  success

                    progress.dismiss();

                } // try
                catch (Exception e) {
                    Log.i("QP", "exception : " + e.toString());
                    progress.dismiss();
                } // catch
            } // onResponse

            @Override
            public void onFailure(Call<ResultModelUser> call, Throwable t) {
                showDialog.initDialog(getString(R.string.retry), ClientBanquetOrderDetails.this);
                Log.i("QP", "error : " + t.toString());
                progress.dismiss();
            } // on Failure
        });
// Retrofit

           /* }

        }.start();*/
    } // button click reject Order

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.onDestroy();

    }
} // class of ClientBanquetOrderDetails
