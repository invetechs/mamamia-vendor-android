package com.invetechs.mamamiavendorversion.View.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Adapter.PointAdapter;
import com.invetechs.mamamiavendorversion.Adapter.RulesAdapter;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.BranchApi.SelectAllKitchen;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.PointApi.SelectPointApi;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.RuleApi.SelectAllRulesApi;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.BranchRetrofit.SelectAllKitchenResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.PointsResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.RulesResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class PointsAndRulesFragment extends Fragment {

    //bindview

    @BindView(R.id.recycler_view_points)
    RecyclerView recyclerViewPoints;

    @BindView(R.id.pointsLayout)
    LinearLayout pointsLayout;

    @BindView(R.id.bt_rules)
    Button bt_rules;

    @BindView(R.id.bt_points)
    Button bt_points;

    @BindView(R.id.rules_view_rules)
    RecyclerView rulesviewRules;

    String click = "rule";

    @BindView(R.id.sp_vendor_restaurants)
    Spinner spinner_restaurants;

    //vars
    SharedPreferences.Editor editor;
    public final String MY_PREFS_NAME = "MyPrefsFile";
    String id;
    Handler handler;
    CustomDialogProgress progress;
    SharedPreferences sharedPreferences;
    int lastBranchPositionChosen = 0;
    List<SelectAllKitchenResultModel.KitchensdataBean> branchArrayList = new ArrayList<>();
    List<SelectAllKitchenResultModel.KitchensdataBean> branchesNameList = new ArrayList<>();
    String code = "";
    List<RulesResultModel.AllrulesBean> ruleArrayList = new ArrayList<>();
    List<RulesResultModel.AllrulesBean> ruleArrayListAP = new ArrayList<>();
    List<PointsResultModel.AlluserspointBean> pointArrayList = new ArrayList<>();
    List<PointsResultModel.AlluserspointBean> pointsList = new ArrayList<>();
    PointAdapter pointAdapter;
    RulesAdapter ruleAdapter;
    SharedPreferences prefs;
    ShowDialog showDialog = new ShowDialog();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_points_and_rules, container, false);
        ButterKnife.bind(this, view);
        getUserId(); // get user id from shared preference
        checkLanguage();
        initRulesRecyclerView(); // init rules list
        initPointsRecyclerView();
        deFaultColorToButton();// set color to rule and points button
        getAllKitchenData();
        getBranchChosenFromList();
        return view;
    } // onCreateView

    private void initRulesRecyclerView() {

        ruleAdapter = new RulesAdapter(getContext(), ruleArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rulesviewRules.setHasFixedSize(true);
        rulesviewRules.setLayoutManager(linearLayoutManager);
        rulesviewRules.setAdapter(ruleAdapter);

    } //initialize rules recycler view

    private void getUserId() {
        prefs = getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        id = prefs.getString("id", "0");
    } // getUserId function

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth


    private void getVendorRules(final int kitchenId) {

        ruleArrayListAP.clear();
        ruleArrayList.clear();
        ruleAdapter.notifyDataSetChanged();
        progress = new CustomDialogProgress();
        progress.init(getActivity());
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
      /*  new Thread() {
            public void run() {*/
//Retrofit

        Retrofit retrofit = RetrofiUrlConnection.connectWith();

        final SelectAllRulesApi ruleApi = retrofit.create(SelectAllRulesApi.class);

        final Call<RulesResultModel> getInterestConnection = ruleApi.getAllRuless(id, kitchenId);

        getInterestConnection.enqueue(new Callback<RulesResultModel>() {
            @Override
            public void onResponse(Call<RulesResultModel> call, Response<RulesResultModel> response) {
                try {

                    String code = response.body().getCode();

                    response.body();
                    Log.i("QP", "code" + code + " id " + id);

                    if (code.equals("200")) {
                        if (response.body() != null) {
                            ruleArrayListAP = response.body().getAllrules();
                            fillListWithRules();
                        } else {
                            showDialog.initDialog(getString(R.string.retry), getActivity());
                        }

                    } // login success
                    else if (code.equals("1313")) {

                    } // no Orders

                    progress.dismiss();

                } // try
                catch (Exception e) {
                    Log.i("QP", "exception : " + e.toString());
                    progress.dismiss();
                } // catch

            } // onResponse

            @Override
            public void onFailure(Call<RulesResultModel> call, Throwable t) {
                showDialog.initDialog(getString(R.string.retry), getActivity());
                Log.i("QP", "error : " + t.toString());
                progress.dismiss();
            } // on Failure
        });
// Retrofit
      /*      }

        }.start();*/
    } // function of getVendorRules

    private void getVendorPoints(final int kitchenId) {

        pointArrayList.clear();
        pointsList.clear();
        pointAdapter.notifyDataSetChanged();
        progress = new CustomDialogProgress();
        progress.init(getActivity());
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
     /*   new Thread() {
            public void run() {*/
//Retrofit

        Retrofit retrofit = RetrofiUrlConnection.connectWith();

        final SelectPointApi pointApi = retrofit.create(SelectPointApi.class);

        final Call<PointsResultModel> getInterestConnection = pointApi.getAllPoints(id, kitchenId);

        Log.i("QP", "vendor id : " + id);
        Log.i("QP", "kitchen id : " + kitchenId);
        getInterestConnection.enqueue(new Callback<PointsResultModel>() {
            @Override
            public void onResponse(Call<PointsResultModel> call, Response<PointsResultModel> response) {
                try {

                    String code = response.body().getCode();

                    Log.i("QP", "code" + code + " id " + id);

                    if (response.body() != null) {
                        if (code.equals("200")) {
                            pointArrayList = response.body().getAlluserspoint();

                            Log.i("QP", "size : " + pointArrayList.size());

                            if (pointArrayList.size() <= 0) {
                                pointsLayout.setVisibility(View.GONE);

                            }
                            else {
                                pointsLayout.setVisibility(View.VISIBLE);

                                getPoints();


                            }
                        } // request success

                        else if (code.equals("1313")) {
                            pointsLayout.setVisibility(View.GONE);
                        } // no Orders
                    } else {
                        showDialog.initDialog(getString(R.string.retry), getActivity());

                    }


                    progress.dismiss();

                } // try
                catch (Exception e) {
                    Log.i("QP", "exception : " + e.toString());
                    progress.dismiss();
                } // catch
            } // onResponse

            @Override
            public void onFailure(Call<PointsResultModel> call, Throwable t) {
                showDialog.initDialog(getString(R.string.retry), getActivity());
                Log.i("QP", "error : " + t.toString());
                progress.dismiss();
            } // on Failure
        });
// Retrofit
/*
            }

        }.start();*/
    }

    private void fillListWithRules() {
        Log.i("QP", "size arraFromApi : " + ruleArrayListAP.size());
        for (int i = 0; i < ruleArrayListAP.size(); i++) {
            ruleArrayList.add(ruleArrayListAP.get(i));
        }
        Log.i("QP", "size newArray : " + ruleArrayList.size());
        ruleAdapter.notifyDataSetChanged();
    }

    private void checkLanguage() {
        sharedPreferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(getActivity().getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            bt_rules.setTypeface(font);
            bt_points.setTypeface(font);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(getActivity().getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            bt_rules.setTypeface(font);
            bt_points.setTypeface(font);

        }
    } // checkLanguage

    private void initPointsRecyclerView() {

        pointAdapter = new PointAdapter(getContext(), pointsList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewPoints.setHasFixedSize(true);
        recyclerViewPoints.setLayoutManager(linearLayoutManager);
        recyclerViewPoints.setAdapter(pointAdapter);
    } // intialize list of points

    private void deFaultColorToButton() {
        bt_rules.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        bt_points.setBackgroundColor(getResources().getColor(R.color.gray));
    } // set color to rule and points button

    @OnClick(R.id.bt_rules)
    public void buttonRulesClick(View view) {
        click = "rule";
        pointsLayout.setVisibility(View.GONE);
        rulesviewRules.setVisibility(View.VISIBLE);
        bt_rules.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        bt_points.setBackgroundColor(getResources().getColor(R.color.gray));
        if (branchesNameList.size() > 0) {
            getVendorRules(branchesNameList.get(lastBranchPositionChosen).getId());
        }
        Log.i("QP", "last position chosen : rule / " + lastBranchPositionChosen);

    } // function of button Rules click

    @OnClick(R.id.bt_points)
    public void buttonPointstClick() {
        click = "point";
        pointsLayout.setVisibility(View.VISIBLE);
        rulesviewRules.setVisibility(View.GONE);
        bt_rules.setBackgroundColor(getResources().getColor(R.color.gray));
        bt_points.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

        if (branchesNameList.size() > 0) {
            getVendorPoints(branchesNameList.get(lastBranchPositionChosen).getId());
        }
      //  Log.i("QP", "last position chosen : point : " + branchesNameList.get(lastBranchPositionChosen).getId());

    }// function of button Points click

    private void getPoints() {

        Log.i("QP", "point array size : " + pointArrayList.size());
        for (int i = 0; i < pointArrayList.size(); i++) {

            pointsList.add(pointArrayList.get(i));
        }
        pointAdapter.notifyDataSetChanged();
    } // function of getPoints


    public void getAllKitchenData() {
        progress = new CustomDialogProgress();
        progress.init(getActivity());
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
//Retrofit


                RetrofiUrlConnection connection = new RetrofiUrlConnection(getActivity());
                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                SelectAllKitchen selectAllKitchen = retrofit.create(SelectAllKitchen.class);

                final Call<SelectAllKitchenResultModel> getInterestConnection = selectAllKitchen.getAllKitchen(id);

                getInterestConnection.enqueue(new Callback<SelectAllKitchenResultModel>() {
                    @Override
                    public void onResponse(Call<SelectAllKitchenResultModel> call, Response<SelectAllKitchenResultModel> response) {
                        try {


                            String code = response.body().getCode();
                            if (code.equals("200")) {
                                if (response.body() != null) {
                                    branchArrayList = response.body().getKitchensdata();
                                    fillSpinnerWithRestaurants();
                                } else {
                                    showDialog.initDialog(getString(R.string.retry), getActivity());

                                }

                            } else if (code.equals("1313")) {
                                showDialog.initDialog(getString(R.string.no_kitchen), getActivity());

                            }
                            Log.i("QP", "code : " + code + " : userId : " + id);
                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<SelectAllKitchenResultModel> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
                // Retrofit
            }

        }.start();
    } // function of getAllKitchen

    private void fillSpinnerWithRestaurants() {
        for (int i = 0; i < branchArrayList.size(); i++) {
            sharedPreferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
            editor = sharedPreferences.edit();

            if (sharedPreferences.getString("language", "ar").equals("ar")) {
                branchesNameList.add(new SelectAllKitchenResultModel.KitchensdataBean(branchArrayList.get(i).getId(),
                        branchArrayList.get(i).getAr_title()));

            } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                branchesNameList.add(new SelectAllKitchenResultModel.KitchensdataBean(branchArrayList.get(i).getId(),
                        branchArrayList.get(i).getEn_title()));

            }
        }

        ArrayAdapter<SelectAllKitchenResultModel.KitchensdataBean> adapter = new ArrayAdapter<SelectAllKitchenResultModel.KitchensdataBean>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, branchesNameList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (sharedPreferences.getString("language", "ar").equals("ar")) {
                    Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
                    ((TextView) v).setTypeface(externalFont);

                } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                    Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
                    ((TextView) v).setTypeface(externalFont);

                }

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                if (sharedPreferences.getString("language", "ar").equals("ar")) {
                    Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
                    ((TextView) v).setTypeface(externalFont);

                } else if (sharedPreferences.getString("language", "ar").equals("en")) {
                    Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
                    ((TextView) v).setTypeface(externalFont);

                }

                return v;
            }

        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_restaurants.setAdapter(adapter);

    } // function fillSpinnerWithRestaurants

    private void getBranchChosenFromList() {
        spinner_restaurants.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

               lastBranchPositionChosen = position;

                Log.i("QP", "last bracch choosen : " + branchesNameList.get(position).getAr_title() +
                        "     : " + branchesNameList.get(position).getId());

                if (click.equals("rule")) {

                    getVendorRules(branchesNameList.get(position).getId());
                    Log.i("QP", "branchesName : rule" + branchesNameList.get(position).getId());
                }
                else if (click.equals("point")) {
                    getVendorPoints(branchesNameList.get(position).getId());
                    Log.i("QP", "position need " + branchesNameList.get(lastBranchPositionChosen).getId());
                }
            } // onItem Selected from spinner

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            } // last selected from spinner

        });
    }

} // class of PointsAndRulesFragment
