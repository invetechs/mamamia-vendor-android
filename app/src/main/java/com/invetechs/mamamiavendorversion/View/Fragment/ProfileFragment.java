package com.invetechs.mamamiavendorversion.View.Fragment;

import android.app.Activity;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.invetechs.mamamiavendorversion.Adapter.OrderAdapter;
import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.LoginApi.LoginApi;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.ProfileApi.ProfileApi;
import com.invetechs.mamamiavendorversion.Model.Class.Model.User;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Activity.HomePageScreen;
import com.invetechs.mamamiavendorversion.View.Activity.LoginScreen;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {

    // bind view
    @BindView(R.id.img_profile)
    ImageView imgProfile;

    @BindView(R.id.tv_name)
    TextInputEditText tvName;

    @BindView(R.id.tv_mobile)
    TextInputEditText tvMobile;

    @BindView(R.id.tv_email)
    TextInputEditText tvEmail;

    @BindView(R.id.tv_name_title)
    TextView tv_name_title;

    @BindView(R.id.tv_mobile_title)
    TextView tv_mobile_title;

    @BindView(R.id.tv_email_title)
    TextView tv_email_title;

    // vars
    SharedPreferences sharedPreferences;
    Handler handler;
    CustomDialogProgress progress;
    String id = "";
    public  final String MY_PREFS_NAME = "MyPrefsFile";
    ResultModelUser.UserdataBean user;
    SharedPreferences.Editor editor;
    ShowDialog showDialog = new ShowDialog();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        initLanguage();
        getUserID();
        getDataFromApi();
        return view;
    } // onCreateView


    private void initLanguage() {

        sharedPreferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/GESSTwoMedium.otf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            tv_name_title.setTypeface(font);
            tv_mobile_title.setTypeface(font);
            tv_email_title.setTypeface(font);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Calligrapher calligrapher = new Calligrapher(getContext());
            calligrapher.setFont(getActivity(), "fonts/OpenSans-Regular.ttf", true);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            tv_name_title.setTypeface(font);
            tv_mobile_title.setTypeface(font);
            tv_email_title.setTypeface(font);

        }


    }//initialize language

    private void getUserID() {
        SharedPreferences prefs = getContext().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        id = prefs.getString("id", "");
    } // function get the id of user to send it to api

    private void setViewData() {

        Glide.with(getContext())
                .load("https://mammamiaa.com/cpanel/upload/user/" + user.getImage())
                .placeholder(R.drawable.ic_person)
                .into(imgProfile);

        tvName.setText(user.getName());
        tvMobile.setText(user.getMobilenumber());
        tvEmail.setText(user.getEmail());

    } // set view data from api


    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth


    private void getDataFromApi() {
        progress = new CustomDialogProgress();
        progress.init(getActivity());
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
     /*   new Thread() {
            public void run() {*/
//Retrofit

                final RetrofiUrlConnection connection = new RetrofiUrlConnection(getActivity());
                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final ProfileApi profileApi = retrofit.create(ProfileApi.class);

                final Call<ResultModelUser> getInterestConnection = profileApi.show_profile(id);

                getInterestConnection.enqueue(new Callback<ResultModelUser>() {
                    @Override
                    public void onResponse(Call<ResultModelUser> call, Response<ResultModelUser> response) {
                        try {


                            String code = response.body().getCode();

                            response.body();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {

                                if (response.body() != null) {
                                    user = response.body().getUserdata();
                                    setViewData();
                                } else {
                                    showDialog.initDialog(getString(R.string.retry), getActivity());

                                }

                            } // get user data successfully

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<ResultModelUser> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), getActivity());
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

      /*      }

        }.start();*/

    } // show profile data

} // class of ProfileFragment
