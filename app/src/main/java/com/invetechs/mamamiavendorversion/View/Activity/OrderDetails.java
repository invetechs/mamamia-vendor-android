package com.invetechs.mamamiavendorversion.View.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.invetechs.mamamiavendorversion.Adapter.MealComponentAdapter;
import com.invetechs.mamamiavendorversion.Config.ToolbarConfig;
import com.invetechs.mamamiavendorversion.Language.Language;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi.AcceptOrderApi;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi.RejectOrderApi;
import com.invetechs.mamamiavendorversion.Model.Class.API.LoginApi.OrderApi.SelectOneOrderDetails;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.OneOrderResultModel;
import com.invetechs.mamamiavendorversion.Model.Class.ResultModel.ResultModelUser;
import com.invetechs.mamamiavendorversion.Model.Class.Retrofit.RetrofiUrlConnection;
import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Dialog.CustomDialogProgress;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OrderDetails extends Language {

    // bind views
    @BindView(R.id.ed_orderName)
    TextView ed_orderName;

    @BindView(R.id.recycler_view_amout_meal)
    RecyclerView recyclerView;


    @BindView(R.id.ed_orderNumber)
    TextInputEditText ed_orderNumber;

    @BindView(R.id.ed_customerName)
    TextInputEditText ed_customerName;

    @BindView(R.id.ed_customerMobileNumber)
    TextInputEditText ed_customerMobileNumber;

    @BindView(R.id.ed_orderTime)
    TextInputEditText ed_orderTime;

    @BindView(R.id.ed_orderDate)
    TextInputEditText ed_orderDate;

    @BindView(R.id.ed_customerLocation)
    TextInputEditText ed_customerLocation;

    @BindView(R.id.ed_customerNotes)
    TextInputEditText ed_customerNotes;

    @BindView(R.id.ed_branch)
    TextInputEditText ed_branch;

    @BindView(R.id.ed_distanceToCustomer)
    TextInputEditText ed_distanceToCustomer;

    @BindView(R.id.order_status)
    TextView order_status;

    @BindView(R.id.ed_totalCostDelivery)
    TextInputEditText ed_totalCostDelivery;

    @BindView(R.id.ed_priceVat)
    TextInputEditText ed_priceVat;

    @BindView(R.id.ed_orderPrice)
    TextInputEditText ed_orderPrice;

    @BindView(R.id.ed_totalOrder)
    TextInputEditText ed_totalOrder;

    @BindView(R.id.ed_sellerNotes)
    TextInputEditText ed_sellerNotes;

    @BindView(R.id.ed_day)
    TextView ed_day;

    @BindView(R.id.ed_hour)
    TextView ed_hour;

    @BindView(R.id.ed_minutes)
    TextView ed_minutes;

    @BindView(R.id.bt_accept)
    TextView bt_accept;

    @BindView(R.id.bt_reject)
    TextView bt_reject;

    @BindView(R.id.tv_discount)
    TextView tv_discount;

    @BindView(R.id.linear_discount)
    LinearLayout linear_discount;

    // vars
    String  orderName = "", orderNumber = "", customerName = "", customerMobile = "", orderTime = "",
            orderDate = "", customerLocation = "", branch = "", distanceToCustomer = "",
            customerNotes = "", needTime = "", day = "", hour = "", minute = "", sellerNotes = "",
            discount = "" , totalDeliveryPrice = "" , price_vat = "" , totalOrder = "" , order_price = "";
    SharedPreferences.Editor editor;
    Handler handler;
    CustomDialogProgress progress;
    Dialog acceptAndRejectDialog;
    TextView message;
    LinearLayout layout;
    MealComponentAdapter adapter;
    List<OneOrderResultModel.OneorderBean.MealsBean> mealsList = new ArrayList<>(), meals = new ArrayList<>();
    String orderId = "", archive = "0";
    private OneOrderResultModel.OneorderBean oneorder;
    ShowDialog showDialog = new ShowDialog();
    ToolbarConfig toolBarConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        setToolBarConfig();
        checkLanguage();
        initAcceptAndRejectDialog();
        getIdFromOrderList(); // get Id From OrderList
        getOneOrderDetails(); //getOrderDetails with id
        initMealComponentList(); // intialize recycler meal component

    } // function of onCreate

    private void setToolBarConfig() {
        toolBarConfig = new ToolbarConfig(this);
        toolBarConfig.setTitle(getString(R.string.order_details));
        toolBarConfig.setBack(this);
        toolBarConfig.addNotification();

        Log.i("QW", "menu activity notification");

    } // function of setToolBarConfig

    private void initAcceptAndRejectDialog() {
        acceptAndRejectDialog = new Dialog(this);
        acceptAndRejectDialog.setContentView(R.layout.accept_refuse_dialog);
        layout = acceptAndRejectDialog.findViewById(R.id.layoutDialog);
        layout.setBackgroundResource(R.color.colorPrimaryDark);
        acceptAndRejectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        acceptAndRejectDialog.getWindow();
        message = acceptAndRejectDialog.findViewById(R.id.tv_message);

    }

    @OnClick(R.id.img_back)
    public void backImage() {
        onBackPressed();
    } // get back image

    private void checkLanguage() {
        sharedPreferences = this.getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/GESSTwoMedium.otf", true);

        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            ViewCompat.setLayoutDirection(this.getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_LTR);
            Calligrapher calligrapher = new Calligrapher(this);
            calligrapher.setFont(this, "fonts/OpenSans-Regular.ttf", true);
        }
    } // function of checkLanguage

    private void initMealComponentList() {
        adapter = new MealComponentAdapter(this, mealsList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }// intialize recycler meal component

    private void fillMealComponentList() {

        for (int i = 0; i < meals.size(); i++) {
            mealsList.add(meals.get(i));
            adapter.notifyDataSetChanged();
        } // for loop

    } // fill component meal from api

    private void getIdFromOrderList() {
        Intent intent = getIntent();
        if (intent != null) {

            orderId = intent.getStringExtra("orderId");
            archive = intent.getStringExtra("archive");
        }

        if (archive.equals("1")) {
            ed_sellerNotes.setEnabled(false);
            bt_accept.setVisibility(View.GONE);
            bt_reject.setVisibility(View.GONE);
        }
    } // function of getIdFromOrderList

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    } // function getScreenWidth

    private void getOneOrderDetails() {
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
      /*  new Thread() {
            public void run() {*/
//Retrofit

                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final SelectOneOrderDetails orderApi = retrofit.create(SelectOneOrderDetails.class);

                final Call<OneOrderResultModel> getInterestConnection = orderApi.getOneOrderDetailsApi(orderId);

                getInterestConnection.enqueue(new Callback<OneOrderResultModel>() {
                    @Override
                    public void onResponse(Call<OneOrderResultModel> call, Response<OneOrderResultModel> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                if (response.body() != null) {
                                    oneorder = response.body().getOneorder();

                                    getDetailsData();
                                } else {
                                    showDialog.initDialog(getString(R.string.retry), OrderDetails.this);
                                }

                            } // found order
                            else if (code.equals("1313")) {

                            } // not found

                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<OneOrderResultModel> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), OrderDetails.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

           /* }
        }.start();*/
    } // function of getOneOrderDetails

    private void getDetailsData() {
        if (sharedPreferences.getString("language", "ar").equals("en")) {
            if (!(oneorder.getMeals().size() == 0)) {
                orderName = oneorder.getMeals().get(0).getEn_title();
            }
            branch = oneorder.getKitchen().getEn_title();
        } else if (sharedPreferences.getString("language", "ar").equals("ar")) {
            if (!(oneorder.getMeals().size() == 0)) {
                orderName = oneorder.getMeals().get(0).getAr_title();

            }
            branch = oneorder.getKitchen().getAr_title();
        }

        if (oneorder.getStatus().equals("0") || oneorder.getStatus().equals("2")) {
            order_status.setText(getString(R.string.newStatus));
        } else if (oneorder.getStatus().equals("1")) {
            order_status.setText(getString(R.string.accepted));
        } else if (oneorder.getStatus().equals("-1")) {
            order_status.setText(getString(R.string.rejected));
        }

        orderNumber = oneorder.getId();
        orderDate = oneorder.getOrderdate();
        orderTime = oneorder.getOrdertime();
        customerLocation = oneorder.getUserplace();
        customerNotes = oneorder.getUsernote();

        needTime = oneorder.getNeededtime();
        Log.e("QP","neededTime : "+needTime);
        // convert need time to day , hours , minute
        int d = 0, h = 0, m = 0;
        if(!needTime.equals("0") || !needTime.equals(null)) {
            d = (Integer.parseInt(needTime)) / (24 * 60);
            h = ((Integer.parseInt(needTime)) - (d * (24 * 60))) / 60;
            m = ((Integer.parseInt(needTime)) - (d * (24 * 60))) - (h * 60);
        }
        day = String.valueOf(d);
        hour = String.valueOf(h);
        minute = String.valueOf(m);
        meals = oneorder.getMeals();
        distanceToCustomer = oneorder.getDistancetouser();
        totalDeliveryPrice = oneorder.getDeliverytotalprice();
        totalOrder = oneorder.getTotalprice();
        sellerNotes = oneorder.getVendornote();
        customerName = oneorder.getUser().getName();
        customerMobile = oneorder.getUser().getMobilenumber();


        price_vat = oneorder.getPriceVat();
        double priceVat = Double.parseDouble(price_vat);
        Log.i("QP", "priceVat : " + priceVat);

        double totalDilevery = Double.parseDouble(totalDeliveryPrice);
        Log.i("QP", "totalDilevery : " + totalDilevery);

        order_price = oneorder.getMealstotalprice();

        Log.i("QP", "order price price : " + order_price);


        if (oneorder.getDiscountpoint().equals("0"))
        {
            linear_discount.setVisibility(View.GONE);
        }
        else if (!oneorder.getDiscountpoint().equals("0"))
        {
            discount = oneorder.getDiscountpoint();

        }
        setText();
        fillMealComponentList(); // fill meal compnents

    } // function of getDetails data from object

    private void setText() {
        ed_sellerNotes.setText(sellerNotes);
        ed_orderName.setText(orderName);
        ed_orderNumber.setText(orderNumber);
        ed_customerName.setText(customerName);
        ed_customerMobileNumber.setText(customerMobile);
        ed_orderTime.setText(orderTime);
        ed_orderDate.setText(orderDate);
        ed_customerLocation.setText(customerLocation);
        ed_customerNotes.setText(customerNotes);
        ed_day.setText(day + " " + getResources().getString(R.string.dayO));
        ed_hour.setText(hour + " " + getResources().getString(R.string.hourO));
        ed_minutes.setText(minute + " " + getResources().getString(R.string.Minute));
        ed_branch.setText(branch);
        ed_distanceToCustomer.setText(distanceToCustomer);
        ed_totalCostDelivery.setText(totalDeliveryPrice);
        ed_totalOrder.setText(totalOrder);
        Log.i("QP " , "total price" + totalOrder);
        tv_discount.setText(discount);
        ed_priceVat.setText(price_vat + "" );
        ed_orderPrice.setText(order_price + "");

    } // function of setText

    @SuppressLint("HandlerLeak")
    public void acceptOrder(View view) {

        sellerNotes = ed_sellerNotes.getText().toString();
        progress = new CustomDialogProgress();
        progress.init(this);
        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
    /*    new Thread() {
            public void run() {*/
//Retrofit

                RetrofiUrlConnection connection = new RetrofiUrlConnection(OrderDetails.this);
                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final AcceptOrderApi orderApi = retrofit.create(AcceptOrderApi.class);

                final Call<ResultModelUser> getInterestConnection = orderApi.acceptOrderApi(orderId, sellerNotes);

                getInterestConnection.enqueue(new Callback<ResultModelUser>() {
                    @Override
                    public void onResponse(Call<ResultModelUser> call, Response<ResultModelUser> response) {
                        try {

                            String code = response.body().getCode();
                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                if (response.body() != null) {
                                    acceptAndRejectDialog.show();
                                    message.setText(getString(R.string.orderAccepted));
                                    Intent intent = new Intent(OrderDetails.this, HomePageScreen.class);
                                    intent.putExtra("fragment", "order");
                                    startActivity(intent);
                                    finish();
                                }
                                else {
                                    showDialog.initDialog(getString(R.string.retry), OrderDetails.this);

                                }
                            } //  success
                            progress.dismiss();
                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<ResultModelUser> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), OrderDetails.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

         /*   }

        }.start();*/
    }// button click accept Order

    public void rejectOrder(View view) {
        sellerNotes = ed_sellerNotes.getText().toString().trim();
        progress = new CustomDialogProgress();
        progress.init(this);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                super.handleMessage(msg);
            }

        };
        progress.show();
      /*  new Thread() {
            public void run() {*/
//Retrofit
                RetrofiUrlConnection connection = new RetrofiUrlConnection(OrderDetails.this);
                Retrofit retrofit = RetrofiUrlConnection.connectWith();

                final RejectOrderApi orderApi = retrofit.create(RejectOrderApi.class);

                final Call<ResultModelUser> getInterestConnection = orderApi.rejectOrderApiWithVendorNotes(orderId, sellerNotes);

                getInterestConnection.enqueue(new Callback<ResultModelUser>() {
                    @Override
                    public void onResponse(Call<ResultModelUser> call, Response<ResultModelUser> response) {
                        try {

                            String code = response.body().getCode();

                            Log.i("QP", "code" + code);

                            if (code.equals("200")) {
                                if (response.body() != null) {
                                    acceptAndRejectDialog.show();
                                    message.setText(getString(R.string.orderRejected));
                                    Intent intent = new Intent(OrderDetails.this, HomePageScreen.class);
                                    intent.putExtra("fragment", "order");
                                    startActivity(intent);
                                    finish();
                                } else {
                                    showDialog.initDialog(getString(R.string.retry), OrderDetails.this);

                                }

                            } //  success


                            progress.dismiss();

                        } // try
                        catch (Exception e) {
                            Log.i("QP", "exception : " + e.toString());
                            progress.dismiss();
                        } // catch
                    } // onResponse

                    @Override
                    public void onFailure(Call<ResultModelUser> call, Throwable t) {
                        showDialog.initDialog(getString(R.string.retry), OrderDetails.this);
                        Log.i("QP", "error : " + t.toString());
                        progress.dismiss();
                    } // on Failure
                });
// Retrofit

          /*  }

        }.start();*/
    } // button click reject Order

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread.interrupted();
        toolBarConfig.onDestroy();

    }
} // class of OrderDetails
