package com.invetechs.mamamiavendorversion.View.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.invetechs.mamamiavendorversion.R;
import com.invetechs.mamamiavendorversion.View.Activity.HomePageScreen;
import com.invetechs.mamamiavendorversion.View.Dialog.ShowDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

public class ChangeLanguageFragment extends Fragment {

    // bind views
    @BindView(R.id.cb_arabic)
    AppCompatCheckBox cbArabic;

    @BindView(R.id.cb_english)
    AppCompatCheckBox cb_english;

    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    //vars
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String language;
    Boolean isArabic = true, isEnglish = true;
    SharedPreferences sharedPreferences;
    ShowDialog showDialog = new ShowDialog();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_language_fragment, container, false);
        ButterKnife.bind(this, view);
        checkLanguage();
        checkCheckBox();
        initLanguage();
        return view;
    } //on create language


    private void checkCheckBox() {
        cbArabic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                cb_english.setChecked(false);
                isArabic = true;
                isEnglish = false;

            }
        });

        cb_english.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                cbArabic.setChecked(false);
                isEnglish = true;
                isArabic = false;
            }
        });
    } // select checkbox

    private void checkLanguage() {
        preferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
        language = preferences.getString("language", "ar");

    }//initialize language

    @OnClick(R.id.btn_confirm)
    public void setLanguage() {
        if (cbArabic.isChecked() || cb_english.isChecked()) {

            if (isArabic == true) {
                preferences.edit().putString("language", "ar").apply();
                Intent in = new Intent(getContext(), HomePageScreen.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(in);
            } // change language

            else if (isEnglish == true) {
                preferences.edit().putString("language", "en").apply();

                Intent in = new Intent(getContext(), HomePageScreen.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(in);
            } // change language

        } // choose language success
        else {
            showDialog.initDialog(getString(R.string.please_select_language), getActivity());
            return;
        } // dont change language

    }// buttom set lang

    private void initLanguage() {

        sharedPreferences = getContext().getSharedPreferences("user", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getString("language", "ar").equals("ar")) {
            ViewCompat.setLayoutDirection(((Activity) getContext()).getWindow().getDecorView(), ViewCompat.LAYOUT_DIRECTION_RTL);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GESSTwoMedium.otf");
            cb_english.setTypeface(font);
            cbArabic.setTypeface(font);
            btnConfirm.setTypeface(font);
        } else if (sharedPreferences.getString("language", "ar").equals("en")) {
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
            cb_english.setTypeface(font);
            cbArabic.setTypeface(font);
            btnConfirm.setTypeface(font);
        }

    }//initialize language

}
